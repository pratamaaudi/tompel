-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14 Nov 2018 pada 12.34
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tompel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `foto_barang` varchar(50) NOT NULL,
  `jenis_barang` varchar(50) NOT NULL,
  `shapus` tinyint(4) NOT NULL,
  `nasabah_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id`, `nama_barang`, `foto_barang`, `jenis_barang`, `shapus`, `nasabah_id`) VALUES
(1, 'IPHONE X', '1-1', 'IOS', 0, 1),
(2, 'SAMSUNG J6 PRO', '1-2', 'ANDROID', 0, 1),
(3, 'SAMSUNG S8 PLUS', '1-3', 'ANDROID', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_transaksi_gadai`
--

CREATE TABLE `barang_transaksi_gadai` (
  `id_barang` int(11) DEFAULT NULL,
  `id_transaksi_gadai` int(11) DEFAULT NULL,
  `jumlah_barang` int(11) DEFAULT NULL,
  `status_barang` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang_transaksi_gadai`
--

INSERT INTO `barang_transaksi_gadai` (`id_barang`, `id_transaksi_gadai`, `jumlah_barang`, `status_barang`) VALUES
(1, 3, 0, 0),
(2, 3, 0, 0),
(3, 3, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `denda`
--

CREATE TABLE `denda` (
  `id_denda` int(11) NOT NULL,
  `denda_hari` int(11) NOT NULL,
  `s_hapus` tinyint(4) NOT NULL,
  `min_pinjam` int(11) DEFAULT NULL,
  `max_pinjam` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `denda`
--

INSERT INTO `denda` (`id_denda`, `denda_hari`, `s_hapus`, `min_pinjam`, `max_pinjam`) VALUES
(5, 2000, 0, 0, 999000),
(6, 2500, 0, 1000000, 1499999),
(7, 3000, 0, 1500000, 5000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `judul` longtext NOT NULL,
  `keterangan` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id_faq`, `pegawai_id`, `judul`, `keterangan`) VALUES
(3, 4, 'TUGAS AKHIR', 'BISMILLLAH SEMOGA TUGAS AKHIR BISA BERJALAN DENGAN BAIK DAN LANCAR AMINNN '),
(4, 4, 'SIDANG TUGAS AKHIR', 'BISMILLAH SEMOGA SIDANG TUGAS AKHIR BISA BERJALAN DENGAN BAIK AMINNNN'),
(5, 1, 'TUGAS AKHIR', 'SEMOGA DIBERI KELANCARAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar_berjalan`
--

CREATE TABLE `gambar_berjalan` (
  `id_gambar_berjalan` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `gambar` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar_berjalan`
--

INSERT INTO `gambar_berjalan` (`id_gambar_berjalan`, `pegawai_id`, `gambar`) VALUES
(1, 4, '0283918269.png'),
(2, 1, '5ed836f930.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_jaminan`
--

CREATE TABLE `jenis_jaminan` (
  `id` int(11) NOT NULL,
  `jenis` tinyint(4) DEFAULT NULL,
  `no_mesin` varchar(45) DEFAULT NULL,
  `tahun_kendaraan` int(11) DEFAULT NULL,
  `no_polisi` varchar(45) DEFAULT NULL,
  `merk` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `tahun_pembuatan` int(11) DEFAULT NULL,
  `isi_silinder` varchar(45) DEFAULT NULL,
  `warna` varchar(45) DEFAULT NULL,
  `tahun_registrasi` int(11) DEFAULT NULL,
  `foto_dokumen` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `nama_pemilik_stnk` varchar(45) DEFAULT NULL,
  `no_rangka` varchar(45) NOT NULL,
  `shapus` smallint(6) NOT NULL,
  `nasabah_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_jaminan`
--

INSERT INTO `jenis_jaminan` (`id`, `jenis`, `no_mesin`, `tahun_kendaraan`, `no_polisi`, `merk`, `type`, `model`, `tahun_pembuatan`, `isi_silinder`, `warna`, `tahun_registrasi`, `foto_dokumen`, `status`, `nama_pemilik_stnk`, `no_rangka`, `shapus`, `nasabah_id`) VALUES
(1, 3, '1NZZ194460', 2015, 'W 794 RT', 'TOYOTA', 'YARIS TRD SPORTIVO', 'MINIBUS', 2015, '01497 CC', 'MERAH METALIC', 2015, '1-1', 1, 'AHMAD BASTOMMY MUSTHAFA', 'MHFKT9RF31F600202', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nasabah`
--

CREATE TABLE `nasabah` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `alamat` varchar(45) DEFAULT NULL,
  `telp` varchar(45) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `no_ktp` varchar(45) DEFAULT NULL,
  `nama_pekerjaan` varchar(45) DEFAULT NULL,
  `alamat_pekerjaan` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `no_rekening` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nasabah`
--

INSERT INTO `nasabah` (`id`, `nama`, `alamat`, `telp`, `no_hp`, `no_ktp`, `nama_pekerjaan`, `alamat_pekerjaan`, `user_id`, `pegawai_id`, `no_rekening`) VALUES
(1, 'DELLA ERDIYANTI AMD.Keb STR.Keb', 'JALAN KESAMBEN WETAN RT 12 DRIOYOREJO, GRESIK', '03186877766', '082256665555', '111111', 'BIDAN', 'JALAN KESAMBEN WETAN RT 12 DRIYOREJO, GRESIK ', 2, 1, '787776536363663');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id` int(11) NOT NULL,
  `keterangan` longtext NOT NULL,
  `transaksi_peminjaman_no_kontrak` int(11) DEFAULT NULL,
  `transaksi_investasi_id` int(11) DEFAULT NULL,
  `transaksi_gadai_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `status_baca` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `alamat`, `status`, `no_hp`, `user_id`) VALUES
(1, 'Ahmad Bastommy Musthafa', 'Jalan Berbek 1 Nomor 68 Waru Sidoarjo', 1, '082230999286', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perubahan_suku_bunga`
--

CREATE TABLE `perubahan_suku_bunga` (
  `id` int(11) NOT NULL,
  `id_suku_bunga` int(11) DEFAULT NULL,
  `id_transaksi_peminjaman` int(11) DEFAULT NULL,
  `id_transaksi_gadai` int(11) DEFAULT NULL,
  `id_transaksi_investasi` int(11) DEFAULT NULL,
  `tgl_perubahan_suku_bunga` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `perubahan_suku_bunga`
--

INSERT INTO `perubahan_suku_bunga` (`id`, `id_suku_bunga`, `id_transaksi_peminjaman`, `id_transaksi_gadai`, `id_transaksi_investasi`, `tgl_perubahan_suku_bunga`) VALUES
(9, 2, 4, NULL, NULL, '2018-11-14 04:20:18'),
(10, 4, NULL, 3, NULL, '2018-11-14 10:20:56'),
(11, 1, NULL, NULL, 4, '2018-11-14 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suku_bunga`
--

CREATE TABLE `suku_bunga` (
  `id` int(11) NOT NULL,
  `jumlah_bunga` double DEFAULT NULL,
  `kode_transaksi` tinyint(4) DEFAULT NULL,
  `shapus` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `suku_bunga`
--

INSERT INTO `suku_bunga` (`id`, `jumlah_bunga`, `kode_transaksi`, `shapus`) VALUES
(1, 1, 2, 0),
(2, 1.5, 0, 0),
(3, 2, 2, 1),
(4, 2, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_angsuran`
--

CREATE TABLE `transaksi_angsuran` (
  `id` int(11) NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `real_tgl_bayar` datetime DEFAULT NULL,
  `jumlah_denda` double NOT NULL,
  `jumlah_bayar` double NOT NULL,
  `angsuran_ke` int(11) NOT NULL,
  `status_angsuran` tinyint(4) NOT NULL,
  `status_denda` tinyint(4) NOT NULL,
  `transaksi_peminjaman_no_kontrak` int(11) NOT NULL,
  `pegawai_id` int(11) NOT NULL,
  `struk_pembayaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_angsuran`
--

INSERT INTO `transaksi_angsuran` (`id`, `tanggal_bayar`, `real_tgl_bayar`, `jumlah_denda`, `jumlah_bayar`, `angsuran_ke`, `status_angsuran`, `status_denda`, `transaksi_peminjaman_no_kontrak`, `pegawai_id`, `struk_pembayaran`) VALUES
(89, '2018-12-14 04:20:18', NULL, 0, 0, 1, 0, 0, 4, 0, ''),
(90, '2019-01-14 04:20:18', NULL, 0, 0, 2, 0, 0, 4, 0, ''),
(91, '2019-02-14 04:20:18', NULL, 0, 0, 3, 0, 0, 4, 0, ''),
(92, '2019-03-14 04:20:18', NULL, 0, 0, 4, 0, 0, 4, 0, ''),
(93, '2019-04-14 04:20:18', NULL, 0, 0, 5, 0, 0, 4, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_angsuran_gadai`
--

CREATE TABLE `transaksi_angsuran_gadai` (
  `id` int(11) NOT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `real_tgl_bayar` datetime DEFAULT NULL,
  `jumlah_denda` double DEFAULT NULL,
  `jumlah_bayar` double DEFAULT NULL,
  `angsuran_ke` int(11) DEFAULT NULL,
  `status_angsuran` tinyint(4) DEFAULT NULL,
  `transaksi_gadai_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `struk_pembayaran` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_angsuran_gadai`
--

INSERT INTO `transaksi_angsuran_gadai` (`id`, `tanggal_bayar`, `real_tgl_bayar`, `jumlah_denda`, `jumlah_bayar`, `angsuran_ke`, `status_angsuran`, `transaksi_gadai_id`, `pegawai_id`, `struk_pembayaran`) VALUES
(11, '2018-12-14 10:20:56', NULL, NULL, NULL, 1, 0, 3, NULL, NULL),
(12, '2019-01-14 10:20:56', NULL, NULL, NULL, 2, 0, 3, NULL, NULL),
(13, '2019-02-14 10:20:56', NULL, NULL, NULL, 3, 0, 3, NULL, NULL),
(14, '2019-03-14 10:20:56', NULL, NULL, NULL, 4, 0, 3, NULL, NULL),
(15, '2019-04-14 10:20:56', NULL, NULL, NULL, 5, 0, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_gadai`
--

CREATE TABLE `transaksi_gadai` (
  `id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status_cair` tinyint(4) DEFAULT NULL,
  `tgl_cair` datetime DEFAULT NULL,
  `status_transaksi` tinyint(4) DEFAULT NULL,
  `jumlah_pinjaman` int(11) NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `jumlah_angsuran` int(11) NOT NULL,
  `biaya_administrasi` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `nasabah_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `shapus` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_gadai`
--

INSERT INTO `transaksi_gadai` (`id`, `tanggal`, `status_cair`, `tgl_cair`, `status_transaksi`, `jumlah_pinjaman`, `jangka_waktu`, `jumlah_angsuran`, `biaya_administrasi`, `total`, `nasabah_id`, `pegawai_id`, `shapus`) VALUES
(3, '2018-11-14 10:20:56', 1, '2018-11-14 10:21:15', 1, 2000000, 5, 40000, 100000, 2200000, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_investasi`
--

CREATE TABLE `transaksi_investasi` (
  `id` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `jumlah_uang` double DEFAULT NULL,
  `jangka_waktu` int(11) DEFAULT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `status_transaksi` tinyint(4) NOT NULL DEFAULT '0',
  `nasabah_id` int(11) NOT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `transaksi_investasi_id` int(11) DEFAULT NULL,
  `shapus` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_investasi`
--

INSERT INTO `transaksi_investasi` (`id`, `tanggal`, `jumlah_uang`, `jangka_waktu`, `tanggal_jatuh_tempo`, `status_transaksi`, `nasabah_id`, `pegawai_id`, `transaksi_investasi_id`, `shapus`) VALUES
(4, '2018-11-14 00:00:00', 99000000, 5, '2019-04-14', 0, 1, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_peminjaman`
--

CREATE TABLE `transaksi_peminjaman` (
  `no_kontrak` int(11) NOT NULL,
  `tanggal_pinjam` datetime DEFAULT NULL,
  `jangka_waktu` int(11) DEFAULT NULL,
  `jumlah_pinjaman` double DEFAULT NULL,
  `status_transaksi` tinyint(4) DEFAULT NULL,
  `status_jaminan` tinyint(4) DEFAULT NULL,
  `jaminan` tinyint(4) DEFAULT NULL,
  `jumlah_angsuran` int(11) DEFAULT NULL,
  `biaya_administrasi` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `id_denda` int(11) DEFAULT NULL,
  `status_cair` tinyint(4) NOT NULL,
  `tgl_cair` datetime NOT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `nasabah_id` int(11) DEFAULT NULL,
  `jenis_jaminan_id` int(11) DEFAULT NULL,
  `shapus` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_peminjaman`
--

INSERT INTO `transaksi_peminjaman` (`no_kontrak`, `tanggal_pinjam`, `jangka_waktu`, `jumlah_pinjaman`, `status_transaksi`, `status_jaminan`, `jaminan`, `jumlah_angsuran`, `biaya_administrasi`, `total`, `id_denda`, `status_cair`, `tgl_cair`, `pegawai_id`, `nasabah_id`, `jenis_jaminan_id`, `shapus`) VALUES
(4, '2018-11-14 04:20:18', 5, 2000000, 1, 0, 0, 430000, 100000, 2150000, 7, 1, '2018-11-14 10:20:28', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_pencairan_dana`
--

CREATE TABLE `transaksi_pencairan_dana` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nasabah_id` int(11) NOT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `transaksi_investasi_id` int(11) NOT NULL,
  `shapus` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_pencairan_dana`
--

INSERT INTO `transaksi_pencairan_dana` (`id`, `status`, `jumlah`, `tanggal`, `nasabah_id`, `pegawai_id`, `transaksi_investasi_id`, `shapus`) VALUES
(1, 1, 1000000, '2018-11-14', 1, 1, 4, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_pendapatan_bunga_investasi`
--

CREATE TABLE `transaksi_pendapatan_bunga_investasi` (
  `id` int(11) NOT NULL,
  `jumlah_pendapatan_bunga` double NOT NULL,
  `status_cair` tinyint(4) NOT NULL,
  `tanggal_cair` datetime DEFAULT NULL,
  `transaksi_investasi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_pendapatan_bunga_investasi`
--

INSERT INTO `transaksi_pendapatan_bunga_investasi` (`id`, `jumlah_pendapatan_bunga`, `status_cair`, `tanggal_cair`, `transaksi_investasi_id`) VALUES
(1, 990000, 1, '2019-01-16 00:00:00', 4),
(2, 990000, 1, '2019-01-16 00:00:00', 4),
(3, 990000, 1, '2019-01-16 00:00:00', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `tgl_regist` datetime NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `shapus` smallint(6) NOT NULL,
  `img` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `tgl_regist`, `hak_akses`, `email`, `shapus`, `img`) VALUES
(1, 'tommy', '5abf03c529e5896cdac8a29c535ece50', '2018-08-28 08:38:00', 'AKTIF', 'weallytommy@gmail.com', 0, 'WIN_20180607_19_04_20_Pro.jpg'),
(2, 'della', '48b491e4a8a91d5ab1ca730b4398017c', '2018-11-04 01:37:50', 'AKTIF', 'dellaerdiyanti2@gmail.com', 0, 'della.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id_denda`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indexes for table `gambar_berjalan`
--
ALTER TABLE `gambar_berjalan`
  ADD PRIMARY KEY (`id_gambar_berjalan`);

--
-- Indexes for table `jenis_jaminan`
--
ALTER TABLE `jenis_jaminan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nasabah`
--
ALTER TABLE `nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perubahan_suku_bunga`
--
ALTER TABLE `perubahan_suku_bunga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suku_bunga`
--
ALTER TABLE `suku_bunga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_angsuran`
--
ALTER TABLE `transaksi_angsuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_angsuran_gadai`
--
ALTER TABLE `transaksi_angsuran_gadai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_gadai`
--
ALTER TABLE `transaksi_gadai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_investasi`
--
ALTER TABLE `transaksi_investasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_peminjaman`
--
ALTER TABLE `transaksi_peminjaman`
  ADD PRIMARY KEY (`no_kontrak`);

--
-- Indexes for table `transaksi_pencairan_dana`
--
ALTER TABLE `transaksi_pencairan_dana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_pendapatan_bunga_investasi`
--
ALTER TABLE `transaksi_pendapatan_bunga_investasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `denda`
--
ALTER TABLE `denda`
  MODIFY `id_denda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gambar_berjalan`
--
ALTER TABLE `gambar_berjalan`
  MODIFY `id_gambar_berjalan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jenis_jaminan`
--
ALTER TABLE `jenis_jaminan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nasabah`
--
ALTER TABLE `nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `perubahan_suku_bunga`
--
ALTER TABLE `perubahan_suku_bunga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `suku_bunga`
--
ALTER TABLE `suku_bunga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi_angsuran`
--
ALTER TABLE `transaksi_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `transaksi_angsuran_gadai`
--
ALTER TABLE `transaksi_angsuran_gadai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transaksi_gadai`
--
ALTER TABLE `transaksi_gadai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi_investasi`
--
ALTER TABLE `transaksi_investasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi_peminjaman`
--
ALTER TABLE `transaksi_peminjaman`
  MODIFY `no_kontrak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi_pencairan_dana`
--
ALTER TABLE `transaksi_pencairan_dana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaksi_pendapatan_bunga_investasi`
--
ALTER TABLE `transaksi_pendapatan_bunga_investasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
