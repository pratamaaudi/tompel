<?php

require '../config.php'; 

$sql1 = "SELECT no_kontrak, day(tanggal_pinjam) as 'hari', month(tanggal_pinjam) as 'bulan', year(tanggal_pinjam) as 'tahun', jangka_waktu FROM `transaksi_peminjaman` WHERE status_transaksi = 1";
$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {

	$tanggalTransaksi = getTanggalTransaksi($row1['hari'], $row1['bulan'], $row1['tahun']);
	$jatuhTempo = getTanggalJatuhTempo($tanggalTransaksi, $row1['jangka_waktu']);
	// echo getJumlahAngsuran($conn, $row1['no_kontrak']);
	if(cekJatuhTempo($jatuhTempo)){
		if(getJumlahAngsuran($conn, $row1['no_kontrak']) == 0){
			updateStatusTransaksiKreditMacet($conn, $row1['no_kontrak']);
			// echo "sukses";
		}
	}
}

// fungsi untuk menngambil tanggal transaksi
function getTanggalTransaksi($hari, $bulan, $tahun){
	return $date = DateTime::createFromFormat('j-m-Y', "$hari-$bulan-$tahun");
}

function getTanggalJatuhTempo($tanggaltransaksi, $jangka_waktu){
	return $tanggaltransaksi->modify('+'.$jangka_waktu.' months');
}

function cekJatuhTempo($tanggalJatuhTempo){
	if($tanggalJatuhTempo < new DateTime()){
		return true;
	}else{
		return false;
	}
}

function getJumlahAngsuran($conn, $noKontrak){
	$jumlah = 0;
	$sql1 = "SELECT count(*) as 'jumlah' FROM `transaksi_angsuran` WHERE transaksi_peminjaman_no_kontrak = $noKontrak and status_angsuran = 1";
	$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {
	$jumlah = $row1['jumlah'];
}

return $jumlah;	
	
}

function updateStatusTransaksiKreditMacet($conn, $noKontrak){
	$sql = "UPDATE `transaksi_peminjaman` SET `status_transaksi` = '5', `status_jaminan` = 1 WHERE `transaksi_peminjaman`.`no_kontrak` = $noKontrak;";
	$result1 = mysqli_query($conn, $sql);
}






// KREDIT MACET UNTUK GADAI 

$sql1 = "SELECT id, day(tanggal) as 'hari', month(tanggal) as 'bulan', year(tanggal) as 'tahun', jangka_waktu FROM `transaksi_gadai` WHERE status_transaksi = 1";
$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {

	$tanggalTransaksiGadai = getTanggalTransaksiGadai($row1['hari'], $row1['bulan'], $row1['tahun']);
	$jatuhTempoGadai = getTanggalJatuhTempoGadai($tanggalTransaksiGadai, $row1['jangka_waktu']);
	// echo getJumlahAngsuran($conn, $row1['no_kontrak']);
	if(cekJatuhTempoGadai($jatuhTempoGadai)){
		if(getJumlahAngsuranGadai($conn, $row1['id']) == 0){
			updateStatusTransaksiKreditMacetGadai($conn, $row1['id']);
			// echo "sukses";
		}
	}
}

// fungsi untuk menngambil tanggal transaksi
function getTanggalTransaksiGadai($hari, $bulan, $tahun){
	return $date = DateTime::createFromFormat('j-m-Y', "$hari-$bulan-$tahun");
}

function getTanggalJatuhTempoGadai($tanggaltransaksi, $jangka_waktu){
	return $tanggaltransaksi->modify('+'.$jangka_waktu.' months');
}

function cekJatuhTempoGadai($tanggalJatuhTempo){
	if($tanggalJatuhTempo < new DateTime()){
		return true;
	}else{
		return false;
	}
}

function getJumlahAngsuranGadai($conn, $id){
	$jumlah = 0;
	$sql1 = "SELECT count(*) as 'jumlah' FROM `transaksi_angsuran_gadai` WHERE transaksi_gadai_id = $id and status_angsuran = 1";
	$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {
	$jumlah = $row1['jumlah'];
}

return $jumlah;	
	
}

function updateStatusTransaksiKreditMacetGadai($conn, $id){
	$sql = "UPDATE `transaksi_gadai` SET `status_transaksi` = '5' WHERE id = $id;";
	$result1 = mysqli_query($conn, $sql);
	$sql2 = "UPDATE `barang_transaksi_gadai` SET `status_barang` = '1' WHERE id_transaksi_gadai = $id;";
	$result2 = mysqli_query($conn, $sql2);
}
?>