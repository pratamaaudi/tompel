<?php
require '../config.php'; 

$sql1 = "SELECT ti.id, date(tanggal) as 'tanggal', jumlah_uang, jumlah_bunga, COUNT(NULLIF(tpbi.id,'')) as 'bunga_sebelumnya' FROM `transaksi_investasi` ti
LEFT JOIN
transaksi_pendapatan_bunga_investasi tpbi on ti.id = tpbi.transaksi_investasi_id
INNER JOIN
perubahan_suku_bunga psb on ti.id = psb.id_transaksi_investasi
INNER JOIN
suku_bunga sb on psb.id_suku_bunga = sb.id
WHERE
ti.status_transaksi = 0
GROUP BY
ti.id";
$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {

	$tanggal = DateTime::createFromFormat('Y-m-j', $row1['tanggal']);
	$tanggal->modify('+'.($row1['bunga_sebelumnya']+1).'months');

	if($tanggal<new DateTime()){
		$bunga = $row1['jumlah_uang']*$row1['jumlah_bunga']/100;
		$id = $row1['id'];
		$result = mysqli_query($conn, "INSERT INTO `transaksi_pendapatan_bunga_investasi` (`id`, `jumlah_pendapatan_bunga`, `status_cair`, `tanggal_cair`, `transaksi_investasi_id`) VALUES (NULL, $bunga, '0', NULL, $id);");
		if (!$result) { 
			die("SQL ERROR : gagal insert data"); 
			return false;
		}else{
			return true;
		}
		return true;
	}

	// echo $tanggal->format('d-m-y');
}
?>