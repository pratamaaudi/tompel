<?php

require '../config.php'; 

$sql1 = "SELECT id, day(tanggal) as 'hari', month(tanggal) as 'bulan', year(tanggal) as 'tahun', jangka_waktu, id, jumlah_uang, nasabah_id, pegawai_id, tanggal_jatuh_tempo FROM `transaksi_investasi` WHERE status_transaksi = 0";
$result1 = mysqli_query($conn, $sql1);
if (!$result1) { die("SQL Error Result1 "); }
while ($row1 = mysqli_fetch_array($result1)) {

	$jatuhTempo = $row1['tanggal_jatuh_tempo'];
	$jatuhTempoDate = DateTime::createFromFormat('Y-m-d', $jatuhTempo);

	if(cekPerpanjangan($jatuhTempoDate)){
		$tanggalHariIni = new DateTime;
		 if(insertDataRenewal($tanggalHariIni->format('Y-m-d'), $row1['jumlah_uang'], $row1['jangka_waktu'], getTanggalJatuhTempo($tanggalHariIni, $row1['jangka_waktu'])->format('Y-m-d'),$row1['nasabah_id'], $row1['pegawai_id'], $row1['id'], $conn)){
		 	updateDataLama($conn, $row1['id']);
		 	setBunga($conn, getBungaInvestasi($conn), getTransaksiInvestasiId($conn), $tanggalHariIni->format('Y-m-d'));
		 }
	}
}
// fungsi untuk menngambil tanggal transaksi
function getTanggalTransaksi($hari, $bulan, $tahun){
	return $date = DateTime::createFromFormat('j-m-Y', "$hari-$bulan-$tahun");
}

function cekPerpanjangan($tanggalJatuhTempo){
	if($tanggalJatuhTempo < new DateTime()){
		return true;
	}else{
		return false;
	}
}

function insertDataRenewal($tanggalHariIni, $jumlahUang, $jangkaWaktu, $tanggalJatuhTempo, $nasabahId, $pegawaiId, $id, $conn){
	$result = mysqli_query($conn, "INSERT INTO `transaksi_investasi` (
			`id`, 
			`tanggal`, 
			`jumlah_uang`, 
			`jangka_waktu`, 
			`tanggal_jatuh_tempo`, 
			`status_transaksi`, 
			`nasabah_id`, 
			`pegawai_id`, 
			`transaksi_investasi_id`, 
			`shapus`) 
			VALUES (
			NULL, 
			'$tanggalHariIni', 
			'$jumlahUang', 
			'$jangkaWaktu', 
			'$tanggalJatuhTempo', 
			'0', 
			'$nasabahId', 
			'$pegawaiId', 
			'$id', 
			'0');");

		if (!$result) { 
			die("SQL ERROR : gagal insert data"); 
			return false;
		}else{
			return true;
		}

		return true;
}

function updateDataLama($conn, $id){
	$sql = "UPDATE `transaksi_investasi` SET `status_transaksi` = '1' WHERE `transaksi_investasi`.`id` = $id;";
	$result1 = mysqli_query($conn, $sql);
}

function getTanggalJatuhTempo($tanggaltransaksi, $jangka_waktu){
	return $tanggaltransaksi->modify('+'.$jangka_waktu.' months');
}

function setBunga($conn, $sukuBungaId, $transaksiInvestasiId, $tanggal){
	$result = mysqli_query($conn, "
		INSERT INTO `perubahan_suku_bunga` (
		`id`, 
		`id_suku_bunga`, 
		`id_transaksi_peminjaman`, `id_transaksi_gadai`, 
		`id_transaksi_investasi`, 
		`tgl_perubahan_suku_bunga`) 
		VALUES (
		NULL, 
		'$sukuBungaId', 
		NULL, NULL, 
		'$transaksiInvestasiId', 
		'$tanggal'
		);
		");
}

function getTransaksiInvestasiId($conn){
	$transaksiInvestasiId;
		$sql1 = "SELECT id FROM `transaksi_investasi` ORDER BY id DESC LIMIT 1";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$transaksiInvestasiId = $row1['id'];
		}
		return $transaksiInvestasiId;
}

function getBungaInvestasi($conn){
	$bungaInvestasi;
		$sql1 = "SELECT id FROM `suku_bunga` where kode_transaksi = 2 and shapus = 0 limit 1";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$bungaInvestasi = $row1['id'];
		}
		return $bungaInvestasi;
}
?>