<?php
ob_start();
require 'config.php'; 
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="img/flags/flags.css">
	<link rel="stylesheet" href="css/retina.css">
	<link rel="stylesheet" href="js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="css/linecons/style.css">
	<link rel="stylesheet" href="js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<style type="text/css">
	#full_body{
		background-color: #f2f2f2; 
		width: 70%; 
		margin: 2% 0% 0% 0%; 
		border-radius: 5px; 
		padding: 15px 5px 0px 5px;
	}
	#img_body{
		width: 97%; 
		border-radius: 5px; 
		max-height: 150px; 
		height: 100%; 
		margin-bottom: 10px;
	}
	#full_panel{
		border-radius: 2px;
	}
	#scroll_panel{
		overflow-y: scroll; 
		height: 300px;
	}
	#footer_panel{
		background: #50524d; 
		text-align: left;
	}
	#title_footer{
		color: white;
	}
	#tempat_btn{
		border-radius: 10px;
		height: 140px; 
		padding: 10px 10px 10px 10px; 
		background: #50524d; 
		margin: 10px 0px 10px 0px;
	}
	#btn{
		position: absolute;
		border-radius: 10px; 
		bottom: 0px; 
		right: 0px;
		box-shadow: 0px 0px 0px 15px rgba(255,255,255,1), 0px 0px 0px 1px rgba(0,0,0,0.5) inset;
		cursor: pointer;
	}
	h3{
		color: white;
	}
</style>
</head>
<body class="sidebar_hidden">
	<div id="wrapper_all">
		<center>
			<div id="full_body">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php
						$ke1 = 0;
						$resultGB1 = mysqli_query($conn, "SELECT gambar FROM gambar_berjalan");
						if (!$resultGB1) { die("SQL Error ResultGB "); }
						while ($rowGB1 = mysqli_fetch_array($resultGB1)) {
							?>
							<li data-target="#myCarousel" data-slide-to="<?php echo $ke1; ?>"<?php if($ke1 == 0){ echo ' class="active"'; } ?>></li>
							<?php 
							$ke1++; 
						} ?>
					</ol>
					<div class="carousel-inner" role="listbox">
						<?php
						$ke2 = 0;
						$resultGB2 = mysqli_query($conn, "SELECT gambar FROM gambar_berjalan");
						if (!$resultGB2) { die("SQL Error ResultGB "); }
						while ($rowGB2 = mysqli_fetch_array($resultGB2)) {
							$ke2++; 
							?>
							<div class="item<?php if($ke2 == 1){ echo ' active'; } ?>" style="height: 160px;">
								<center><img src="gallery/<?php echo $rowGB2['gambar']; ?>" id="img_body"/></center>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="col-sm-9" id="scroll_panel">
					<div class="panel-group" id="accordion1">
						<?php
						$i = 0;
						$resultFAQ = mysqli_query($conn, "SELECT f.id_faq, f.judul, f.keterangan, p.nama FROM faq f JOIN pegawai p ON f.pegawai_id = p.id");
						if (!$resultFAQ) { die("SQL Error ResultFAQ "); }
						while ($rowFAQ = mysqli_fetch_array($resultFAQ)) {
							$i++;
							?>
							<div class="panel panel-default<?php if($i == 1){ echo ' sect_active'; } ?>" id="full_panel">
								<div class="panel-heading" id="footer_panel">
									<h4 class="panel-title">
										<a id="title_footer" class="accordion-toggle<?php if($i != 1){ echo ' collapsed'; } ?>" data-toggle="collapse" data-parent="#accordion1" href="#acc<?php echo $i; ?>_collapse<?php echo $i; ?>">
											<?php echo 'No.' . $i . ' ' . $rowFAQ['judul']; ?>
											<span class="icon-angle-up" style="color: #50524d;"></span>
										</a>
									</h4>
								</div>
								<div id="acc<?php echo $i; ?>_collapse<?php echo $i; ?>" style="height: auto;" class="panel-collapse<?php if($i == 1){ echo ' in'; } else { echo ' collapse'; } ?>">
									<div class="panel-body">
										<div class="col-sm-12"><?php echo $rowFAQ['keterangan']; ?></div>
										<div class="col-sm-12">
											<div class="col-sm-8"></div>
											<div class="col-sm-4"><center>Penulis</center><center>(<?php echo $rowFAQ['nama']; ?>)</center></div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="col-sm-12" id="tempat_btn">
						<h3>ADMIN / PEGAWAI</h3>
						<a class="btn btn-info" href="karyawan/index.php" id="btn">MASUK</a><br><br>
					</div>
					<div class="col-sm-12" id="tempat_btn">
						<h3>NASABAH</h3>
						<a class="btn btn-info" href="nasabah/index.php" id="btn">MASUK</a>
					</div>
				</div>
				<img style="width: 100%; height: 0%;" />
			</div>
		</center>
	</div>	
	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/jquery.ba-resize.min.js"></script>
	<script src="js/jquery_cookie.min.js"></script>
	<script src="js/retina.min.js"></script>
	<script src="js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="js/tinynav.js"></script>
	<script src="js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="js/ebro_common.js"></script>
	<script src="js/lib/peity/jquery.peity.min.js"></script>
	<script src="js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="js/lib/flot/jquery.flot.min.js"></script>
	<script src="js/lib/flot/jquery.flot.pie.min.js"></script>
	<script src="js/lib/flot/jquery.flot.time.min.js"></script>
	<script src="js/lib/flot/jquery.flot.tooltip.min.js"></script>
	<script src="js/lib/flot/jquery.flot.resize.js"></script>
	<script src="js/lib/FitVids/jquery.fitvids.js"></script>
	<script src="js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="js/lib/fullcalendar/fullcalendar.js"></script>
	<script src="js/pages/ebro_dashboard.js"></script>
</div>
</body>
</html>
<?php ob_end_flush(); ?>