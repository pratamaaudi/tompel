<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 

if(isset($_POST['isi_data_nasabah'])){
	$id_nasabah = $_POST['id_nasabah'];
	$result = mysqli_query($conn, "SELECT * FROM nasabah WHERE id = $id_nasabah");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	echo $row['no_ktp'] . "-" . $row['nama'] . "-" . $row['alamat'] . "-" . $row['nama_pekerjaan'] . "-" . $row['alamat_pekerjaan'] . "-" . $row['telp'] . "-" . $row['no_hp'];
}
if(isset($_POST['isi_data_kendaraan'])){
	$id_kendaraan = $_POST['id_kendaraan'];
	$result = mysqli_query($conn, "SELECT * FROM jenis_jaminan WHERE id = $id_kendaraan");
	if (!$result) { die("SQL Error Result SELECT * FROM jenis_jaminan WHERE id = $id_kendaraan"); }
	$row = mysqli_fetch_array($result);
	$status = 'AN';
	if($row['status'] == 1){ $status = 'OL'; }

	$jenis = '';
	if($row['jenis'] == 1){ $jenis = 'Roda Dua'; }
	else if($row['jenis'] == 2){ $jenis = 'Roda Tiga'; }
	else if($row['jenis'] == 3){ $jenis = 'Roda Empat'; }

	echo "-" . $row['nama_pemilik_stnk'] . "-" . $status . "-" . $jenis . "-" . $row['no_polisi'] . "-" . $row['tahun_kendaraan'] . "-" . $row['no_mesin'] . "-" . $row['no_rangka'];
}
if (isset($_POST['edit_transaksi_peminjaman'])) {
	$no_kontrak = $_POST['edit_transaksi_peminjaman'];
	$id_pegawai = $_SESSION['login_pegawai'];
	$id_nasabah = $_POST['nasabah'];
	$id_jenis_jaminan = $_POST['kendaraan'];
	$id_suku_bunga = $_POST['suku_bunga'];
	$tanggal_pinjam_lama = $_POST['tgl_lama'];
	$tanggal_pinjam = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jangka_waktu_lama = $_POST['jangka_waktu_lama'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	$status_transaksi = $_POST['status_transaksi'];
	$status_transaksi_lama = $_POST['status_transaksi_lama'];
	if($status_transaksi_lama == 3){
		$status_transaksi = $status_transaksi_lama;
	}
	$status_jaminan = $_POST['status_jaminan'];
	$jaminan = $_POST['jaminan'];
	$jumlah_angsuran = $_POST['nominal_angsuran'];
	$biaya_administrasi = $_POST['biaya_administrasi'];
	$total = $_POST['total'];
	$denda_hari = $_POST['denda'];
	$status_cair = $_POST['status_cair'];
	$tgl_cair = date('Y-m-d h:i:s');

	$result9 = mysqli_query($conn, "SELECT tp.status_transaksi, SUM(ta.jumlah_bayar) AS sudah_angsur FROM transaksi_peminjaman tp JOIN transaksi_angsuran ta ON tp.no_kontrak = ta.transaksi_peminjaman_no_kontrak WHERE tp.no_kontrak = $no_kontrak AND tp.status_transaksi = 1 GROUP BY tp.no_kontrak");
	if(!$result9){ die("SQL Error result 9"); }
	if (mysqli_num_rows($result9)) {
		$row9 = mysqli_fetch_array($result9);
		$sudah_angsur = $row9['sudah_angsur'];
		if($sudah_angsur > 0){ 
			$_SESSION['pesan_transaksi_peminjaman_edit'] = 'Maaf Transaksi Peminjaman tidak dapat di EDIT.\nKarena Transaksi Angsuran pada Transaksi Pinjaman Ini, Ada yang sudah dibayar';
			header('Location: transaksi_peminjaman_edit.php?id_transaksi='.$no_kontrak);
		} else {
			$sql1 = "UPDATE `transaksi_peminjaman` SET `tanggal_pinjam` = '$tanggal_pinjam', `jangka_waktu` = $jangka_waktu, `jumlah_pinjaman` = $jumlah_pinjaman, `status_transaksi` = $status_transaksi, `status_jaminan` = $status_jaminan, `jaminan` = $jaminan, `jumlah_angsuran` = $jumlah_angsuran, `biaya_administrasi` = $biaya_administrasi, `total` = $total, `id_denda` = $denda_hari, `status_cair` = $status_cair";
			if($status_cair != 0){
				$sql1 = $sql1 . ", `tgl_cair` = '$tgl_cair'";
			}
			$sql1 = $sql1 . ", `pegawai_id` = $id_pegawai, `nasabah_id` = $id_nasabah, `jenis_jaminan_id` = $id_jenis_jaminan WHERE `no_kontrak` = $no_kontrak";
			$result1 = mysqli_query($conn, $sql1);
			if(!$result1){ die("SQL ERROR : Result1"); }

			$result3 = mysqli_query($conn, "UPDATE `perubahan_suku_bunga` SET `id_suku_bunga` = $id_suku_bunga, `id_transaksi_peminjaman` = $no_kontrak, `tgl_perubahan_suku_bunga` = '$tanggal_pinjam' WHERE `tgl_perubahan_suku_bunga` = '$tanggal_pinjam_lama'");
			if(!$result3){ die("SQL ERROR : Result3"); }

			
			//|| $tanggal_pinjam_lama != $tanggal_pinjam jika memakai readonly
			if($jangka_waktu_lama != $jangka_waktu || $tanggal_pinjam_lama != $tanggal_pinjam){
				$result5 = mysqli_query($conn, "DELETE FROM `transaksi_angsuran` WHERE transaksi_peminjaman_no_kontrak = $no_kontrak");
				if(!$result5){ die("SQL ERROR : Result5"); }

				$tgl_angsur = intval(substr($tanggal_pinjam, 8, 2));
				$bln_angsur = intval(substr($tanggal_pinjam, 5, 2));
				$thn_angsur = intval(substr($tanggal_pinjam, 0, 4));
				$wkt_angsur = substr($tanggal_pinjam, 11, 8);
				for($i = 1; $i <= intval($jangka_waktu); $i++){
					$bln_angsur++;
					if($bln_angsur == 13){
						$bln_angsur = 0;
						$thn_angsur++;
						$i--;
					} else {
						$last_day = $thn_angsur . "-" . $bln_angsur;
						if(strlen(strval($bln_angsur)) == 1){
							$last_day = $thn_angsur . "-0" . $bln_angsur;
						}
						$sesuai_tgl_wkt_angsur = $last_day . "-" . $tgl_angsur . " " . $wkt_angsur;
						if(strlen(strval($tgl_angsur)) == 1){
							$sesuai_tgl_wkt_angsur = $last_day . "-0" . $tgl_angsur . " " . $wkt_angsur;
						}
						$date = new DateTime($last_day);
						$date->modify('last day of this month');
						$tgl_wkt_last = $date->format('Y-m-d') . " " . $wkt_angsur;

						$tgl_wkt_pasti_angsur = '';
						if($sesuai_tgl_wkt_angsur > $tgl_wkt_last){
							$tgl_wkt_pasti_angsur = $tgl_wkt_last;
						} else {
							$tgl_wkt_pasti_angsur = $sesuai_tgl_wkt_angsur;
						}
						$result4 = mysqli_query($conn, "INSERT INTO `transaksi_angsuran`(`tanggal_bayar`, `angsuran_ke`, `transaksi_peminjaman_no_kontrak`) VALUES ('$tgl_wkt_pasti_angsur', $i, $no_kontrak)");
						if(!$result4){ die("SQL ERROR : Result4"); }
					}
				}
			}
			header('Location: transaksi_peminjaman.php'); 
		}
	} else {
		if($status_transaksi_lama == 3){
			$sql1 = "UPDATE `transaksi_peminjaman` SET `tanggal_pinjam` = '$tanggal_pinjam', `jangka_waktu` = $jangka_waktu, `jumlah_pinjaman` = $jumlah_pinjaman, `status_transaksi` = $status_transaksi, `status_jaminan` = $status_jaminan, `jaminan` = $jaminan, `jumlah_angsuran` = $jumlah_angsuran, `biaya_administrasi` = $biaya_administrasi, `total` = $total, `id_denda` = $denda_hari, `status_cair` = $status_cair";
			if($status_cair != 0){
				$sql1 = $sql1 . ", `tgl_cair` = '$tgl_cair'";
			}
			$sql1 = $sql1 . ", `pegawai_id` = $id_pegawai, `nasabah_id` = $id_nasabah, `jenis_jaminan_id` = $id_jenis_jaminan WHERE `no_kontrak` = $no_kontrak";
			$result1 = mysqli_query($conn, $sql1);
			if(!$result1){ die("SQL ERROR : Result1"); }

			$result3 = mysqli_query($conn, "UPDATE `perubahan_suku_bunga` SET `id_suku_bunga` = $id_suku_bunga, `id_transaksi_peminjaman` = $no_kontrak, `tgl_perubahan_suku_bunga` = '$tanggal_pinjam' WHERE `tgl_perubahan_suku_bunga` = '$tanggal_pinjam_lama'");
			if(!$result3){ die("SQL ERROR : Result3"); }
			//|| $tanggal_pinjam_lama != $tanggal_pinjam jika memakai readonly
			//if($jangka_waktu_lama != $jangka_waktu || $tanggal_pinjam_lama != $tanggal_pinjam){
			$result5 = mysqli_query($conn, "DELETE FROM `transaksi_angsuran` WHERE transaksi_peminjaman_no_kontrak = $no_kontrak");
			if(!$result5){ die("SQL ERROR : Result5"); }

			$tgl_angsur = intval(substr($tanggal_pinjam, 8, 2));
			$bln_angsur = intval(substr($tanggal_pinjam, 5, 2));
			$thn_angsur = intval(substr($tanggal_pinjam, 0, 4));
			$wkt_angsur = substr($tanggal_pinjam, 11, 8);
			for($i = 1; $i <= intval($jangka_waktu); $i++){
				$bln_angsur++;
				if($bln_angsur == 13){
					$bln_angsur = 0;
					$thn_angsur++;
					$i--;
				} else {
					$last_day = $thn_angsur . "-" . $bln_angsur;
					if(strlen(strval($bln_angsur)) == 1){
						$last_day = $thn_angsur . "-0" . $bln_angsur;
					}
					$sesuai_tgl_wkt_angsur = $last_day . "-" . $tgl_angsur . " " . $wkt_angsur;
					if(strlen(strval($tgl_angsur)) == 1){
						$sesuai_tgl_wkt_angsur = $last_day . "-0" . $tgl_angsur . " " . $wkt_angsur;
					}
					$date = new DateTime($last_day);
					$date->modify('last day of this month');
					$tgl_wkt_last = $date->format('Y-m-d') . " " . $wkt_angsur;

					$tgl_wkt_pasti_angsur = '';
					if($sesuai_tgl_wkt_angsur > $tgl_wkt_last){
						$tgl_wkt_pasti_angsur = $tgl_wkt_last;
					} else {
						$tgl_wkt_pasti_angsur = $sesuai_tgl_wkt_angsur;
					}
					$result4 = mysqli_query($conn, "INSERT INTO `transaksi_angsuran`(`tanggal_bayar`, `angsuran_ke`, `transaksi_peminjaman_no_kontrak`) VALUES ('$tgl_wkt_pasti_angsur', $i, $no_kontrak)");
					if(!$result4){ die("SQL ERROR : Result4"); }
				}
			}
			//}
			header('Location: transaksi_peminjaman.php');
		} else {
			$result1 = mysqli_query($conn, "UPDATE `transaksi_peminjaman` SET status_jaminan = $status_jaminan WHERE no_kontrak = $no_kontrak");
			if(!$result1){ die("SQL ERROR : Result1"); }
			header('Location: transaksi_peminjaman_edit.php?id_transaksi='.$no_kontrak);
		}
	}
}
if(isset($_GET['del_no_kontrak'])){
	$no_kontrak = $_GET['del_no_kontrak'];
	$result1 = mysqli_query($conn, "UPDATE `transaksi_peminjaman` SET `shapus` = 1 WHERE no_kontrak = $no_kontrak AND status_transaksi = 0");
	if(!$result1){ die("SQL ERROR : Result1"); }
	$_SESSION['pesan_transaksi_peminjaman_edit'] = "Transaksi Peminjaman Tidak Bisa Di Hapus, Karena Angsuran Belum Lunas";
	header("Location: transaksi_peminjaman_edit.php?id_transaksi=".$no_kontrak);
}
if(isset($_GET['ust'])){
	$no_kontrak = $_GET['ust'];
	$id_nasabah =$_GET['id_nasabah'];
	$result1 = mysqli_query($conn, "UPDATE `transaksi_peminjaman` SET `status_transaksi` = 2 WHERE no_kontrak = $no_kontrak AND status_transaksi = 3 AND biaya_administrasi = (jumlah_pinjaman*5/100)");
	if(!$result1){ 
		$_SESSION['pesan_transaksi_peminjaman'] = "SQL ERROR : Result1"; 
	} else if($result1){
		kirimNotifikasiKeNasbah($conn, $no_kontrak, $id_nasabah, "Transaksi Peminjaman Anda dengan no kontrak $no_kontrak  Sudah di setujui");
		header("Location: transaksi_peminjaman.php");
	} else { 
		$_SESSION['pesan_transaksi_peminjaman'] = "Lengkapi Dulu Pada Detail Transaksi Peminjaman"; 
		header("Location: transaksi_peminjaman.php");
	}
}


function kirimNotifikasiKeNasbah($conn, $peminjaman_id, $userId, $msg){
	$query = "INSERT INTO `notifikasi` (`id`, `keterangan`, `transaksi_peminjaman_no_kontrak`, `transaksi_investasi_id`, `transaksi_gadai_id`, `user_id`, `status_baca`) VALUES (NULL, '$msg', '$peminjaman_id', NULL, NULL, '$userId', '0');";

    $result = mysqli_query($conn, $query);
    if (!$result) { die("SQL ERROR : gagal insert data"); }else{
        $_SESSION['alert_invesasi_insert'] = true;
    }
}
ob_end_flush(); ?>