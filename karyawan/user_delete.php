<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 
$idPegawai = 0;
$idNasabah = 0;
$idUser = 0;
if (isset($_GET['idPegawai'])) {
	$idPegawai = $_GET['idPegawai'];
	$idUser = $_GET['idPegawai'];
}
if (isset($_GET['idNasabah'])) {
	$idNasabah = $_GET['idNasabah'];
	$idUser = $_GET['idNasabah'];
}
$cekTunggaanNasabah = 0;
$pesan = '';
if (isset($_GET['idNasabah'])) {
	$result2 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id JOIN transaksi_peminjaman tp ON n.id = tp.nasabah_id WHERE tp.status_transaksi = 1 AND u.id = $idUser");
	if(!$result2){ die("SQL ERROR : result2"); } 
	if (mysqli_num_rows($result2)) {
		$cekTunggaanNasabah = 1;
		$pesan = 'Nasabah Tidak Dapat Di Hapus!. Karena Masih Punya Tunggaan Angsuran Peminjaman';
	}
	$result3 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id JOIN transaksi_gadai tg ON n.id = tg.nasabah_id WHERE tg.status_transaksi = 1 AND u.id = $idUser");
	if(!$result3){ die("SQL ERROR : result3"); }
	if (mysqli_num_rows($result3)) {
		$cekTunggaanNasabah = 1;
		$pesan = 'Nasabah Tidak Dapat Di Hapus!. Karena Masih Punya Tunggaan Angsuran Gadai';
	}
	$result4 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id JOIN transaksi_investasi ti ON n.id = ti.nasabah_id WHERE ti.status_transaksi = 1 AND u.id = $idUser");
	if(!$result4){ die("SQL ERROR : result4"); }
	if (mysqli_num_rows($result4)) {
		$cekTunggaanNasabah = 1;
		$pesan = 'Uang Investasi Belum Diambil';
	}
}
if($cekTunggaanNasabah == 0){
	$result1 = mysqli_query($conn, "UPDATE user SET shapus = 1 WHERE id = $idUser");
	if ($result1){
		if($idNasabah != 0){
			header("Location: nasabah.php");
		} else {
			header("Location: pegawai.php");
		}
	} else { 
		die("SQL ERROR : result1"); 
	}
} else {
	$_SESSION['pesan_profil_nasabah'] = $pesan;
	header("Location: profil_nasabah.php?idUser=".$idUser);
}
ob_end_flush(); ?>