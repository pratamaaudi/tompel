<body class=" sidebar_hidden side_fixed">



	
	<nav id="side_fixed_nav">
		<div class="slim_scroll">
			<div class="side_nav_actions">
				<a href="javascript:void(0)" id="side_fixed_nav_toggle"><span class="icon-align-justify"></span></a>
				<div id="toogle_nav_visible" class="make-switch switch-mini" data-on="success" data-on-label="<i class='icon-lock'></i>" data-off-label="<i class='icon-unlock-alt'></i>">
					<input id="nav_visible_input" type="checkbox">
				</div>
			</div>
			<ul id="text_nav_side_fixed">
				<li>
					<a href="javascript:void(0)"><span class="icon-home"></span>Home</a>
					<ul>
						<li><a href="index.php">Home</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)"><span class="icon-dashboard"></span>Master</a>
					<ul>
						<li><a href="pegawai.php">Pegawai</a></li>
						<li><a href="nasabah.php">Nasabah</a></li>
						<li><a href="jaminan.php">Jaminan</a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)"><span class="icon-th-list"></span>Transaksi</a>
					<ul>
						<li><a href="transaksi_peminjaman.php">Transaksi Peminjaman</a></li>
						<li><a href="transaksi_gadai.php">Transaksi Pegadaian</a></li>
						<li><a href="transaksi_investasi.php">Transaksi Investasi</a></li>
						<li>
							<a href="javascript:void(0)">Pencairan Dana</a>
							<ul>
								<li><a href="transaksi_investasi_pencairan_sukses.php">Pencairan Dana Sukses</a></li>
								<li><a href="transaksi_investasi_pencairan_pending.php">Pencairan Dana Pending</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)"><span class="icon-money"></span>Angsuran</a>
					<ul>
						<li><a href="angsuran_transaksi_peminjaman.php">Detail Histori Angsuran Peminjaman</a></li>
						<li><a href="angsuran_transaksi_gadai.php">Detail Angsuran Pegadaian</a></a></li>
					</ul>
				</li>
				<li>
					<a href="javascript:void(0)"><span class="icon-book"></span>Rekapitulasi</a>
					<ul>
						<li><a href="laporan_pemasukan_dana.php">Laporan Pemasukan Dana</a></li>
						<li><a href="laporan_pemasukan_dana.php">Laporan Pengeluaran Dana</a></li>
						<li><a href="laporan_pemasukan_dana.php">Laporan Biaya Administrasi</a></li>
						<li><a href="laporan_pemasukan_dana.php">Laporan Denda</a></li>
						<li><a href="laporan_pemasukan_dana.php">Laporan Pemasukan Dana</a></li>	
					</ul>
				</li>				
			</ul>
		</div>
	</nav>