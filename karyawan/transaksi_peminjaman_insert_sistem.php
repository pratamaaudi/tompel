<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 

if(isset($_POST['sel_denda'])){
	$np = $_POST['sel_denda'];
	echo '<option value="0">--pilih--</option>';
	$result = mysqli_query($conn, "SELECT * FROM denda WHERE s_hapus = 0");
	if (!$result) { die("SQL Error Result "); }
	while ($row = mysqli_fetch_array($result)) {
		if($row['min_pinjam'] <= $np && $np <= $row['max_pinjam']){
			echo '<option value="' . $row['id_denda'] . '" selected="">' . $row['denda_hari'] . '</option>';
		} else {
			echo '<option value="' . $row['id_denda'] . '">' . $row['denda_hari'] . '</option>';
		}
	}
}

// QUERY MENAMPILKAN DATA NASABAH
if(isset($_POST['isi_data_nasabah'])){
	$id_nasabah = $_POST['id_nasabah'];
	$result = mysqli_query($conn, "SELECT * FROM nasabah WHERE id = $id_nasabah");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	echo $row['no_ktp'] . "-" . $row['nama'] . "-" . $row['alamat'] . "-" . $row['nama_pekerjaan'] . "-" . $row['alamat_pekerjaan'] . "-" . $row['telp'] . "-" . $row['no_hp'];
}
if(isset($_POST['isi_data_kendaraan'])){
	$id_kendaraan = $_POST['id_kendaraan'];
	$result = mysqli_query($conn, "SELECT * FROM jenis_jaminan WHERE id = $id_kendaraan");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	$status = 'AN';
	if($row['status'] == 1){ $status = 'OL'; }

	$jenis = '';
	if($row['jenis'] == 1){ $jenis = 'Roda Dua'; }
	else if($row['jenis'] == 2){ $jenis = 'Roda Tiga'; }
	else if($row['jenis'] == 3){ $jenis = 'Roda Empat'; }

	echo "-" . $row['nama_pemilik_stnk'] . "-" . $status . "-" . $jenis . "-" . $row['no_polisi'] . "-" . $row['tahun_kendaraan'] . "-" . $row['no_mesin'] . "-" . $row['no_rangka'];
}
if (isset($_POST['insert_transaksi_peminjaman'])) {
	$id_pegawai = $_SESSION['login_pegawai'];
	$id_nasabah = $_POST['nasabah'];
	$id_jenis_jaminan = $_POST['kendaraan'];
	$id_suku_bunga = $_POST['suku_bunga'];

	$tanggal_pinjam = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	$status_transaksi = $_POST['status_transaksi'];
	$status_jaminan = $_POST['status_jaminan'];
	$jaminan = $_POST['jaminan'];
	$jumlah_angsuran = $_POST['nominal_angsuran'];
	$biaya_administrasi = $_POST['biaya_administrasi'];
	$total = $_POST['total'];
	$denda_hari = $_POST['denda'];
	$status_cair = $_POST['status_cair'];
	$tgl_cair = date('Y-m-d h:i:s');




	
	$sql1 = "INSERT INTO `transaksi_peminjaman`(`tanggal_pinjam`, `jangka_waktu`, `jumlah_pinjaman`, `status_transaksi`, `status_jaminan`, `jaminan`, `jumlah_angsuran`, `biaya_administrasi`, `total`, `id_denda`, `status_cair`";
	if($status_cair != 0){
		$sql1 = $sql1 . ", `tgl_cair`";
	}
	$sql1 = $sql1 . ", `pegawai_id`, `nasabah_id`, `jenis_jaminan_id`) VALUES ('$tanggal_pinjam', $jangka_waktu, $jumlah_pinjaman, $status_transaksi, $status_jaminan, $jaminan, $jumlah_angsuran, $biaya_administrasi, $total, $denda_hari, $status_cair";
	if($status_cair != 0){
		$sql1 = $sql1 . ", '$tgl_cair'";
	}
	$sql1 = $sql1 . ", $id_pegawai, $id_nasabah, $id_jenis_jaminan)";
	$result1 = mysqli_query($conn, $sql1);
	if(!$result1){ die("SQL ERROR : Result1"); }

	$result2 = mysqli_query($conn, "SELECT * FROM transaksi_peminjaman WHERE tanggal_pinjam = '$tanggal_pinjam'");
	if(!$result2){ die("SQL ERROR : Result2"); }
	$row2 = mysqli_fetch_array($result2);
	$no_transaksi_peminjaman = $row2['no_kontrak'];

	$result3 = mysqli_query($conn, "INSERT INTO `perubahan_suku_bunga`(`id_suku_bunga`, `id_transaksi_peminjaman`, `tgl_perubahan_suku_bunga`) VALUES ($id_suku_bunga, $no_transaksi_peminjaman, '$tanggal_pinjam')");
	if(!$result3){ die("SQL ERROR : Result3"); }

	$tgl_angsur = intval(substr($tanggal_pinjam, 8, 2));
	$bln_angsur = intval(substr($tanggal_pinjam, 5, 2));
	$thn_angsur = intval(substr($tanggal_pinjam, 0, 4));
	$wkt_angsur = substr($tanggal_pinjam, 11, 8);
	for($i = 1; $i <= intval($jangka_waktu); $i++){
		$bln_angsur++;
		if($bln_angsur == 13){
			$bln_angsur = 0;
			$thn_angsur++;
			$i--;
		} else {
			$last_day = $thn_angsur . "-" . $bln_angsur;
			if(strlen(strval($bln_angsur)) == 1){
				$last_day = $thn_angsur . "-0" . $bln_angsur;
			}
			$sesuai_tgl_wkt_angsur = $last_day . "-" . $tgl_angsur . " " . $wkt_angsur;
			if(strlen(strval($tgl_angsur)) == 1){
				$sesuai_tgl_wkt_angsur = $last_day . "-0" . $tgl_angsur . " " . $wkt_angsur;
			}
			$date = new DateTime($last_day);
			$date->modify('last day of this month');
			$tgl_wkt_last = $date->format('Y-m-d') . " " . $wkt_angsur;

			$tgl_wkt_pasti_angsur = '';
			if($sesuai_tgl_wkt_angsur > $tgl_wkt_last){
				$tgl_wkt_pasti_angsur = $tgl_wkt_last;
			} else {
				$tgl_wkt_pasti_angsur = $sesuai_tgl_wkt_angsur;
			}

			// QUERY UNTUK MENAMPILKAN JANGKA WAKTU PEMINJAMAN.
			$result4 = mysqli_query($conn, "INSERT INTO `transaksi_angsuran`(`tanggal_bayar`, `angsuran_ke`, `transaksi_peminjaman_no_kontrak`) VALUES ('$tgl_wkt_pasti_angsur', $i, $no_transaksi_peminjaman)");
			if(!$result4){ die("SQL ERROR : Result3"); }
		}
	}

	//if ($result1) { 
	header('Location: transaksi_peminjaman.php'); 
	//} else { 
	//	die("SQL ERROR : Result1"); 
	//}
}
ob_end_flush(); ?>