<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	
	<!-- BOOTSTRAP FOR SEARCHING -->
	<link  rel="stylesheet" href="../js/lib/dataTables/media/DT_bootstrap.css">
	<link rel="stylesheet" href="../js/lib/dataTables/extras/TableTools/media/css/TableTools.css">
</head>
<body class="sidebar_hidden">
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">		
											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>

					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li class="active">             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<!--ul>
					<li><a href="#">Ebro Admin</a></li>
					<li><span>Dashboard</span></li>						
				</ul-->
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); padding: 20px 5px 20px 5px">
				<div id="main_content">
					<!-- main content -->
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: rgb(0, 128, 128)">
								<h4 class="panel-title" style="color: white"><strong>SEMUA ANGSURAN TRANSAKSI PEMINJAMAN</strong></h4>
							</div>
							<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table id="dt_basic" class="table table-striped">
									<thead>
										<tr>
											<th>No Kontrak</th>
											<th>Jatuh Tempo</th>
											<th>Tanggal Pembayaran</th>
											<th>Nama Nasabah</th>
											<th>Jumlah Bayar</th>
											<th>Jumlah Denda</th>
											<th>Keterangan</th>
											<th>Bukti</th>
											<th>Alat</th>
										</tr>
									</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php 
										$rno = 0;
										$result3 = mysqli_query($conn, "SELECT tp.no_kontrak, tp.tanggal_pinjam, tp.jumlah_pinjaman, tp.jangka_waktu, tp.jumlah_angsuran, n.nama, ta.real_tgl_bayar, COUNT(angsuran_ke) AS rangkap_bulan, SUM(ta.jumlah_bayar) AS total_bayar, SUM(ta.jumlah_denda) AS total_denda, MAX(ta.status_angsuran) AS status_angsuran, ta.struk_pembayaran FROM transaksi_peminjaman tp JOIN transaksi_angsuran ta ON tp.no_kontrak = ta.transaksi_peminjaman_no_kontrak JOIN nasabah n ON tp.nasabah_id = n.id WHERE status_angsuran != 0 GROUP BY ta.real_tgl_bayar DESC");
										if (!$result3) { die("SQL Error Result3 "); }
										while ($row3 = mysqli_fetch_array($result3)) {
											$rno++;
											$no_kontrak = $row3['no_kontrak'];
											$real_tgl_bayar = $row3['real_tgl_bayar'];
											$tgl_pinjam = substr($row3['tanggal_pinjam'], 8, 2)."-".substr($row3['tanggal_pinjam'], 5, 2)."-".substr($row3['tanggal_pinjam'], 0, 4);
											$tgl_real_bayar = substr($row3['real_tgl_bayar'], 8, 2)."-".substr($row3['real_tgl_bayar'], 5, 2)."-".substr($row3['real_tgl_bayar'], 0, 4);
													/*$total_angsuran = $row3['total_bayar'];
													$total_denda = $row3['total_denda'];
													$cek_denda = '';
													if ($total_denda < 0){
														$total_denda = 0;
													}
													if($total_denda < $row3['total_denda']){
														$cek_denda = 'Denda Nunggak';
													}*/
													$rangkap_bulan_ke = '';
													$rbk = 0;
													$result9 = mysqli_query($conn, "SELECT angsuran_ke FROM transaksi_angsuran WHERE transaksi_peminjaman_no_kontrak = $no_kontrak AND real_tgl_bayar = '$real_tgl_bayar'");
													if (!$result9) { die("SQL Error Result9 "); }
													while ($row9 = mysqli_fetch_array($result9)) {
														if($rbk == 0){
															$rangkap_bulan_ke = $rangkap_bulan_ke . $row9['angsuran_ke']; 
															$rbk = 1;
														} else {
															$rangkap_bulan_ke = $rangkap_bulan_ke . "," . $row9['angsuran_ke'];
														}
													}
													$pokok = $row3['jumlah_pinjaman'] / $row3['jangka_waktu'];
													$bunga = $row3['jumlah_angsuran'] - $pokok;
													$test = '';
													?>
													<tr class="odd">
														<td><?php echo $row3['no_kontrak'] . "/" . $tgl_pinjam; ?></td>
														<td>
															<?php
															$tb = 0;
															$result10 = mysqli_query($conn, "SELECT jumlah_bayar, jumlah_denda, tanggal_bayar FROM transaksi_angsuran WHERE transaksi_peminjaman_no_kontrak = $no_kontrak AND real_tgl_bayar = '$real_tgl_bayar'");
															if (!$result10) { die("SQL Error Result10 "); }
															while ($row10 = mysqli_fetch_array($result10)) {
																$tgl_jatuh_tempo = substr($row10['tanggal_bayar'], 8, 2)."-".substr($row10['tanggal_bayar'], 5, 2)."-".substr($row10['tanggal_bayar'], 0, 4);
																if($tb == 0){
																	echo $tgl_jatuh_tempo;
																	$tb = 1;
																} else {
																	echo "<br>" . $tgl_jatuh_tempo;
																}
																if($row10['jumlah_bayar'] > $pokok){
																	if($row10['jumlah_bayar'] <= ($pokok+$bunga)){
																		if($row10['jumlah_denda'] > $row10['jumlah_bayar']-($pokok+$bunga)){
																			if($test == '')
																			{
																				$test = $test . "Denda Nunggak";

																			}
																		}
																	}
																}
															}
															?>	
														</td>
														<td><?php echo $tgl_real_bayar; ?></td>
														<td><?php echo $row3['nama']; ?></td>
														<td><?php echo $row3['total_bayar']; ?></td>
														<td><?php echo $row3['total_denda']; ?></td>
														<td>
															<?php
															$cek_col3 = 0; 
															$keterangan = '';
															if($row3['rangkap_bulan'] > 1){
																$keterangan = $keterangan . "Rangkap " . $row3['rangkap_bulan'] . " bulan (" . $rangkap_bulan_ke . ")";
															} 
															echo $keterangan . "<br>" . $test; 

															?>
														</td>
														<td id="col_confirm<?php echo $rno; ?>">
															<?php if($row3['status_angsuran'] == 4){ ?>

																<a class="btn btn-info" onclick="public_info_row(<?php echo $no_kontrak . ", '" . $real_tgl_bayar . "', '" . $row3['struk_pembayaran'] . "', " . $rno; ?>)"><i class="icon-eye-open"></i> Cek</a>

															<?php } else if($row3['status_angsuran'] == 3 || $row3['status_angsuran'] == 2){ 
																$s = mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"));
																if($tgl_real_bayar <= date("d-m-Y", $s)){ ?>

																	<a class="btn btn-warning" onclick="confirm_keputusan(<?php echo $no_kontrak . ", '" . $real_tgl_bayar . "', " . $row3['status_angsuran'] . ", '" . $row3['struk_pembayaran'] . "'"; ?>)"><i class=""></i> Konfirmasi</a>

																<?php } else {
																	if($row3['status_angsuran'] == 3){
																		echo "Di Tolak";
																	} else if($row3['status_angsuran'] == 2){
																		echo "Di Terima";
																	}
																}
															} ?>
														</td>
														<td>
															<a class="btn btn-danger" onclick="batalkan_angsuran(<?php echo $no_kontrak . ", '" . $real_tgl_bayar . "'"; ?>)"><i class="icon-close"></i> Batalkan</a>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer_space"></div>
	</div>


	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<div class="modal fade" id="modal_bukti">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Bukti Pembayaran</h4>
				</div>
				<div class="modal-body"><center>
					<img src="" alt="" style="width: 400px; height: 450px; margin-top: 5px; padding: 3px 3px 3px 3px;" id="fbukti"></center>
				</div>
				<div class="modal-footer">
					<div class="col-xs-6"></div>
					<div class="col-xs-3">
						<a class="btn btn-success btn-block" onclick="karyawan_terima_angsur()">Terima</a>
					</div>
					<div class="col-xs-3">
						<a class="btn btn-danger btn-block" onclick="karyawan_tolak_angsur()">Tolak</a>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/peity/jquery.peity.min.js"></script>
	<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="../js/lib/flot/jquery.flot.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.resize.js"></script>
	<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
	<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
	<script src="../js/pages/ebro_dashboard.js"></script>


	<!--[[ page specific plugins ]]-->
	<!-- datatables -->
	<script src="../js/lib/dataTables/media/js/jquery.dataTables.min.js"></script>
	<!-- datatables column reorder -->
	<script src="../js/lib/dataTables/extras/ColReorder/media/js/ColReorder.min.js"></script>
	<!-- datatable fixed columns -->
	<script src="../js/lib/dataTables/extras/FixedColumns/media/js/FixedColumns.min.js"></script>
	<!-- datatables column toggle visibility -->
	<script src="../js/lib/dataTables/extras/ColVis/media/js/ColVis.min.js"></script>
	<!-- datatable table tools -->
	<script src="../js/lib/dataTables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script src="../js/lib/dataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>
	<!-- datatable bootstrap style -->
	<script src="../js/lib/dataTables/media/DT_bootstrap.js"></script>
	
	<script src="../js/pages/ebro_datatables.js"></script>
	<script type="text/javascript">
		var p_no_kontrak = 0;
		var p_real_tgl_bayar = '';
		var p_struk_pembayaran = '';
		var p_rno = 0;
		function public_info_row(no_kontrak, real_tgl_bayar, struk_pembayaran, rno){
			p_no_kontrak = no_kontrak;
			p_real_tgl_bayar = real_tgl_bayar;
			p_struk_pembayaran = struk_pembayaran;
			p_rno = rno;
			document.getElementById("fbukti").src = '../gallery/bukti_pembayaran_angsuran_peminjaman/' + struk_pembayaran;
				//alert(no_kontrak + " (" + real_tgl_bayar + ") - " + struk_pembayaran);
				$('#modal_bukti').modal('show'); 
			}

			function karyawan_tolak_angsur(){
				$.post("angsuran_transaksi_peminjaman_sistem.php", {
					no_kontrak : p_no_kontrak,
					real_tgl_bayar : p_real_tgl_bayar,
					karyawan_tolak_angsur : ''
				}, function(result){
					if(result != ''){
						ubah_btn(parseInt(result));
					}
				});
			}

			function karyawan_terima_angsur(){
				$.post("angsuran_transaksi_peminjaman_sistem.php", {
					no_kontrak : p_no_kontrak,
					real_tgl_bayar : p_real_tgl_bayar,
					karyawan_terima_angsur : ''
				}, function(result){
					if(result != ''){
						ubah_btn(parseInt(result));
					}
				});
			}

			function ubah_btn(sa){
				var tulis = '';
				var t1 = p_real_tgl_bayar.toString().substring(0, 10);
				var t2 = '<?php echo date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))); ?>';
				if(t1 <= t2){ 
					tulis = '<a class="btn btn-warning" onclick="confirm_keputusan(' + p_no_kontrak + ', \'' + p_real_tgl_bayar + '\', ' + sa + ', \'' + p_struk_pembayaran + '\')"><i class=""></i> Konfirmasi</a>';
				} else {
					if(sa == 3){ 
						tulis = "Di Tolak";
					} else if(sa == 2){ 
						tulis = "Di Terima";
					}
				}
				//alert(t1 + " == " + t2);
				document.getElementById('col_confirm' + p_rno).innerHTML = tulis;
				p_no_kontrak = 0;
				p_real_tgl_bayar = '';
				p_struk_pembayaran = '';
				$('#modal_bukti').modal('hide');
			}

			function confirm_keputusan(ck_no_kontrak, ck_real_tgl_bayar, ck_status_angsuran, ck_struk_pembayaran){
				$.post("angsuran_transaksi_peminjaman_sistem.php", {
					no_kontrak : ck_no_kontrak,
					real_tgl_bayar : ck_real_tgl_bayar,
					status_angsuran : ck_status_angsuran,
					struk_pembayaran : ck_struk_pembayaran,
					nasabah_konfirmasi_angsur : ''
				}, function(result){
					if(result == ''){
						document.location.reload(true);
					}
				});
			}

			function batalkan_angsuran(ba_nk, ba_rtb){
				$.post("angsuran_transaksi_peminjaman_sistem.php", {
					no_kontrak : ba_nk,
					real_tgl_bayar : ba_rtb,
					batalkan_angsuran : ''
				}, function(result){
					if(result == ''){
						document.location.reload(true);
					} else {
						alert(result);
					}
				});
			}
		</script>

		<div class="style_items" id="sidebar_switch">
			<p class="style_title">Sidebar position</p>
			<label class="radio-inline">
				<input type="radio" name="sidebar_position" id="sidebar_left" value="left" checked> Left
			</label>
			<label class="radio-inline">
				<input type="radio" name="sidebar_position" id="sidebar_right" value="right"> Right
			</label>
		</div>
	</div>
</body>
</html>


<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}

function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}

function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}


?>
<?php ob_end_flush(); ?>