<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
  header("Location: login.php");
}
require '../config.php'; 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
  <title>Ebro Admin Template v1.3</title>

  <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
  <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/todc-bootstrap.min.css">
  <link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../img/flags/flags.css">
  <link rel="stylesheet" href="../css/retina.css">
  <link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
  <link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">  
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/jquery.dm-uploader.min.css">
  <link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
  <link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous"> -->

    <!-- Custom styles -->
    <link rel="stylesheet" href="../css/jquery.dm-uploader.min.css">
    <!-- <link href="../dist/css/jquery.dm-uploader.min.css" rel="stylesheet"> -->
    <!-- <link href="styles.css" rel="stylesheet"> -->
  </head>

  <body>

    <main role="main" class="container">

      <div class="row">
        <div class="col-md-6 col-sm-12">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script> -->


    <script src="../js/jquery.dm-uploader.min.js"></script>
    <script src="../js/demo-ui.js"></script>
    <script src="../js/demo-config.js"></script>
    <!-- <script src="../dist/js/jquery.dm-uploader.min.js"></script> -->
    <!-- <script src="demo-ui.js"></script> -->
    <!-- <script src="demo-config.js"></script> -->

    <!-- File item template -->
    <script type="text/html" id="files-template">
      <li class="media">
        <div class="media-body mb-1">
          <p class="mb-2">
            <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
          </p>
          <div class="progress mb-2">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
              role="progressbar"
              style="width: 0%" 
              aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            </div>
          </div>
          <hr class="mt-1 mb-1" />
        </div>
      </li>
    </script>

    <!-- Debug item template -->
    <script type="text/html" id="debug-template">
      <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
    </script>


    <body class="sidebar_hidden">
      <?php
  $nasabah = 0;
  $noPolisi = '';
  $namaPemilikSTNK = '';
  $merk = '';
  $type = '';
  $jenis = 0;
  $model = '';
  $thnPembuatan = date('Y');
  $isiSilinder = '';
  $noRangka = '';
  $noMesin = '';
  $warna = '';
  $thnRegistrasi = date('Y');
  $thnKendaraan = date('Y');
  $status = 0;
  if (isset($_POST['insert_jaminan_kendaraan'])) {
    $nasabah = $_POST['nasabah'];
    $noPolisi = $_POST['noPolisi'];
    $namaPemilikSTNK = $_POST['namaPemilikSTNK'];
    $merk = $_POST['merk'];
    $type = $_POST['type'];
    $jenis = $_POST['jenis'];
    $model = $_POST['model'];
    $thnPembuatan = $_POST['thnPembuatan'];
    $isiSilinder = $_POST['isiSilinder'];
    $noRangka = $_POST['noRangka'];
    $noMesin = $_POST['noMesin'];
    $warna = $_POST['warna'];
    $thnRegistrasi = $_POST['thnRegistrasi'];
    $thnKendaraan = $_POST['thnKendaraan'];
    $status = $_POST['status'];
    
    $fd = explode(".", $_FILES['fotoDocumen']['name']);
    $fotoDocumen = substr(md5($_FILES['fotoDocumen']['name'] . time()), 0, 10) . "." . $fd[count($fd) - 1];
    move_uploaded_file($_FILES['fotoDocumen']['tmp_name'], "../gallery/jaminan_kendaraan/" . $fotoDocumen);

    $result1 = mysqli_query($conn, "INSERT INTO `jenis_jaminan`(`jenis`, `no_mesin`, `tahun_kendaraan`, `no_polisi`, `merk`, `type`, `model`, `tahun_pembuatan`, `isi_silinder`, `warna`, `tahun_registrasi`, `foto_dokumen`, `status`, `nama_pemilik_stnk`, `no_rangka`, `shapus`, `nasabah_id`) VALUES ($jenis, '$noMesin', $thnKendaraan, '$noPolisi', '$merk', '$type', '$model', $thnPembuatan, '$isiSilinder', '$warna', $thnRegistrasi, '$fotoDocumen', $status, '$namaPemilikSTNK', '$noRangka', 0, $nasabah)");
    if ($result1) {
      $_SESSION['jaminan'] = 'kendaraan'; 
      header('Location: jaminan.php'); 
    } else { 
      die("SQL ERROR : Result1"); 
    }
  }
  ?>

  <div id="wrapper_all">
    <header id="top_header">
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-sm-2">
            <a href="dashboard1.html" class="logo_main" title="Ebro Admin Template:"><img src="img/logo_main.png" alt=""></a>
          </div>
          <div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
            <div class="notification_dropdown dropdown">
              <a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
                
                <i class="icon-comment-alt icon-2x"></i>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <div class="dropdown_heading">Comments</div>
                  <div class="dropdown_content">
                    <ul class="dropdown_items">
                      <li>
                        <h3><span class="small_info">12:43</span><a href="#">Lorem ipsum dolor&hellip;</a></h3>
                        <p>Lorem ipsum dolor sit amet&hellip;</p>
                      </li>
                      <li>
                        <h3><span class="small_info">Today</span><a href="#">Lorem ipsum dolor&hellip;</a></h3>
                        <p>Lorem ipsum dolor sit amet&hellip;</p>
                      </li>
                      <li>
                        <h3><span class="small_info">14 Aug</span><a href="#">Lorem ipsum dolor&hellip;</a></h3>
                        <p>Lorem ipsum dolor sit amet&hellip;</p>
                      </li>
                    </ul>
                  </div>
                  <div class="dropdown_footer"><a href="#" class="btn btn-sm btn-default">Show all</a></div>
                </li>
              </ul>
            </div>
            <div class="notification_separator"></div>
            <div class="notification_dropdown dropdown">
              <a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
                
                <i class="icon-envelope-alt icon-2x"></i>
              </a>
              <ul class="dropdown-menu dropdown-menu-wide">
                <li>
                  <div class="dropdown_heading">Messages</div>
                  <div class="dropdown_content">
                    <ul class="dropdown_items">
                      <li>
                        <h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                        <p class="large_info">Sean Walter, 24.05.2014</p>
                        <i class="icon-exclamation-sign indicator"></i>
                      </li>
                      <li>
                        <h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
                        <p class="large_info">Sean Walter, 24.05.2014</p>
                      </li>
                      <li>
                        <h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
                        <p class="large_info">Sean Walter, 24.05.2014</p>
                        <i class="icon-exclamation-sign indicator"></i>
                      </li>
                    </ul>
                  </div>
                  <div class="dropdown_footer">
                    <a href="#" class="btn btn-sm btn-default">Show all</a>
                    <div class="pull-right dropdown_actions">
                      <a href="#"><i class="icon-refresh"></i></a>
                      <a href="#"><i class="icon-cog"></i></a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-xs-6 col-sm-push-4 col-sm-3">
            <div class="pull-right dropdown">
              <a href="#" class="user_info dropdown-toggle" title="Jonathan Hay" data-toggle="dropdown">
                <img src="../gallery/smile.png" alt="">
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="profil_user.php">Profile</a></li>
                
                <li><a href="logout.php">Log Out</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-pull-6 col-sm-4">
            <form class="main_search">
              <input type="text" id="search_query" name="search_query" class="typeahead form-control">
              <button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
            </form> 
          </div>
        </div>
      </div>
    </header>
    <nav id="top_navigation">
      <div class="container">
        <ul id="icon_nav_h" class="top_ico_nav clearfix">
          <li>
            <a href="index.php">
              <i class="icon-home icon-2x"></i>
              <span class="menu_label">Home</span>
            </a>
          </li>
          <li>             
            <a href="nasabah.php">
              <i class="icon-male icon-2x"></i>
              <span class="menu_label">Nasabah</span>
            </a>
          </li>
          <li>             
            <a href="pegawai.php">
              <i class="icon-group icon-2x"></i>
              <span class="menu_label">Karyawan</span>
            </a>
          </li>
          <li>             
            <a href="jaminan.php">
              <i class="icon-book icon-2x"></i>
              <span class="menu_label">Jaminan</span>
            </a>
          </li>
          <li>             
            <a href="pendataan.php">
              
              <i class="icon-tasks icon-2x"></i>
              <span class="menu_label">Pendataan</span>
            </a>
          </li>
          
          <li>             
            <a href="bunga.php">
              <i class="icon-beaker icon-2x"></i>
              <span class="menu_label">Bunga</span>
            </a>
          </li>
          <li>             
            <a href="setting.php">
              <i class="icon-wrench icon-2x"></i>
              <span class="menu_label">Settings</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- mobile navigation -->
    <nav id="mobile_navigation"></nav>

    <section id="breadcrumbs">
      <div class="container">
        <ul>
          <li><a href="jaminan.php">Jaminan</a></li>
          <li><span>Tambah Kendaraan Jaminan</span></li>            
        </ul>
      </div>
    </section>

    <section class="container clearfix main_section">
      <div id="main_content_outer" class="clearfix">
        <div id="main_content">
          <!-- main content -->
          <div class="row">
            <div class="col-sm-12">
              <div class="col-sm-12">
                <center>
                  <div class="user_actions pull-right"></div>
                  <h2>TAMBAH KENDARAAN JAMINAN</h2> 
                </center>
              </div>
              <div class="user_content">
                <div class="row">
                  <div class="col-sm-10 col-sm-offset-1">
                    <form class="form-horizontal user_form" method="POST" enctype="multipart/form-data">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Nasabah</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select id="nasabah" onchange="isi_data_nasabah()" name="nasabah" class="form-control">
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">No Polisi</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="noPolisi" value="<?php echo $noPolisi; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Pemilik STNK</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="namaPemilikSTNK" value="<?php echo $namaPemilikSTNK; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Merk</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="merk" value="<?php echo $merk; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Type</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="type" value="<?php echo $type; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select name="jenis" class="form-control">
                              <option value="0"<?php if($jenis == 0){echo ' selected=""';} ?>>
                                --pilih--
                              </option>
                              <option value="1"<?php if($jenis == 1){echo ' selected=""';} ?>>
                                Roda Dua
                              </option>
                              <option value="2"<?php if($jenis == 2){echo ' selected=""';} ?>>
                                Roda Tiga
                              </option>
                              <option value="3"<?php if($jenis == 3){echo ' selected=""';} ?>>
                                Roda Empat
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Model</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="model" value="<?php echo $model; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun Pembuatan</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select id="thnPembuatan" name="thnPembuatan" class="form-control">
                              <!--script type="text/javascript">
                                isiThn(parseInt('<?php echo $thnPembuatan; ?>'), 1);
                              </script-->
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Isi Silinder</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="isiSilinder" value="<?php echo $isiSilinder; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">No Rangka</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="noRangka" value="<?php echo $noRangka; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">No Mesin</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="noMesin" value="<?php echo $noMesin; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Warna</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <input type="text" class="form-control" name="warna" value="<?php echo $warna; ?>" required="">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun Registrasi</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select id="thnRegistrasi" name="thnRegistrasi" class="form-control">
                              <!--script type="text/javascript">
                                isiThn(parseInt('<?php echo $thnRegistrasi; ?>'), 2);
                              </script-->
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun Kendaraan</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select id="thnKendaraan" name="thnKendaraan" class="form-control">
                              <!--script type="text/javascript">
                                isiThn(parseInt('<?php echo $thnKendaraan; ?>'), 3);
                              </script-->>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <select name="status" class="form-control">
                              <?php if($status == 0){ ?>
                                <option value="0" selected="">AN</option>
                                <option value="1">OL</option>
                              <?php } else { ?>
                                <option value="0">AN</option>
                                <option value="1" selected="">OL</option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Foto Dokumen</label>
                        <div class="col-sm-10 editable">
                          <div>
                            <!-- <input type="file" name="fotoDocumen" required=""> -->
                            <!-- Our markup, the important part here! -->
          <div id="drag-and-drop-zone" class="dm-uploader p-5">
            <h3 class="mb-5 mt-5 text-muted">Drag &amp; drop files here</h3>

            <div class="btn btn-primary btn-block mb-5">
                <span>Open the file Browser</span>
                <input type="file" title='Click to add Files' />
            </div>
          </div><!-- /uploader -->

        </div>
        <div class="col-md-6 col-sm-12">
          <div class="card h-100">
            <div class="card-header">
              File List
            </div>

            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
              <li class="text-muted text-center empty">No files uploaded.</li>
            </ul>
          </div>
        </div>
      </div><!-- /file list -->
                          </div>
                        </div>
                      </div>
                      <div class="form_submit clearfix">
                        <div class="row">
                          <div class="col-sm-10 col-sm-offset-2">
                            <input type="hidden" class="form-control" name="insert_jaminan_kendaraan">
                            <button type="submit" class="btn btn-primary btn-lg">SIMPAN</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    <div id="footer_space"></div>
  </div>
    </body>
  </body>

  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">&copy; 2013 Your Company</div>
        <div class="col-sm-8"></div>
        <div class="col-sm-1 text-right"><small class="text-muted">ARTA MULIA</small></div>
      </div>
    </div>
  </footer>

  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/jquery.ba-resize.min.js"></script>
  <script src="../js/jquery_cookie.min.js"></script>
  <script src="../js/retina.min.js"></script>
  <script src="../js/lib/typeahead.js/typeahead.min.js"></script>
  <script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
  <script src="../js/tinynav.js"></script>
  <script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
  <script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
  <script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
  <script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
  <script src="../js/ebro_common.js"></script>
  <script src="../js/lib/bootbox/bootbox.min.js"></script>
  <script src="../js/pages/ebro_user_profile.js"></script>
  <script src="../js/jquery.dm-uploader.min.js"></script>
  <script src="../js/demo-ui.js"></script>
  <script src="../js/demo-config.js"></script>
  <script type="text/javascript">
    isiThn(<?php echo $thnPembuatan; ?>, 1);
    isiThn(<?php echo $thnRegistrasi; ?>, 2);
    isiThn(<?php echo $thnKendaraan; ?>, 3);

    function isiThn(thnPilih, id){
      var batas = parseInt("<?php echo date('Y'); ?>");
      var optIsi = '';
      for(var i = 1945; i <= batas; i++){
        if(i == thnPilih){
          optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
        } else {
          optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
        }
      }
      if(id == 1){
        document.getElementById('thnPembuatan').innerHTML = '';
        document.getElementById('thnPembuatan').innerHTML = optIsi;
      } else if(id == 2){
        document.getElementById('thnRegistrasi').innerHTML = '';
        document.getElementById('thnRegistrasi').innerHTML = optIsi;
      } else {
        document.getElementById('thnKendaraan').innerHTML = '';
        document.getElementById('thnKendaraan').innerHTML = optIsi;
      }
    }
    isiCboNasabah(parseInt(<?php echo $nasabah; ?>));
    function isiCboNasabah(id_nasabah){
      var optIsi = '';

      // UNTUK OPTION PILIH 
      optIsi = optIsi + '<option value="" selected="">--pilih--</option>';
      <?php
      $result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
      if (!$result1) { die("SQL Error Result1 "); }
      if (mysqli_num_rows($result1)) {
        while ($allRow1 = mysqli_fetch_array($result1)) {
          ?>
          var id = parseInt(<?php echo $allRow1['id']; ?>);
          var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
          if(id == id_nasabah){
            optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
          } else {
            optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
          }
        <?php }} ?>

        document.getElementById('nasabah').innerHTML = '';
        document.getElementById('nasabah').innerHTML = optIsi;
      }
    </script>
</html>
