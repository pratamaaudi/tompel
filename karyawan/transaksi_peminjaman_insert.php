<?php
ob_start();
session_start();
// date_default_timezone_set('Asia/Jakarta');
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php
	$nasabah = 0;
	$kendaraan = 0;
	$suku_bunga = 0;
	$denda = 0;
	$jangka_waktu = 0;
	$tgl_waktu_sekarang = date('Y-m-d h:i:s');
	$tws = explode(" ", $tgl_waktu_sekarang);
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black">ARTA MULIA</strong></a>	
						</div>	
					</div>

					
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">

											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>


					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>          
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 12.5%">
				<ul>
					<li><a href="pendataan.php">Pendataan</a></li>
					<li><a href="transaksi_peminjaman.php">Transaksi Peminjaman</a></li>
					<li><span>Tambah Transaksi Peminjaman</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px">
				<div id="main_content">

					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="user_heading">
								<div class="row">
									<div class="col-sm-1 hidden-xs"></div>
									<div class="col-sm-10">
										<div class="user_heading_info">
											<center>
												<div class="user_actions pull-right"></div>
												<h1 style="color: white; font-size: 300%" >Transaksi Peminjaman</h1>
											</center>
										</div>
									</div>
								</div>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-2">
										<form class="form-horizontal user_form" action="transaksi_peminjaman_insert_sistem.php" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-9 control-label" style="color: white">Tanggal</label>
												<div class="col-sm-3 editable">
													<div>
														<input id="tgl_sekarang" type="date" class="form-control" name="tgl_sekarang" readonly="" value="<?php echo $tws[0]; ?>">
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Nasabah</strong></h3>
											<div class="form-group">
												<label class="col-sm-2 control-label"; style="color: white">Nasabah :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="nasabah" onchange="isi_data_nasabah()" name="nasabah" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">NIK :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="noKtp" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nama Lengkap :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="nama" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Alamat :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="alamat" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Pekerjaan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="namaPekerjaan" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Alamat Kerja :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="alamatPekerjaan" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No. Telpon :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="telp" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No. Handphone :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="noHp" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Kendaraan</strong> 
												<a href="jaminan_kendaraan_insert.php?ikpi=1" class="btn" style="float: right; background-color: white">
													<i class="icon-plus-sign-alt" style=" font-color:white"></i> TAMBAH KENDARAAN
												</a>
											</h3>
											<!-- COMBO BOX KENDARAAN -->
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="kendaraan" onchange="isi_data_kendaraan()" name="kendaraan" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-10 editable">
													<div>
														<p id="no_bpkb" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Atas Nama STNK :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="nama_pemilik_stnk" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="status_kendaraan" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jenis Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="jenis_kendaraan" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No Polisi :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_polisi" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white" >Tahun Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="thn_kendaraan" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No. Mesin :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_mesin" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-bottom: 15px;">
												<label class="col-sm-2 control-label" style="color: white">No. Rangka :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_rangka" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jaminan :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="jaminan" name="jaminan" class="form-control">
															<option value="0">BPKB</option>
															<option value="1">KENDARAAN</option>
															<option value="2">BPKB + KENDARAAN</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Jaminan :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="status_jaminan" name="status_jaminan" class="form-control">
															<option value="0">Di Tahan</option>
															<option value="1">Di Sita</option>
															<option value="2">Di Kembalikan</option>
														</select>
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Terusan</strong></h3>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nominal Pinjaman :</label>
												<div class="col-sm-4 editable">
													<div>
														<input id="nominal_pinjaman" type="number" class="form-control" min="0" name="nominal_pinjaman" required="" data-thousands=".">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jangka Waktu :</label>
												<div class="col-sm-3 editable">
													<div>
														<select id="jangka_waktu" name="jangka_waktu" class="form-control" style="width: 70%; float: left;">
														</select>
														<label style="color: white">&nbsp / BULAN</label>
														
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Suku Bunga :</label>
												<div class="col-sm-3 editable">
													<div>
														<select id="suku_bunga" name="suku_bunga" class="form-control" style="width: 70%; float: left;">
														</select>
														<label style="color: white"><strong>&nbsp %</strong></label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Transaksi :</label>
												<div class="col-sm-4 editable">
													<div>
														<select id="status_transaksi" name="status_transaksi" class="form-control">
															<option value="1">Belum Lunas</option>
															<option value="0">Lunas</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Uang :</label>
												<div class="col-sm-4 editable">
													<div>
														<select id="status_cair" name="status_cair" class="form-control">
															<option value="0">Belum Cair</option>
															<option value="1">Sudah Cair</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-10 editable">
													<div>
														<a class="btn btn-danger" style="float: right;" onclick="isiBiayaTotal()"><i class="icon-refresh"></i> SINKRONISASI</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Biaya Administrasi :</label>
												<div class="col-sm-9 editable">
													<div>
														<input id="biaya_administrasi" type="number" class="form-control" name="biaya_administrasi" style="width: 70%; float: left;" readonly>
														<label style="color: white"><strong>&nbsp 5% * NOMINAL PEMINJAMAN</strong></label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Total Pengembalian :</label>
												<div class="col-sm-8 editable">
													<div>
														<input id="total" type="number" class="form-control" name="total" readonly>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nominal Angsuran :</label>
												<div class="col-sm-4 editable">
													<div>
														<input id="nominal_angsuran" type="number" class="form-control" name="nominal_angsuran" style="width: 70%; float: left;" readonly>
														<label style="color: white">&nbsp /BULAN</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Denda Cicilan :</label>
												<div class="col-sm-4 editable">
													<div>
														<!--input id="denda" type="number" class="form-control" name="denda" style="width: 70%; float: left;"-->

														<!-- MEMILIH DENDA YANG SUDAH DIBUAT  -->
														<select id="denda" name="denda" class="form-control" style="width: 70%; float: left;">
														</select>



														
														<label style="color: white">&nbsp /HARI</label>
													</div>
												</div>
											</div>
											<div class="form_submit clearfix">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<input name="wkt_sekarang" type="hidden" value="<?php echo $tws[1]; ?>">
														<input name="insert_transaksi_peminjaman" type="hidden">
														<button type="submit" class="btn btn-success btn-success"><i class="icon-save" style="color: white"></i> SIMPAN</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>

	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-3"><strong>&copy; 2013 Your Company</strong></div>
				<div class="col-sm-7"></div>
				<div class="col-sm-2 text-right"><small class="text-muted" style="color: black"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>



	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		// change location nik or name
		isiCboNasabah(parseInt(<?php echo $nasabah; ?>));
		function isiCboNasabah(id_nasabah){
			var optIsi = '';
			optIsi = optIsi + '<option value="" selected="">--pilih--</option>';
			<?php
			$result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.User_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
			if (!$result1) { die("SQL Error Result1 "); }
			if (mysqli_num_rows($result1)) {
				while ($allRow1 = mysqli_fetch_array($result1)) {
					?>
					var id = parseInt(<?php echo $allRow1['id']; ?>);
					var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
					if(id == id_nasabah){
						optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
					} else {
						optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
					}
				<?php }} ?>

				document.getElementById('nasabah').innerHTML = '';
				document.getElementById('nasabah').innerHTML = optIsi;
				isi_data_nasabah();
			}

			function isi_data_nasabah(){ 
				var id_nasabah = $('#nasabah').val();
				if(id_nasabah != 0){
					$.post("transaksi_peminjaman_insert_sistem.php", {
						id_nasabah : id_nasabah,
						isi_data_nasabah : ''
					}, function(result){
						if(result != ''){
							var isi1 = result.split("-");;
							document.getElementById('noKtp').innerHTML = isi1[0];
							document.getElementById('nama').innerHTML = isi1[1];
							document.getElementById('alamat').innerHTML = isi1[2];
							document.getElementById('namaPekerjaan').innerHTML = isi1[3];
							document.getElementById('alamatPekerjaan').innerHTML = isi1[4];
							document.getElementById('telp').innerHTML = isi1[5];
							document.getElementById('noHp').innerHTML = isi1[6];
							isiCboKendaraan(id_nasabah);
						} else {
							alert('Error');
						}
					}); 
				} else {
					document.getElementById('noKtp').innerHTML = '';
					document.getElementById('nama').innerHTML = '';
					document.getElementById('alamat').innerHTML = '';
					document.getElementById('namaPekerjaan').innerHTML = '';
					document.getElementById('alamatPekerjaan').innerHTML = '';
					document.getElementById('telp').innerHTML = '';
					document.getElementById('noHp').innerHTML = '';
				}
			}
		</script>
		<script type="text/javascript">
			function isiCboKendaraan(id_nasabah){
				var optIsi = '';
				<?php
				$resultK = mysqli_query($conn, "SELECT * FROM jenis_jaminan WHERE shapus = 0 ORDER BY no_polisi ASC");
				if (mysqli_num_rows($resultK)) {
					while ($rowK = mysqli_fetch_array($resultK)) { 
						$tulis = $rowK['no_polisi']."(".$rowK['model']." ".$rowK['merk']."-".$rowK['type'].")";
						$idK = $rowK['id'];
						$rcst = mysqli_query($conn, "SELECT * FROM transaksi_peminjaman WHERE jenis_jaminan_id = $idK AND status_transaksi != 0");
						if (!mysqli_num_rows($rcst)) {
							?>
							if(id_nasabah == <?php echo $rowK['nasabah_id']; ?>){
								optIsi = optIsi + '<option value="<?php echo $idK; ?>"><?php echo $tulis; ?></option>';
							}
							<?php 
						}
					}
				} ?>
				document.getElementById('kendaraan').innerHTML = '';
				document.getElementById('kendaraan').innerHTML = optIsi;
				isi_data_kendaraan();
			}

			function isi_data_kendaraan(){ 
				var id_kendaraan = $('#kendaraan').val();
				if(id_kendaraan != null){
					$.post("transaksi_peminjaman_insert_sistem.php", {
						id_kendaraan : id_kendaraan,
						isi_data_kendaraan : ''
					}, function(result){
						if(result != ''){
	            //alert(result);
	            var isi1 = result.split("-");;
	            document.getElementById('no_bpkb').innerHTML = isi1[0];
	            document.getElementById('nama_pemilik_stnk').innerHTML = isi1[1];
	            document.getElementById('status_kendaraan').innerHTML = isi1[2];
	            document.getElementById('jenis_kendaraan').innerHTML = isi1[3];
	            document.getElementById('no_polisi').innerHTML = isi1[4];
	            document.getElementById('thn_kendaraan').innerHTML = isi1[5];
	            document.getElementById('no_mesin').innerHTML = isi1[6];
	            document.getElementById('no_rangka').innerHTML = isi1[7];
	        } else {
	        	alert('Error');
	        }
	    }); 
				} else {
					document.getElementById('no_bpkb').innerHTML = '';
					document.getElementById('nama_pemilik_stnk').innerHTML = '';
					document.getElementById('status_kendaraan').innerHTML = '';
					document.getElementById('jenis_kendaraan').innerHTML = '';
					document.getElementById('no_polisi').innerHTML = '';
					document.getElementById('thn_kendaraan').innerHTML = '';
					document.getElementById('no_mesin').innerHTML = '';
					document.getElementById('no_rangka').innerHTML = '';
				}
			}
		</script>
		<script type="text/javascript">
			isiCboSukuBunga(parseInt(<?php echo $suku_bunga; ?>));
			function isiCboSukuBunga(id_suku_bunga){
				var optIsi = '';
				<?php
				$resultSB = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 0 AND shapus = 0 ORDER BY jumlah_bunga ASC");
				if (!$resultSB) { die("SQL Error ResultSB "); }
				if (mysqli_num_rows($resultSB)) {
					while ($rowSB = mysqli_fetch_array($resultSB)) {
						?>
						var id = <?php echo $rowSB['id']; ?>;
						var bunga = <?php echo $rowSB['jumlah_bunga']; ?>;
						if(id == id_suku_bunga){
							optIsi = optIsi + '<option value="' + id + '" selected="">' + bunga + '</option>';
						} else {
							optIsi = optIsi + '<option value="' + id + '">' + bunga + '</option>';
						}
						<?php 
					}
				} ?>
				document.getElementById('suku_bunga').innerHTML = '';
				document.getElementById('suku_bunga').innerHTML = optIsi;
			}
		</script>
		<!--script type="text/javascript">
			isiCboDenda(parseInt(<?php echo $denda; ?>));
			function isiCboDenda(id_denda){
				var optIsi = '';
				optIsi = optIsi + '<option value="" selected="">--pilih--</option>';
				<?php
				$resultSB = mysqli_query($conn, "SELECT * FROM denda WHERE s_hapus = 0 ORDER BY denda_hari ASC");
				if (!$resultSB) { die("SQL Error ResultSB "); }
				if (mysqli_num_rows($resultSB)) {
					while ($rowSB = mysqli_fetch_array($resultSB)) {
						?>
						var id = <?php echo $rowSB['id_denda']; ?>;
						var denda = <?php echo $rowSB['denda_hari']; ?>;
						if(id == id_denda){
							optIsi = optIsi + '<option value="' + id + '" selected="">' + denda + '</option>';
						} else {
							optIsi = optIsi + '<option value="' + id + '">' + denda + '</option>';
						}
						<?php 
					}
				} ?>
				document.getElementById('denda').innerHTML = '';
				document.getElementById('denda').innerHTML = optIsi;
			}
		</script-->
		<script type="text/javascript">
			isiCboJangkaWaktu(parseInt(<?php echo $jangka_waktu; ?>));
			function isiCboJangkaWaktu(jangka_waktu){
				var optIsi = '';
				for(var i = 1; i <= 12; i++){
					if(i == jangka_waktu){
						optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
					} else {
						optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
					}
				}
				document.getElementById('jangka_waktu').innerHTML = '';
				document.getElementById('jangka_waktu').innerHTML = optIsi;
			}
		</script>
		<script type="text/javascript">
			function isiBiayaTotal(){
				var nominal_pinjaman = document.getElementById('nominal_pinjaman').value;
				var cboJangkaWaktu = $('#jangka_waktu').val();
				var cboSukuBunga = $("#suku_bunga").find('option:selected').text();
				if(nominal_pinjaman != '' && parseInt(nominal_pinjaman) != 0){
					var np = parseFloat(nominal_pinjaman);
					var jw = parseFloat(cboJangkaWaktu);
					var sb = parseFloat(cboSukuBunga);
					var biayaAdmin = np * 5 / 100;
					var angsuran = (np/jw) + (np*sb/100);
					var total = angsuran * jw;
					document.getElementById('biaya_administrasi').value = biayaAdmin;
					document.getElementById('total').value = total;
				// MENGAMBIL VALUE TEXTBOXT
				document.getElementById('nominal_angsuran').value = angsuran;
				$.post("transaksi_peminjaman_insert_sistem.php", {
					sel_denda: nominal_pinjaman
				}, function(result){
					if(result != ''){
						document.getElementById('denda').innerHTML = '';
						document.getElementById('denda').innerHTML = result;
					} else {
						alert('Error');
					}
				});
			} else {
				alert('Nominal Pinjaman Tidak Boleh kosong/nol');
			}
		}
	</script>
</body>
</html>


<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>