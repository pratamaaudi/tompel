<?php

ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php';

if(isset($_POST['tambah_pencairan'])){
	$jumlah = $_POST['jumlah'];
	$tanggal = $_POST['tgl_sekarang'];
	$nasbahId = $_POST['nasbahId'];
	$pegawaiId = $_POST['pegawaiId'];
	$transaksiId = $_POST['transaksiId'];

	$result3 = mysqli_query($conn, "INSERT INTO `transaksi_pencairan_dana` (`id`, `status`, `jumlah`, `tanggal`, `nasabah_id`, `pegawai_id`, `transaksi_investasi_id`) VALUES (NULL, '1', '$jumlah', '$tanggal', '$nasbahId', '$pegawaiId', '$transaksiId');");
	if(!$result3){ die("SQL ERROR : Result3"); }

	kurangiSaldoInvestasi($jumlah, $transaksiId, $conn);
	backToInvestasi();
}

function kurangiSaldoInvestasi($jumlah, $investasiId, $conn){
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `jumlah_uang` = (jumlah_uang-$jumlah) WHERE `transaksi_investasi`.`id` = $investasiId;");
    if (!$result) {
        die("SQL Error Result ");
    }
}

if(isset($_POST['insert_bunga'])){
	$bunga = $_POST['bunga'];
	$transasiInvestasiId = $_POST['transaksiId'];
	$tanggal = $_POST['tgl_sekarang'];

	$result3 = mysqli_query($conn, "INSERT INTO `perubahan_suku_bunga` (
		`id`, 
		`id_suku_bunga`, 
		`id_transaksi_peminjaman`, 
		`id_transaksi_gadai`, 
		`id_transaksi_investasi`, 
		`tgl_perubahan_suku_bunga`) 
		VALUES (
		NULL, 
		'$bunga', 
		NULL, 
		NULL, 
		'$transasiInvestasiId', 
		'$tanggal');"
	);
	if(!$result3){ die("SQL ERROR : Result3"); }
	backToInvestasi();
}

function backToInvestasi(){
	header('Location: transaksi_investasi.php');
}

?>