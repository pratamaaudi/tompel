<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php
	$nasabah = 0;
	$nama_barang = '';
	$jenis_barang = '';
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>
					
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">

											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>


					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li class="active">             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 13%">
				<ul>
					<li><a href="jaminan.php">Jaminan</a></li>
					<li><span>Tambah Barang Jaminan</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px">
				<div id="main_content">
					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-12">
								<center>
									<div class="user_actions pull-right"></div>
									<h2 style="color: white">TAMBAH BARANG JAMINAN</h2>	
								</center>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">
										<?php 
										$ibgi=0;
										if(isset($_GET['ibgi'])){
											$ibgi=1;
										}
										?>
										<form class="form-horizontal user_form" action="../gallery/jaminan_barang/buat_folder.php" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-4 control-label" style=" color: white">Nasabah</label>
												<div class="col-sm-5 editable">
													<div>
														<select id="nasabah" onchange="isi_data_nasabah()" name="nasabah" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" style=" color: white">Nama Barang</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="nama_barang" value="<?php echo $nama_barang; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" style=" color: white">Jenis Barang</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="jenis_barang" value="<?php echo $jenis_barang; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" style="color: white" >Foto Barang</label>
												<div class="col-sm-5 editable" style="color: white">
													<div>
														<input type="file" name="foto_barang[]" multiple="">
													</div>
												</div>
											</div>
											<div class="form_submit clearfix">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-4">
														<input type="hidden" class="form-control" name="insert_jaminan_barang">
														<input type="hidden" class="form-control" name="ibgi" value="<?php echo $ibgi; ?>">
														<button type="submit" class="btn btn-success btn-success"><i class="icon-save"></i> SIMPAN</button>  
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>
	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		isiCboNasabah(parseInt(<?php echo $nasabah; ?>));
		function isiCboNasabah(id_nasabah){
			var optIsi = '';

			// UNTUK OPTION PILIH 
			optIsi = optIsi + '<option value="" selected="">--pilih--</option>';
			<?php
			$result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
			if (!$result1) { die("SQL Error Result1 "); }
			if (mysqli_num_rows($result1)) {
				while ($allRow1 = mysqli_fetch_array($result1)) {
					?>
					var id = parseInt(<?php echo $allRow1['id']; ?>);
					var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
					if(id == id_nasabah){
						optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
					} else {
						optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
					}
				<?php }} ?>

				document.getElementById('nasabah').innerHTML = '';
				document.getElementById('nasabah').innerHTML = optIsi;
			}
		</script>
	</body>
	</html>

	<?php
	function getJumlahNotifikasi($conn){
		$jumlahNotifikasi = 0;

		if(cekNotifikasiPengajuanPinjaman($conn)){
			$jumlahNotifikasi++;
		}

		if(cekNotifikasiPengajuanGadai($conn)){
			$jumlahNotifikasi++;
		}

		if(cekNotifikasiInvestasiPending($conn)){
			$jumlahNotifikasi++;
		}

		if(cekNotifikasiPencairanDanaPending($conn)){
			$jumlahNotifikasi++;;
		}

		return $jumlahNotifikasi;
	}


	function cekNotifikasiPengajuanPinjaman($conn){

		$notifikasi = false;

		$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$notifikasi = true;
		}

		return $notifikasi;
	}
	function cekNotifikasiPengajuanGadai($conn){
		$notifikasi = false;

		$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$notifikasi = true;
		}

		return $notifikasi;
	}
	function cekNotifikasiInvestasiPending($conn){
		$notifikasi = false;

		$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$notifikasi = true;
		}

		return $notifikasi;
	}
	function cekNotifikasiPencairanDanaPending($conn){
		$notifikasi = false;

		$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$notifikasi = true;
		}

		return $notifikasi;
	}



	function generateNotification($conn){
		if(cekNotifikasiPengajuanPinjaman($conn)){
			$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
			?>
			<li>
				<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
			</li>
			<?php
		}
		if(cekNotifikasiPengajuanGadai($conn)){
			$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
			?>
			<li>
				<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
			</li>
			<?php
		}
		if(cekNotifikasiInvestasiPending($conn)){
			$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
			?>
			<li>
				<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
			</li>
			<?php
		}

		if(cekNotifikasiPencairanDanaPending($conn)){
			$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
			?>
			<li>
				<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
			</li>
			<?php
		}
	}

	function getJumlahPengajuanPinjamanPending($conn){
		$jumlah = 0;

		$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jumlah++;
		}

		return $jumlah;
	}

	function getJumlahPengajuanGadaiPending($conn){
		$jumlah = 0;

		$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jumlah++;
		}

		return $jumlah;
	}
	function getJumlahPengajuanInvestasiPending($conn){
		$jumlah = 0;

		$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jumlah++;
		}

		return $jumlah;
	}

	function getJumlahPencairanDanaPending($conn){
		$jumlah = 0;

		$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jumlah++;
		}

		return $jumlah;
	}

	function cekStatusCair($statusCair){
		if($statusCair==='0'){
			return 'belum cair';
		}else{
			return 'sudah cair';
		}
	}

	function cekTanggalCair($statusCair, $tanggalCair){
		if(cekStatusCair($statusCair)==='belum cair'){
			return '-';
		}else{
			return $tanggalCair;
		}
	}

	function cekStatusTransaksi($statusTransaksi){
		if($statusTransaksi==="0"){
			return "Belum Selesai";
		}else if($statusTransaksi==="1"){
			return "sudah selesai";
		}else if($statusTransaksi==="2"){
			return "pending";
		}
	}

	function getNamaNasabah($idNasabah, $conn){
		$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['nama'];
		}
	}

	function getNamaPegawai($idPegawai, $conn){
		$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['nama'];
		}
	}

	function cekAutoRenewal($autoRenewal){
		if($autoRenewal===NULL){
			return "transaksi baru";
		}else{
			return "transaksi lanjutan dari transaksi $autoRenewal";
		}
	}

	function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

		$button = NULL;
		$setBunga = NULL;
		$btnPenarikan = null;
		$buttonDetail = <<<TOMBOL
		<form action="transaksi_investasi_detail.php" method="POST" >
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-success btn-block">Detail</button>
		</form>
TOMBOL;
		if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
			$btnPenarikan = <<<TOMBOL
			<form action="transaksi_investasi_penarikan.php" method="POST" >
			<input type="hidden" name="update_status_pencairan" value="true">

			<input type="hidden" name="id_investasi" value="$id">
			<input type="hidden" name="id_nasabah" value="$nasabahId">

			<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
			</form>
TOMBOL;
			
			$button = <<<TOMBOL
			<form action="transaksi_investasi_edit_sistem.php" method="POST" >
			<input type="hidden" name="update_selesai_investasi" value="true">

			<input type="hidden" name="id_investasi" value="$id">

			<button type="submit" class="btn btn-danger btn-block">Selesai</button>
			</form>
TOMBOL;
			
		}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
			$button = <<<TOMBOL
			<form action="transaksi_investasi_edit_sistem.php" method="POST" >
			<input type="hidden" name="update_setujui_investasi" value="true">

			<input type="hidden" name="id" value="$id">
			<input type="hidden" name="id_pegawai" value="$pegawaiId">
			<input type="hidden" name="id_nasabah" value="$nasabahId">

			<button type="submit" class="btn btn-success btn-block">Setujui</button>
			</form>

			<form action="transaksi_investasi_edit_sistem.php" method="POST" >
			<input type="hidden" name="update_tolak_investasi" value="true">

			<input type="hidden" name="id" value="$id">
			<input type="hidden" name="id_pegawai" value="$pegawaiId">
			<input type="hidden" name="id_nasabah" value="$nasabahId">

			<button type="submit" class="btn btn-danger btn-block">Tolak</button>
			</form>
TOMBOL;
		}

		if(cekBunga($conn, $id)==false){
			$setBunga = <<<TOMBOL
			<form action="transaksi_investasi_insert_bunga.php" method="POST" >
			<input type="hidden" name="update_setujui_investasi" value="true">
			<input type="hidden" name="id_investasi" value="$id">
			<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
			</form>
TOMBOL;
		}

		return $buttonDetail.$btnPenarikan.$button.$setBunga;
	}

	function generateBunga($conn, $idTransaksiInvestasi){
		if(cekBunga($conn, $idTransaksiInvestasi)!=false){
			return cekBunga($conn, $idTransaksiInvestasi);
		}else{
			return '-';
		}
	}

	function cekBunga($conn, $idTransaksiInvestasi){
		$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
		$jumlah_bunga = null;
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jumlah_bunga = $row1['jumlah_bunga'];
		}

		if($jumlah_bunga==null){
			return false;
		}else{
			return $jumlah_bunga;
		}
	}

	?>


	<?php ob_end_flush(); ?>