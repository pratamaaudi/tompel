<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php
	if(isset($_POST['byr_transaksi_angsuran_gadai'])){
		$id_pegawai = $_SESSION['login_pegawai'];
		$id_transaksi_gadai = $_POST['id_transaksi_gadai'];
		$jmlh_angsuran = $_POST['jmlh_angsuran'];
		$jmlh_pinjaman = $_POST['jmlh_pinjaman'];
		$ket_bayar = $_POST['ket_bayar'];
		$brp_bulan = $_POST['brp_bulan'];
		$brp_bulan1 = $_POST['brp_bulan'];
		if($brp_bulan1 == 0){
			$brp_bulan = 1;
		}
		echo $brp_bulan;
		$real_tgl_bayar = date('Y-m-d h:i:s');
		$loop_bulan_ke = 0;
		$result3 = mysqli_query($conn, "SELECT * FROM transaksi_angsuran_gadai WHERE transaksi_gadai_id = $id_transaksi_gadai AND status_angsuran = 0 ORDER BY angsuran_ke ASC LIMIT $brp_bulan");
		if (!$result3) { die("SQL Error Result3 "); }
		while ($row3 = mysqli_fetch_array($result3)) {
			$id_tag = $row3['id'];
			$loop_bulan_ke++;
			if($ket_bayar == 1){
				if($brp_bulan1 == 0){
					if($loop_bulan_ke == 1){
						$jumlah_bayar = $jmlh_pinjaman;
						$result3_1 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `real_tgl_bayar` = '$real_tgl_bayar', `jumlah_bayar` = $jumlah_bayar, `status_angsuran` = 1, `pegawai_id` = $id_pegawai WHERE id = $id_tag");
						if (!$result3_1) { die("SQL Error Result3_1 "); }

						$result3_4 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `status_angsuran` = 1 WHERE transaksi_gadai_id = $id_transaksi_gadai AND status_angsuran = 0");
						if (!$result3_4) { die("SQL Error Result3_4"); }
					}
				} else{
					if($loop_bulan_ke == $brp_bulan){
						$jumlah_bayar = $jmlh_angsuran + $jmlh_pinjaman;
						$result3_1 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `real_tgl_bayar` = '$real_tgl_bayar', `jumlah_bayar` = $jumlah_bayar, `status_angsuran` = 1, `pegawai_id` = $id_pegawai WHERE id = $id_tag");
						if (!$result3_1) { die("SQL Error Result3_1 "); }

						$result3_4 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `status_angsuran` = 1 WHERE transaksi_gadai_id = $id_transaksi_gadai AND status_angsuran = 0");
						if (!$result3_4) { die("SQL Error Result3_4"); }
					} else {
						$result3_2 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `real_tgl_bayar` = '$real_tgl_bayar', `jumlah_bayar` = $jmlh_angsuran, `status_angsuran` = 1, `pegawai_id` = $id_pegawai WHERE id = $id_tag");
						if (!$result3_2) { die("SQL Error Result3_2"); }
					}
				}
			} else {
				$result3_3 = mysqli_query($conn, "UPDATE `transaksi_angsuran_gadai` SET `real_tgl_bayar` = '$real_tgl_bayar', `jumlah_bayar` = $jmlh_angsuran, `status_angsuran` = 1, `pegawai_id` = $id_pegawai WHERE id = $id_tag");
				if (!$result3_3) { die("SQL Error Result3_3"); }
			}			
		}
		$result7 = mysqli_query($conn, "SELECT COUNT(*) AS apa_gadai_lunas FROM `transaksi_angsuran_gadai` WHERE transaksi_gadai_id = $id_transaksi_gadai AND status_angsuran = 0");
		$row7 = mysqli_fetch_array($result7);
		if($row7['apa_gadai_lunas'] == 0){
			$result8 = mysqli_query($conn, "UPDATE `transaksi_gadai` SET `status_transaksi` = 0 WHERE `id`=$id_transaksi_gadai");
		}
		header("Location: transaksi_gadai_edit.php?id_transaksi=".$id_transaksi_gadai);
	}
	$id_transaksi_gadai = 0;
	if(isset($_GET['id_transaksi'])){
		$id_transaksi_gadai = $_GET['id_transaksi'];
	} 

	$result_cek_lunas = mysqli_query($conn, "SELECT tg.status_transaksi, DATEDIFF(DATE(NOW()), DATE(MAX(tag.tanggal_bayar))) AS sjt_akhir FROM transaksi_gadai tg LEFT JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai");
	if (mysqli_num_rows($result_cek_lunas)) {
		$row_cek_lunas = mysqli_fetch_array($result_cek_lunas);
		if($row_cek_lunas['status_transaksi'] == 0){
			$_SESSION['pesan_transaksi_gadai_edit'] = "Angsuran Tidak Bisa Dilakukan, Karena Transaksi Gadai Sudah Lunas";
			header("Location: transaksi_gadai_edit.php?id_transaksi=".$id_transaksi_gadai);	
		}
		if($row_cek_lunas['status_transaksi'] == 5 && $row_cek_lunas['sjt_akhir'] > 7){
			$_SESSION['pesan_transaksi_gadai_edit'] = "pemutihan lebih dari 7 hari, barang telah disita permanent";
			header("Location: transaksi_gadai_edit.php?id_transaksi=".$id_transaksi_gadai);	
		}
	} else {
		$_SESSION['pesan_transaksi_gadai'] = 'Angsuran Belum Bisa Dilakukan, Karena Transaksi Gadai Dengan Id Transaksi-' . $id_transaksi_gadai . " Belum Di Buat";
		header("Location: transaksi_gadai.php");
	}
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div> 
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">

											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>          
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 13%">
				<!--ul>
					<li><a href="#">Ebro Admin</a></li>
					<li><span>Dashboard</span></li>						
				</ul-->
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix">
				<div id="main_content">
					<!-- main content -->
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: rgb(0, 128, 128);">
								<h4 class="panel-title" style="color: white; font-family: arial"><strong>PEMBAYARAN BUNGA GADAI</strong></h4>
							</div>
							<div class="panel-body">
								<fieldset>
									<?php 
									$result1 = mysqli_query($conn, "SELECT tg.*, n.nama, n.nama_pekerjaan, n.alamat FROM transaksi_gadai tg JOIN nasabah n ON tg.nasabah_id = n.id WHERE tg.id = $id_transaksi_gadai");
									$row1 = mysqli_fetch_array($result1);

									?>
									<div class="col-sm-12">
										<div class="col-sm-6">
											<h3 class="heading_a"><strong>GENERAL</strong></h3>
											<div class="form-group">
												<label class="col-sm-12 control-label">
													Nasabah &nbsp;&nbsp;: <?php echo $row1['nama']; ?>
												</label>
												<!--div class="col-sm-10">
													<p class="form-control-static"><?php echo $row1['nama']; ?></p>
												</div-->
											</div>
											<div class="form-group">
												<label class="col-sm-12 control-label">
													Pekerjaan : <?php echo $row1['nama_pekerjaan']; ?>
												</label>
												<!--div class="col-sm-10">
													<p class="form-control-static"></p>
												</div-->
											</div>
											<div class="form-group">
												<label class="col-sm-12 control-label">
													Alamat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $row1['alamat']; ?>
												</label>
												<!--div class="col-sm-10">
													<p class="form-control"></p>
												</div-->
											</div>
										</div>
										<div class="col-sm-6"><h3 class="heading_a"><strong>BARANG</strong></h3>
											<div class="form-group">
												<?php 
												$no = 0;
												$sqlBar = "SELECT b.nama_barang, tag.jumlah_barang FROM barang_transaksi_gadai tag JOIN barang b ON tag.id_barang = b.id WHERE tag.id_transaksi_gadai = $id_transaksi_gadai";
												$resultBar = mysqli_query($conn, $sqlBar);
												if (!$resultBar) { die("SQL Error Result Bar "); }
												while ($rowBar = mysqli_fetch_array($resultBar)) { 
													$no++; 
													$t = $no . '. ' . $rowBar['nama_barang'] . ' (' . $rowBar['jumlah_barang'] . ')';
													echo '<label class="col-sm-12 control-label">'.$t.' </label>';
												} 
												?>
												<!--div class="col-sm-10">
													<p class="form-control-static"><?php echo $row1['model']; ?></p>
												</div-->
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<?php
										$result2 = mysqli_query($conn, "SELECT COUNT(*) AS kurang_berapa_bulan FROM transaksi_angsuran_gadai WHERE status_angsuran = 0 AND transaksi_gadai_id = $id_transaksi_gadai");
										$row2 = mysqli_fetch_array($result2);
										$kbb = $row2['kurang_berapa_bulan'];
										$brpBulan = 0;
										if(isset($_GET['brpBulan'])){
											$brpBulan = $_GET['brpBulan'];
										}
										$ket_bayar = 0; 
										if(isset($_GET['ket_bayar'])){
											if($_GET['ket_bayar'] == 1){
												$brpBulan = 0;
											}
											$ket_bayar = $_GET['ket_bayar'];
										}
										?>
										<form class="form-inline" method="GET" enctype="multipart/form-data">
											<div class="row">
												<div class="form-group" style="width: 100%;">
													<input type="hidden" name="id_transaksi" value="<?php echo $id_transaksi_gadai; ?>">
													<?php if($row1['status_transaksi'] != 5){ ?>
													<label class="col-sm-3" style="text-align: right"><strong>BAYAR BERAPA BULAN:</strong></label>
													<div class="col-sm-2">
														<select name="brpBulan" class="form-control" id="brpBulan" style="">
															<?php
															if($brpBulan == 0){ 
																echo '<option value="0" selected="">bayar</option>';
															} else {
																echo '<option value="0">bayar</option>';
															}
															for($i = 1; $i <= $kbb; $i++){
																if($brpBulan == $i){
																	echo '<option value="'.$i.'" selected="">'.$i.'</option>';
																} else {
																	echo '<option value="'.$i.'">'.$i.'</option>';
																}
															}
															?>
														</select>
													</div>
													<?php 
													} ?>
													<label class="col-sm-2"style="text-align: right"><strong>BAYAR:</strong></label>
													<div class="<?php if($row1['status_transaksi'] != 5){ echo 'col-sm-2'; } else { echo 'col-sm-7'; }?>">
														<select name="ket_bayar" class="form-control" id="ket_bayar" style="">
															<?php 
															if($row1['status_transaksi'] != 5){ 
																if($ket_bayar == 1){
																	echo '<option value="1" selected="">Lunas</option>'
																	.'<option value="0">Bunga</option>';
																} else {
																	echo '<option value="1">Lunas</option>'
																	.'<option value="0" selected="">Bunga</option>';
																}
															} else {
																echo '<option value="1">Lunas</option>';
															}
															?>
														</select>
													</div>
													<div class="col-sm-3">
														<button type="submit" class="btn btn-danger btn-danger" style="cursor: pointer;"><i class="icon-refresh"></i> Cek</button>
													</div>
												</div>
											</div>
										</form>
										<?php
										$total_total = 0;
										$total_angsuran = 0;
										if($ket_bayar == 1){
											$result_bb = mysqli_query($conn, "SELECT COUNT(*) AS bb FROM transaksi_angsuran_gadai WHERE transaksi_gadai_id = $id_transaksi_gadai AND status_angsuran = 0 AND DATE_FORMAT(tanggal_bayar,'%Y-%m') <= DATE_FORMAT(NOW(),'%Y-%m')");
											$row_bb = mysqli_fetch_array($result_bb);
											$brpBulan = $row_bb['bb'];
											$total_angsuran = $brpBulan * $row1['jumlah_angsuran'];
											$total_total = $total_angsuran + $row1['jumlah_pinjaman'];
										} else {
											$total_angsuran = $brpBulan * $row1['jumlah_angsuran'];
											$total_total = $total_angsuran;
										}
										//if($brpBulan == 0){
										//	$brpBulan = 1;
										//}
										?>
										<form method="POST" enctype="multipart/form-data">
											<div class="col-xs-12" style="padding: 10px 50px 10px 50px;">
												<center><strong>
												Total Bunga Yang Dibayar : </strong><input type="text" name="total_angsuran" class="form-control" value='<?php echo $total_angsuran; ?>' readonly="">
												<strong>Total Pembayaran Keseluruhan : </strong> <input type="text" name="total_total" class="form-control" value='<?php echo $total_total; ?>' readonly="">
											</center>
										</div>
										<div class="col-xs-9"></div>
										<div class="col-xs-3">
											<input type="hidden" name="id_transaksi_gadai" value="<?php echo $id_transaksi_gadai; ?>">
											<input type="hidden" name="brp_bulan" value="<?php echo $brpBulan; ?>">
											<input type="hidden" name="ket_bayar" value="<?php echo $ket_bayar; ?>">
											<input type="hidden" name="jmlh_angsuran" value="<?php echo $row1['jumlah_angsuran']; ?>">
											<input type="hidden" name="jmlh_pinjaman" value="<?php echo $row1['jumlah_pinjaman']; ?>">
											<input type="hidden" name="byr_transaksi_angsuran_gadai">
											<button type="submit" class="btn btn-success btn-success"><i class="icon-money"></i> Bayar</button>
										</div>
									</form>
								</div>	
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="footer_space"></div>
</div>

<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"></div>
				<div class="col-sm-12 text-right">
					<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.ba-resize.min.js"></script>
<script src="../js/jquery_cookie.min.js"></script>
<script src="../js/retina.min.js"></script>
<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
<script src="../js/tinynav.js"></script>
<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
<script src="../js/ebro_common.js"></script>
<script src="../js/lib/peity/jquery.peity.min.js"></script>
<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../js/lib/flot/jquery.flot.min.js"></script>
<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
<script src="../js/lib/flot/jquery.flot.resize.js"></script>
<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
<script src="../js/pages/ebro_dashboard.js"></script>
<script type="text/javascript">
	function last_angsur(kbb){
		d = document.getElementById("brpBulan").value;
		if(d == kbb){
			$('#byrDenda option[value=1]').attr('selected','selected');
			$('#byrDenda').attr("readOnly", "true");
		} else {
			$('#byrDenda').attr("readOnly", "false");
		}
	}
</script>

</div>
</body>
</html>


<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>