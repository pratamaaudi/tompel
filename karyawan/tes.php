			



<?php
ob_start();
session_start();
require '../config.php'; 
?>
<table id="dt_basic" class="table table-striped">
	<thead>
		<tr role="row">
			<th>No</th>
			<th>Nasabah</th>
			<th>Kendaraan</th>
			<th>Jumlah Bayar</th>
		</tr>
	</thead>
	<tbody role="alert" aria-live="polite" aria-relevant="all">
		<?php 
		$no = 0;
		$result1 = mysqli_query($conn, "SELECT tabel.Bayar, tp.no_kontrak, n.nama, jj.merk, jj.type, jj.model FROM nasabah n INNER JOIN transaksi_peminjaman tp on n.id = tp.nasabah_id INNER JOIN jenis_jaminan jj on tp.jenis_jaminan_id=jj.id INNER JOIN (SELECT transaksi_peminjaman_no_kontrak, SUM(jumlah_bayar) as Bayar FROM transaksi_angsuran GROUP by transaksi_peminjaman_no_kontrak ASC) as tabel on tp.no_kontrak = tabel.transaksi_peminjaman_no_kontrak WHERE tabel.Bayar>0 and n.nama like '%elizabeth%'");
		if (!$result1) { die("SQL Error Result1 "); }
		while ($allRow1 = mysqli_fetch_array($result1)) {
			$no++;
			?>
			<tr class="odd">
				<td><?php echo $no; ?></td>
				<td><?php echo $allRow1['nama']; ?></td>
				<td><?php echo $allRow1['merk']."--".$allRow1['type'].$allRow1['model']; ?></td>
				<td><?php echo $allRow1['Bayar']; ?></td>
			</tr>
			<?php 
		} ?>
	</tbody>
</table>