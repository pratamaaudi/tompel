<?php
ob_start();
session_start();
require '../config.php'; 
if(isset($_GET['tgl_laporan_pemasukan'])){
	?>
	<table class="table table-striped dataTable" aria-describedby="dt_basic_info">
		<thead>
			<tr role="row">
				<th><center>No Kontrak</center></th>
				<th><center>Nasabah</center></th>
				<th><center>Jumlah Pemasukan</center></th>
				<th><center>Keterangan</center></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$tgl = substr($_GET['tgl_laporan_pemasukan'], 8, 2) . "-" . substr($_GET['tgl_laporan_pemasukan'], 5, 2) . "-" . substr($_GET['tgl_laporan_pemasukan'], 0, 4);
			$tgl_awal_laporan = $_GET['tgl_laporan_pemasukan'];
			$tgl_akhir_laporan = $_GET['tgl_laporan_pemasukan'];
			$tgl_awal_laporan = $tgl_awal_laporan . " 00:00:00";
			$tgl_akhir_laporan = $tgl_akhir_laporan . " 23:59:59";
			//echo $tgl_awal_laporan." string ".$tgl_akhir_laporan;
			$sqlDtl = "SELECT id, nasabah, total, DATE_FORMAT(tanggal, '%d-%m-%Y') AS tanggal, aw, ak, ket FROM ( SELECT tp.no_kontrak AS id, n.nama AS nasabah, SUM(ta.jumlah_bayar) AS total, ta.real_tgl_bayar AS tanggal, MIN(ta.angsuran_ke) AS aw, MAX(ta.angsuran_ke) AS ak, 'Transaksi Peminjaman' AS ket FROM transaksi_peminjaman tp JOIN transaksi_angsuran ta ON tp.no_kontrak = ta.transaksi_peminjaman_no_kontrak JOIN nasabah n ON tp.nasabah_id = n.id WHERE ta.real_tgl_bayar IS NOT NULL GROUP BY ta.real_tgl_bayar UNION ALL SELECT tg.id AS id, n.nama AS nasabah, SUM(tag.jumlah_bayar) AS total, tag.real_tgl_bayar AS tanggal, MIN(tag.angsuran_ke) AS aw, MAX(tag.angsuran_ke) AS ak, 'Transaksi Gadai' AS ket FROM transaksi_gadai tg JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id JOIN nasabah n ON tg.nasabah_id = n.id WHERE tag.real_tgl_bayar IS NOT NULL GROUP BY tag.real_tgl_bayar UNION ALL SELECT ti.id AS id, n.nama AS nasabah, SUM(ti.jumlah_uang) AS total, ti.tanggal AS tanggal, 0 AS aw, 0 AS ak, 'Transaksi Investasi' AS ket FROM transaksi_investasi ti JOIN nasabah n ON ti.nasabah_id = n.id where transaksi_investasi_id IS NULL)t WHERE (tanggal BETWEEN '$tgl_awal_laporan' AND '$tgl_akhir_laporan')GROUP BY tanggal";
			$resultDtl = mysqli_query($conn, $sqlDtl);
			if (!$resultDtl) { die("SQL Error ResultDtl "); }
			while ($rowDtl = mysqli_fetch_array($resultDtl)) {
				?>
				<tr>
					<td><center><?php echo $rowDtl['id'] . "/" . $tgl; ?></center></td>
					<td><center><?php echo $rowDtl['nasabah']; ?></center></td>
					<td><center><?php echo $rowDtl['total']; ?></center></td>
					<td>
						<center>
							<?php 
							echo $rowDtl['ket']; 
							if($rowDtl['ket'] != "Transaksi Investasi"){
								$lanjutan_keterangan = " Angsuran Ke ";
								for($i = $rowDtl['aw']; $i <= $rowDtl['ak']; $i++){
									if($lanjutan_keterangan == " Angsuran Ke "){
										$lanjutan_keterangan = $lanjutan_keterangan . $i;
									} else {
										$lanjutan_keterangan = $lanjutan_keterangan . ", " . $i;
									}
								}
								echo $lanjutan_keterangan;
							}
							?>
						</center>
					</td>
				</tr>
				<?php	
			}
			?>
		</tbody>
	</table>
	<?php
}
ob_end_flush();
?>