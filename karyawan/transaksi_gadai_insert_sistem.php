<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 


// QUERY MENAMPILKAN DATA NASABAH
if(isset($_POST['isi_data_nasabah'])){
	$id_nasabah = $_POST['id_nasabah'];
	$result = mysqli_query($conn, "SELECT * FROM nasabah WHERE id = $id_nasabah");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	echo $row['no_ktp'] . "-" . $row['nama'] . "-" . $row['alamat'] . "-" . $row['nama_pekerjaan'] . "-" . $row['alamat_pekerjaan'] . "-" . $row['telp'] . "-" . $row['no_hp'];
}

if (isset($_POST['insert_transaksi_gadai'])) {
	$id_pegawai = $_SESSION['login_pegawai'];
	$id_nasabah = $_POST['nasabah'];
	$id_suku_bunga = $_POST['suku_bunga'];
	$tanggal_gadai = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	$status_transaksi = $_POST['status_transaksi'];
	$jumlah_angsuran = $_POST['nominal_angsuran'];
	$biaya_administrasi = $_POST['biaya_administrasi'];
	$total = $_POST['total'];
	$status_cair = $_POST['status_cair'];
	$tgl_cair = date('Y-m-d h:i:s');

	//INSERT INTO `transaksi_gadai`(`id`, `tanggal`, `status_cair`, `tgl_cair`, `status_transaksi`, `jumlah_pinjaman`, `nasabah_id`, `pegawai_id`, `shapus`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])
	$sql1 = "INSERT INTO `transaksi_gadai`(`tanggal`, `status_cair`";
	if($status_cair != 0){
		$sql1 = $sql1 . ", `tgl_cair`";
	}
	$sql1 = $sql1 . ", `status_transaksi`, `jumlah_pinjaman`, `jangka_waktu`, `jumlah_angsuran`, `biaya_administrasi`, `total`, `nasabah_id`, `pegawai_id`, `shapus`) VALUES ('$tanggal_gadai', $status_cair";
	if($status_cair != 0){
		$sql1 = $sql1 . ", '$tgl_cair'";
	}
	$sql1 = $sql1 . ", $status_transaksi, $jumlah_pinjaman, $jangka_waktu, $jumlah_angsuran, $biaya_administrasi, $total, $id_nasabah, $id_pegawai, 0)";
	$result1 = mysqli_query($conn, $sql1);
	if(!$result1){ die("SQL ERROR : Result " . $sql1); }

	$result2 = mysqli_query($conn, "SELECT * FROM transaksi_gadai WHERE tanggal = '$tanggal_gadai'");
	if(!$result2){ die("SQL ERROR : Result2"); }
	$row2 = mysqli_fetch_array($result2);
	$id_transaksi_gadai = $row2['id'];

	$result3 = mysqli_query($conn, "INSERT INTO `perubahan_suku_bunga`(`id_suku_bunga`, `id_transaksi_gadai`, `tgl_perubahan_suku_bunga`) VALUES ($id_suku_bunga, $id_transaksi_gadai, '$tanggal_gadai')");
	if(!$result3){ die("SQL ERROR : Result3"); }

	$jum_barang = count($_POST['id_barang']);
	for($i = 0; $i < $jum_barang; $i++){
		$id_barang = $_POST['id_barang'][$i];
		//$jumlah_barang = $_POST['jumlah_barang'][$i];
		$status_barang = $_POST['status_barang'][$i];
		$result5 = mysqli_query($conn, "INSERT INTO `barang_transaksi_gadai`(`id_barang`, `id_transaksi_gadai`, `jumlah_barang`, `status_barang`) VALUES ($id_barang, $id_transaksi_gadai, 0, $status_barang)");
		if(!$result5){ die("SQL ERROR : INSERT INTO `barang_transaksi_gadai`(`id_barang`, `id_transaksi_gadai`, `jumlah_barang`, `status_barang`) VALUES ($id_barang, $id_transaksi_gadai, 0, $status_barang)"); }	
	}


	$tgl_angsur = intval(substr($tanggal_gadai, 8, 2));
	$bln_angsur = intval(substr($tanggal_gadai, 5, 2));
	$thn_angsur = intval(substr($tanggal_gadai, 0, 4));
	$wkt_angsur = substr($tanggal_gadai, 11, 8);
	for($i = 1; $i <= intval($jangka_waktu); $i++){
		$bln_angsur++;
		if($bln_angsur == 13){
			$bln_angsur = 0;
			$thn_angsur++;
			$i--;
		} else {
			$last_day = $thn_angsur . "-" . $bln_angsur;
			if(strlen(strval($bln_angsur)) == 1){
				$last_day = $thn_angsur . "-0" . $bln_angsur;
			}
			$sesuai_tgl_wkt_angsur = $last_day . "-" . $tgl_angsur . " " . $wkt_angsur;
			if(strlen(strval($tgl_angsur)) == 1){
				$sesuai_tgl_wkt_angsur = $last_day . "-0" . $tgl_angsur . " " . $wkt_angsur;
			}
			$date = new DateTime($last_day);
			$date->modify('last day of this month');
			$tgl_wkt_last = $date->format('Y-m-d') . " " . $wkt_angsur;

			$tgl_wkt_pasti_angsur = '';
			if($sesuai_tgl_wkt_angsur > $tgl_wkt_last){
				$tgl_wkt_pasti_angsur = $tgl_wkt_last;
			} else {
				$tgl_wkt_pasti_angsur = $sesuai_tgl_wkt_angsur;
			}
			//INSERT INTO `transaksi_angsuran_gadai`(`id`, `tanggal_bayar`, `real_tgl_bayar`, `jumlah_denda`, `jumlah_bayar`, `angsuran_ke`, `kode_rangkap`, `transaksi_gadai_id`, `pegawai_id`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9])

			$result4 = mysqli_query($conn, "INSERT INTO `transaksi_angsuran_gadai`(`tanggal_bayar`, `angsuran_ke`, `status_angsuran`, `transaksi_gadai_id`) VALUES ('$tgl_wkt_pasti_angsur', $i, 0, $id_transaksi_gadai)");
			if(!$result4){ die("SQL ERROR : Result4"); }
		}
	}

	//if ($result1) { 
	header('Location: transaksi_gadai.php'); 
	//} else { 
	//	die("SQL ERROR : Result1"); 
	//}
}
ob_end_flush(); ?>