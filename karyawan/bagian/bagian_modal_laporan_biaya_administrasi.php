<?php
ob_start();
session_start();
require '../../config.php'; 
if(isset($_GET['tgl_laporan'])){
	?>
	<table class="table table-striped dataTable" aria-describedby="dt_basic_info">
		<thead>
			<tr role="row">
				<th><center>No Kontrak</center></th>
				<th><center>Nasabah</center></th>
				<th><center>Jumlah Pengeluaran</center></th>
				<th><center>Keterangan</center></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$tgl = substr($_GET['tgl_laporan'], 8, 2) . "-" . substr($_GET['tgl_laporan'], 5, 2) . "-" . substr($_GET['tgl_laporan'], 0, 4);
			$tgl_awal_laporan = $_GET['tgl_laporan'];
			$tgl_akhir_laporan = $_GET['tgl_laporan'];
			$tgl_awal_laporan = $tgl_awal_laporan . " 00:00:00";
			$tgl_akhir_laporan = $tgl_akhir_laporan . " 23:59:59";

			$sqlDtl = "SELECT t.id, DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal, bayar, keterangan, n.nama FROM ( 
			SELECT id as 'id', date(tgl_cair) as 'tanggal', (biaya_administrasi) as 'bayar', nasabah_id, 'transaksi gadai' as 'keterangan' 
			FROM `transaksi_gadai`
			UNION ALL 
			SELECT no_kontrak as 'id', date(tgl_cair) as 'tanggal', (biaya_administrasi) as 'bayar', nasabah_id , 'transaksi peminjaman' as 'keterangan' 
			FROM `transaksi_peminjaman` )t
			INNER JOIN nasabah n
			ON t.nasabah_id = n.id
			WHERE (tanggal BETWEEN '$tgl_awal_laporan' AND '$tgl_akhir_laporan')";
			$resultDtl = mysqli_query($conn, $sqlDtl);
			if (!$resultDtl) { die("SQL Error ResultDtl "); }
			while ($rowDtl = mysqli_fetch_array($resultDtl)) {
				?>
				<tr>
					<td><center><?php echo $rowDtl['id'] . "/" . $tgl; ?></center></td>
					<td><center><?php echo $rowDtl['nama']; ?></center></td>
					<td><center><?php echo $rowDtl['bayar']; ?></center></td>
					<td><center><?php echo $rowDtl['keterangan']; ?></center></td>
				</tr>
				<?php	
			}
			?>
		</tbody>
	</table>
	<?php
}
ob_end_flush();
?>