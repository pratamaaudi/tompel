<?php
ob_start();
session_start();
require '../../config.php'; 
if(isset($_GET['tgl_laporan'])){
	?>
	<table class="table table-striped dataTable" aria-describedby="dt_basic_info">
		<thead>
			<tr role="row">
				<th><center>No Kontrak</center></th>
				<th><center>Nasabah</center></th>
				<th><center>Jumlah Pemasukan</center></th>
				<th><center>Keterangan</center></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$tgl = substr($_GET['tgl_laporan'], 8, 2) . "-" . substr($_GET['tgl_laporan'], 5, 2) . "-" . substr($_GET['tgl_laporan'], 0, 4);
			$tgl_awal_laporan = $_GET['tgl_laporan'];
			$tgl_akhir_laporan = $_GET['tgl_laporan'];
			$tgl_awal_laporan = $tgl_awal_laporan . " 00:00:00";
			$tgl_akhir_laporan = $tgl_akhir_laporan . " 23:59:59";
			//echo $tgl_awal_laporan." string ".$tgl_akhir_laporan;
			$sqlDtl = "SELECT t.id, DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal, denda, keterangan, n.nama, aw, ak FROM ( 
			SELECT tp.no_kontrak as 'id', date(real_tgl_bayar) as 'tanggal', jumlah_denda as 'denda', jumlah_bayar as 'bayar', 'transaksi peminjaman' as 'keterangan', tp.nasabah_id, MIN(ta.angsuran_ke) AS aw, MAX(ta.angsuran_ke) AS ak
			FROM transaksi_angsuran ta 
			inner join transaksi_peminjaman tp
			on ta.transaksi_peminjaman_no_kontrak = tp.no_kontrak
			WHERE real_tgl_bayar IS NOT NULL
			UNION ALL 
			SELECT tg.id as 'id', date(real_tgl_bayar) as 'tanggal', jumlah_denda as 'denda', jumlah_bayar as 'bayar', 'transaksi gadai' as 'keterangan', tg.nasabah_id, MIN(tag.angsuran_ke) AS aw, MAX(tag.angsuran_ke) AS ak 
			FROM transaksi_angsuran_gadai tag
			INNER JOIN transaksi_gadai tg
			on tag.transaksi_gadai_id = tg.id
			WHERE real_tgl_bayar IS NOT NULL UNION ALL
			SELECT id as 'id', date(tanggal), 0 as 'denda', jumlah_uang as 'bayar', 'transaksi investasi' as 'keterangan', null as 'nasabah_id', 0 AS aw, 0 AS ak FROM `transaksi_investasi` where transaksi_investasi_id IS NULL )t
			INNER JOIN nasabah n
			ON t.nasabah_id = n.id
			WHERE (tanggal BETWEEN '$tgl_awal_laporan' AND '$tgl_akhir_laporan')";
			$resultDtl = mysqli_query($conn, $sqlDtl);
			if (!$resultDtl) { die("SQL Error ResultDtl "); }
			while ($rowDtl = mysqli_fetch_array($resultDtl)) {
				?>
				<tr>
					<td><center><?php echo $rowDtl['id'] . "/" . $tgl; ?></center></td>
					<td><center><?php echo $rowDtl['nama']; ?></center></td>
					<td><center><?php echo $rowDtl['denda']; ?></center></td>
					<td>
						<center>
							<?php 
							echo $rowDtl['keterangan']; 
							if($rowDtl['keterangan'] != "Transaksi Investasi"){
								$lanjutan_keterangan = " Angsuran Ke ";
								for($i = $rowDtl['aw']; $i <= $rowDtl['ak']; $i++){
									if($lanjutan_keterangan == " Angsuran Ke "){
										$lanjutan_keterangan = $lanjutan_keterangan . $i;
									} else {
										$lanjutan_keterangan = $lanjutan_keterangan . ", " . $i;
									}
								}
								echo $lanjutan_keterangan;
							}
							?>
						</center>
					</td>
				</tr>
				<?php	
			}
			?>
		</tbody>
	</table>
	<?php
}
ob_end_flush();
?>