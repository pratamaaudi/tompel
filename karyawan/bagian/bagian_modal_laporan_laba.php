<?php
ob_start();
session_start();
require '../../config.php'; 
if(isset($_GET['tgl_laporan'])){
	?>
	<table class="table table-striped dataTable" aria-describedby="dt_basic_info">
		<thead>
			<tr role="row">
				<th><center>No Kontrak</center></th>
				<th><center>Nasabah</center></th>
				<th><center>Jumlah Laba</center></th>
				<th><center>Keterangan</center></th>
			</tr>
		</thead>
		<tbody>
			<?php
			$tgl = substr($_GET['tgl_laporan'], 8, 2) . "-" . substr($_GET['tgl_laporan'], 5, 2) . "-" . substr($_GET['tgl_laporan'], 0, 4);
			$tgl_awal_laporan = $_GET['tgl_laporan'];
			$tgl_akhir_laporan = $_GET['tgl_laporan'];
			$tgl_awal_laporan = $tgl_awal_laporan . " 00:00:00";
			$tgl_akhir_laporan = $tgl_akhir_laporan . " 23:59:59";

			$sqlDtl = "SELECT t.id, DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal, sum(bunga) as 'bunga', nasabah_id, n.nama, keterangan FROM (
			SELECT tg.id, date(tag.real_tgl_bayar) as 'tanggal', SUM(IF(tag.jumlah_bayar != tg.jumlah_pinjaman, tag.jumlah_bayar, 0)) as 'bunga', tg.nasabah_id, 'transaksi gadai' as 'keterangan' 
			FROM transaksi_angsuran_gadai tag INNER JOIN transaksi_gadai tg on tag.transaksi_gadai_id = tg.id 
			where tag.real_tgl_bayar != '0000-00-00 00:00:00' 
			GROUP BY tag.real_tgl_bayar
			UNION ALL 
			SELECT tp.no_kontrak as 'id', ta.real_tgl_bayar, SUM(IF((tp.jumlah_pinjaman/tp.jangka_waktu) = ta.jumlah_bayar, 0, tp.jumlah_angsuran-(tp.jumlah_pinjaman/tp.jangka_waktu))) AS bunga, tp.nasabah_id, 'transaksi peminjaman' as 'keterangan' 
			FROM transaksi_peminjaman tp JOIN transaksi_angsuran ta ON tp.no_kontrak = ta.transaksi_peminjaman_no_kontrak 
			WHERE ta.real_tgl_bayar != '0000-00-00 00:00:00' 
			GROUP BY ta.real_tgl_bayar
			)t
			INNER JOIN nasabah n
			on t.nasabah_id = n.id
			WHERE (tanggal BETWEEN '$tgl_awal_laporan' AND '$tgl_akhir_laporan')";

			$resultDtl = mysqli_query($conn, $sqlDtl);
			if (!$resultDtl) { die("SQL Error ResultDtl "); }
			while ($rowDtl = mysqli_fetch_array($resultDtl)) {
				if($rowDtl['id'] !== null){
				?>
				<tr>
					<td><center><?php echo $rowDtl['id'] . "/" . $tgl; ?></center></td>
					<td><center><?php echo $rowDtl['nama']; ?></center></td>
					<td><center><?php echo $rowDtl['bunga']; ?></center></td>
					<td><center><?php echo $rowDtl['keterangan']; ?></center></td>
				</tr>
				<?php	
				}
			}
			?>
		</tbody>
	</table>
	<?php
}
ob_end_flush();
?>