<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<div class="modal fade" id="modal_tambah_denda">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Denda</h4>
				</div>
				<form method="POST">
					<div class="modal-body">
						<input style="width: 32%; float: left; margin-right: 2%;" type="number" name="denda_hari" class="form-control" required="" placeholder="nominal">
						<input style="width: 32%; float: left; margin-right: 2%;" type="number" name="min_pinjam" class="form-control" required="" placeholder="min pinjaman">
						<input style="width: 32%; float: left;" type="number" name="max_pinjam" class="form-control" required="" placeholder="max pinjaman">
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default" style="margin-top: 10px;">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php
	if (isset($_SESSION['psn_setting'])){
		$psn = $_SESSION['psn_setting']; 
		echo '<script type="text/javascript">alert("' . $psn . '")</script>';
		unset($_SESSION['psn_setting']);
	}
	if (isset($_POST['denda_hari'])){
		$denda_hari = $_POST['denda_hari'];
		$min_pinjam = $_POST['min_pinjam'];
		$max_pinjam = $_POST['max_pinjam'];
		$id = 0;
		$sNomDenda = 0;
		$minMax = 0;
		$result1 = mysqli_query($conn, "SELECT * FROM denda");
		if (!$result1) { die("SQL ERROR : result1"); }
		while ($allRow1 = mysqli_fetch_array($result1)) {
			$dh = $allRow1['denda_hari'];
			if($dh == $denda_hari){
				$sNomDenda = 1;
				$id = $allRow1['denda_hari'];
			}
			$mp1 = $allRow1['max_pinjam'];
			$mp2 = $allRow1['min_pinjam'];
			if(($mp1 >= $min_pinjam && $mp2 <= $min_pinjam) || ($mp1 >= $max_pinjam && $mp2 <= $max_pinjam)){
				$minMax = 1;
			}
		}
		//echo '<script type="text/javascript">alert("' . $id . '_' . $minMax . '_' . $sNomDenda . '")</script>';
		if($sNomDenda == 0 && $minMax == 0){
			$resultG = mysqli_query($conn, "INSERT INTO denda(denda_hari, min_pinjam, max_pinjam, s_hapus) VALUES ($denda_hari, $min_pinjam, $max_pinjam, 0)");
			if (!$resultG) { die("SQL ERROR : resultG"); }
		} else if($sNomDenda == 1 && $minMax == 0){
			$resultG = mysqli_query($conn, "UPDATE denda SET min_pinjam = $min_pinjam, max_pinjam = $max_pinjam, s_hapus = 0 WHERE id_denda = $id");
			if (!$resultG) { die("SQL ERROR : resultG"); }
		} else {
			$_SESSION['psn_setting'] = 'failed process';
		}
		header('Location: setting.php');
	}
	
	if (isset($_POST['submit_gambar'])){
		$idPegawai = $_SESSION['login_pegawai'];
		$fg = explode(".", $_FILES['fGerak']['name']);
		$gbr = substr(md5($_FILES['fGerak']['name'] . time()), 0, 10) . "." . $fg[count($fg) - 1];
		move_uploaded_file($_FILES['fGerak']['tmp_name'], "../gallery/" . $gbr);
		$resultG = mysqli_query($conn, "INSERT INTO gambar_berjalan(pegawai_id, gambar) VALUES ($idPegawai, '$gbr')");
		if (!$resultG) { die("SQL ERROR : resultG"); }
		unset($_POST['submit_gambar']);
		header('Location: setting.php');
	}
	if(isset($_POST['submit_faq'])){
		$idPegawai = $_SESSION['login_pegawai'];
		$judul = $_POST['judul'];
		$ket = $_POST['isi'];
		$resultF = mysqli_query($conn, "INSERT INTO faq(pegawai_id, judul, keterangan) VALUES ($idPegawai, '$judul', '$ket')");
		if (!$resultF) { die("SQL ERROR : resultF"); }
		unset($_POST['submit_faq']);
		header('Location: setting.php');
	}
	if (isset($_GET['dGbr'])){
		$id = $_GET['dGbr'];
		$gbr = $_GET['gbr'];
		unlink("../gallery/".$gbr);
		$resultDG = mysqli_query($conn, "DELETE FROM gambar_berjalan WHERE id_gambar_berjalan = $id");
		if (!$resultDG) { die("SQL ERROR : resultDG"); }
		header('Location: setting.php');
	}
	if(isset($_GET['dFaq'])){
		$id = $_GET['dFaq'];
		$resultDF = mysqli_query($conn, "DELETE FROM faq WHERE id_faq = $id");
		if (!$resultDF) { die("SQL ERROR : resultDF"); }
		header('Location: setting.php');
	}
	if(isset($_GET['dIdDenda'])){
		$id = $_GET['dIdDenda'];
		$resultDF = mysqli_query($conn, "UPDATE denda SET s_hapus = 1 WHERE id_denda = $id");
		if (!$resultDF) { die("SQL ERROR : resultDF"); }
		header('Location: setting.php');
	}
	?>
	<div id="wrapper_all">
		<header id="top_header";  style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>




					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">		
											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>




					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>          
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li class="active">             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 12.5%">
				<!--ul>
					<li><a href="#">Ebro Admin</a></li>
					<li><span>Dashboard</span></li>						
				</ul-->
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix">
				<div id="main_content">
					



					<!-- TAMBAHAN DENDA DARI PAK NAUFAL -->
					<?php if ($_SESSION['status_pegawai'] == 1) { ?>
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#tab_denda">DENDA</a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_halaman_depan">HALAMAN DEPAN</a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="tab_denda" class="tab-pane active">
								<div class="col-sm-2"></div>
								<div class="col-sm-8">
									<a class="btn btn-success" style="float: right;" data-toggle="modal" data-target="#modal_tambah_denda"><i class="icon-plus"></i> Tambah Denda</a>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">Denda Peminjaman</h4>
										</div>
										<div class="dataTable_wrapper form-inline" role="grid">
											<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
												<thead>
													<tr role="row">
														<th>No</th>
														<th>Nominal Denda</th>
														<th>Min Pinjaman</th>
														<th>Max Pinjaman</th>
														<th>Alat</th>
													</tr>
												</thead>
												<tbody role="alert" aria-live="polite" aria-relevant="all">
													<?php 
													$no1 = 0;
													$result1 = mysqli_query($conn, "SELECT * FROM denda WHERE s_hapus = 0");
													if (!$result1) { die("SQL Error Result1 "); }
													while ($allRow1 = mysqli_fetch_array($result1)) {
														$no1++;
														?>
														<tr class="odd">
															<td><?php echo $no1; ?></td>
															<td><?php echo $allRow1['denda_hari']; ?></td>
															<td><?php echo $allRow1['min_pinjam']; ?></td>
															<td><?php echo $allRow1['max_pinjam']; ?></td>
															<td>
																<a class="btn btn-danger" style="cursor: pointer;" href="setting.php?dIdDenda=<?php echo $allRow1['id_denda']; ?>"> <i class="icon-trash"></i> delete</a>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-sm-2"></div>
							</div>
							<div id="tab_halaman_depan" class="tab-pane">
							<?php } ?>






							<div class="col-sm-6">
								<a class="btn btn-success" onclick="document.getElementById('al1').style.display='inherit';"><i class="icon-plus"></i> Tambah Gambar Bergerak</a>
								<div id="al1" class="alert alert-info" style="display: none;">
									<a class="close" onclick="document.getElementById('al1').style.display='none';">×</a>
									<form class="form-inline" method="POST" enctype="multipart/form-data">
										<div class="row">
											<div class="form-group" style="width: 100%;">
												<label class="col-sm-3 control-label" align="left">Tambah Gambar:</label>
												<div class="col-sm-6"><input type="file" name="fGerak" required=""></div>
												<div class="col-sm-3">
													<button type="submit" name="submit_gambar" class="btn btn-default btn-xs btn-block" style="cursor: pointer;">Simpan</button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="panel panel-default" style="margin-top: 10px;">
									<div class="panel-heading" style="background-color: rgb(0, 128, 128);">
										<h4 class="panel-title" style="color: white"><strong>Gambar Bergerak</strong></h4>
									</div>
									<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
										<div class="dt-top-row">
											<div class="dt-wrapper">
												<?php
												$resultGB = mysqli_query($conn, "SELECT gb.id_gambar_berjalan, gb.gambar, p.nama FROM gambar_berjalan gb JOIN pegawai p ON gb.pegawai_id = p.id");
												if (!$resultGB) { die("SQL Error ResultGB "); }
												while ($rowGB = mysqli_fetch_array($resultGB)) { 
													?>
													<div class="col-sm-4" style='border: 1px solid black; padding: 5px 5px 5px 5px'>
														<img src="../gallery/<?php echo $rowGB['gambar']; ?>" title="<?php echo $rowGB['nama']; ?>" style="width: 100%; height: 80px; margin-bottom: 5px;" />
														<center><a href="setting.php?dGbr=<?php echo $rowGB['id_gambar_berjalan']; ?>&gbr=<?php echo $rowGB['gambar']; ?>" class="btn btn-danger btn-xs"> <i class="icon-trash"></i> DELETE</a></center>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<a class="btn btn-success" onclick="document.getElementById('al2').style.display='inherit';"><i class="icon-plus"></i> Tambah Berita</a>
								<div id="al2" class="alert alert-info" style="display: none;">
									<a class="close" onclick="document.getElementById('al2').style.display='none';">×</a><br>
									<form method="POST">
										<label style="float: left; width: 15%; margin-top: 6px;">Judul:</label>
										<input type="text" class="form-control input-sm" name="judul" style="float: left; width: 85%" required=""><br>
										<div class="col-sm-8">
											<label style="float: left; width: 100%; margin-top: 6px;">Isi Berita:</label>
											<textarea name="isi" rows="1" class="form-control" style="float: right; width: 85%;" required=""></textarea>
										</div>
										<button type="submit" class="btn btn-success btn-sm" name="submit_faq" style=" float: right; margin-top: 50px; text-align: right;">Simpan</button>
									</form>
									<br><br><br><br><br>
								</div>
								<div class="panel panel-default" style="margin-top: 10px;">
									<div class="panel-heading" style="background-color: rgb(0, 128, 128);">
										<h4 class="panel-title" style="color: white"><strong>Berita</strong></h4>
									</div>
									<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
										<div class="dt-top-row">
											<div class="dt-wrapper">
												<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
													<thead>
														<tr>
															<th>No</th>
															<th>Pegawai</th>
															<th>Judul</th>
															<th>Hapus</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$noFAQ = 0;
														$resultFAQ = mysqli_query($conn, "SELECT f.id_faq, f.judul, f.keterangan, p.nama FROM faq f JOIN pegawai p ON f.pegawai_id = p.id");
														if (!$resultFAQ) { die("SQL Error ResultFAQ "); }
														while ($rowFAQ = mysqli_fetch_array($resultFAQ)) {
															$noFAQ++;
															?>
															<tr title="<?php echo $rowFAQ['keterangan']; ?>" style="cursor: pointer;">
																<td><?php echo $noFAQ; ?></td>
																<td><?php echo $rowFAQ['nama']; ?></td>
																<td><?php echo $rowFAQ['judul']; ?></td>
																<td>
																	<center>
																		<a href="setting.php?dFaq=<?php echo $rowFAQ['id_faq']; ?>" class="btn btn-danger btn-xs"> <i class="icon-trash"></i> DELETE</a>
																	</center>
																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>



							<!--  UNTUK MENGAKSES HANYA ADMIN--> 
							<?php if ($_SESSION['status_pegawai'] == 1) { ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</section>
			<div id="footer_space"></div>
		</div>
		<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
			<footer id="footer" style="background-color: rgb(0, 128, 128);">
				<div class="container">
					<div class="row">
						<div class="col-sm-8"></div>
						<div class="col-sm-12 text-right">
							<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<script src="../js/jquery.min.js"></script>
		<script src="../bootstrap/js/bootstrap.min.js"></script>
		<script src="../js/jquery.ba-resize.min.js"></script>
		<script src="../js/jquery_cookie.min.js"></script>
		<script src="../js/retina.min.js"></script>
		<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
		<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
		<script src="../js/tinynav.js"></script>
		<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
		<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
		<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
		<script src="../js/ebro_common.js"></script>
		<script src="../js/lib/peity/jquery.peity.min.js"></script>
		<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
		<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="../js/lib/flot/jquery.flot.min.js"></script>
		<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
		<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
		<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
		<script src="../js/lib/flot/jquery.flot.resize.js"></script>
		<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
		<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
		<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
		<script src="../js/pages/ebro_dashboard.js"></script>
		
		<div class="style_items" id="sidebar_switch">
			<p class="style_title">Sidebar position</p>
			<label class="radio-inline">
				<input type="radio" name="sidebar_position" id="sidebar_left" value="left" checked> Left
			</label>
			<label class="radio-inline">
				<input type="radio" name="sidebar_position" id="sidebar_right" value="right"> Right
			</label>
		</div>
		
		
	</div>
</body>
</html>





<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>