<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title >ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>





	<!-- BOOTSTRAP FOR SEARCHING -->
	<link  rel="stylesheet" href="../js/lib/dataTables/media/DT_bootstrap.css">
	<link rel="stylesheet" href="../js/lib/dataTables/extras/TableTools/media/css/TableTools.css">

</head>
<body class="sidebar_hidden">
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40" > <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>



					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">		
											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>








					
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">profile</a></li>	
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li class="active">
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>          
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 12.5%">
			</div>
		</section>





		<!-- UNTUK MENAMPILKAN DATA NASABAH -->
		<section class="container clearfix main_section";>
			<div id="main_content_outer" class="clearfix">
				<div id="main_content" >
					<!-- main content -->
					<div class="col-sm-12">
						<?php 
						$jml_transaksi_pinjam=0;
						$jml_transaksi_gadai=0;
						$jml_transaksi_investasi=0;

						$sql1 = "
						SELECT count(*) as jumlah
						FROM transaksi_peminjaman 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_pinjam = $row1['jumlah'];
						}

						$sql1 = "
						SELECT count(id) as jumlah
						FROM transaksi_gadai 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_gadai = $row1['jumlah'];
						}

						$sql1 = "
						SELECT count(id) as jumlah
						FROM transaksi_gadai 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_investasi = $row1['jumlah'];
						}
						?>
						<div class="row"> 
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<a href="transaksi_peminjaman.php"><img src="../../koperasi/gallery/peminjaman.jpg" style="width: 100%;"></a>
									</span>
									<h4><?php echo $jml_transaksi_pinjam; ?></h4>
									<small>Transkasi Peminjaman</small>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<a href="transaksi_gadai.php"><img src="../../koperasi/gallery/gadai.jpg" style="width: 100%;"></a>
									</span>
									<h4><?php echo $jml_transaksi_gadai; ?></h4>
									<small>Transaksi Gadai</small>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<a href="transaksi_investasi.php"><img src="../../koperasi/gallery/investasi.jpg" style="width: 100%;"></a>
									</span>
									<h4><?php echo $jml_transaksi_investasi; ?></h4>
									<small>Transaksi Invesatasi</small>
								</div>
							</div> 
						</div>
					</div>
					<div class="col-lg-12">
						<br>
						<br>
						<div id="div_chart_kec_count_penduduk"></div>
						<br>
						<div id="sliders">
							<table>
								<tr>
									<td>Alpha Angle</td>
									<td><input id="alpha" type="range" min="0" max="45" value="0"/> <span id="alpha-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Beta Angle</td>
									<td><input id="beta" type="range" min="-45" max="45" value="11"/> <span id="beta-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Depth</td>
									<td><input id="depth" type="range" min="20" max="100" value="100"/> <span id="depth-value" class="value"></span></td>
								</tr>
							</table>
						</div>
						<div class="row">
							<div class="col-lg-12"> 
								<dir id="chart_pie"></dir>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>



		<div id="footer_space"></div>
	</div>






	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/peity/jquery.peity.min.js"></script>
	<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="../js/lib/flot/jquery.flot.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.resize.js"></script>
	<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
	<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
	<script src="../js/pages/ebro_dashboard.js"></script>







	<!--[[ page specific plugins ]]-->
	<!-- datatables -->
	<script src="../js/lib/dataTables/media/js/jquery.dataTables.min.js"></script>
	<!-- datatables column reorder -->
	<script src="../js/lib/dataTables/extras/ColReorder/media/js/ColReorder.min.js"></script>
	<!-- datatable fixed columns -->
	<script src="../js/lib/dataTables/extras/FixedColumns/media/js/FixedColumns.min.js"></script>
	<!-- datatables column toggle visibility -->
	<script src="../js/lib/dataTables/extras/ColVis/media/js/ColVis.min.js"></script>
	<!-- datatable table tools -->
	<script src="../js/lib/dataTables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script src="../js/lib/dataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>
	<!-- datatable bootstrap style -->
	<script src="../js/lib/dataTables/media/DT_bootstrap.js"></script>

	<script src="../js/pages/ebro_datatables.js"></script>

	<script src="../highcharts/highcharts.js"></script>
	<script src="../highcharts/highcharts-3d.js"></script>
	<script src="../highcharts/exporting.js"></script>
	<script src="../highcharts/export-data.js"></script>

	<div class="style_items" id="sidebar_switch">
	</div>

	<script type="text/javascript"> 

// Set up the chart
var chart_div_chart_kec_count_penduduk = new Highcharts.Chart({
	colors: ['#33cc33', '#ff3300', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
	chart: {
		renderTo: 'div_chart_kec_count_penduduk',
		type: 'column',
		options3d: {
			enabled: true,
			alpha: 0,
			beta: 11,
			depth: 100,
			viewDistance: 25
		}
	},
	title: {
		text: 'Jumlah Transaksi Selesai & Belum Selesai'
	}, 
	plotOptions: {
		column: {
			depth: 25
		}
	},
	xAxis: {
		categories: ['Transaksi Peminjaman', 'Transaksi Gadai', 'Transaksi Investasi'],
		labels: {
			skew3d: true,
			style: {
				fontSize: '16px'
			}
		}
	},
	yAxis: {
		title: {
			text: null
		}
	},
	series: [  
	{
		<?php 
		$jml_lunas_transaksi_pinjam=0;
		$jml_lunas_transaksi_gadai=0;
		$jml_lunas_transaksi_investasi=0;

		$sql1 = "SELECT 
		(SELECT COUNT(no_kontrak) FROM transaksi_peminjaman WHERE status_transaksi=0) as jml_peminjaman,
		(SELECT COUNT(id) FROM transaksi_gadai WHERE status_transaksi=0) as jml_gadai,
		(SELECT COUNT(id) FROM transaksi_investasi WHERE status_transaksi=1) as jml_investasi

		FROM transaksi_gadai 
		LIMIT 1
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) { 
			$jml_lunas_transaksi_pinjam=$row1['jml_peminjaman'];
			$jml_lunas_transaksi_gadai=$row1['jml_gadai'];
			$jml_lunas_transaksi_investasi=$row1['jml_investasi'];
		}
		?>
		name: 'Sudah Selesai ',
		data: [<?php echo $jml_lunas_transaksi_pinjam; ?>, <?php echo $jml_lunas_transaksi_gadai; ?>, <?php echo $jml_lunas_transaksi_investasi; ?>],
		point: {
			events: {
				click: function () {
					// alert('ok');
				}
			}
		}
	}, 
	{
		<?php 
		$jml_blm_lunas_transaksi_pinjam=0;
		$jml_blm_lunas_transaksi_gadai=0;
		$jml_blm_lunas_transaksi_investasi=0;

		$sql1 = "SELECT 
		(SELECT COUNT(no_kontrak) FROM transaksi_peminjaman WHERE status_transaksi=1) as jml_peminjaman,
		(SELECT COUNT(id) FROM transaksi_gadai WHERE status_transaksi=1) as jml_gadai,
		(SELECT COUNT(id) FROM transaksi_investasi WHERE status_transaksi=0) as jml_investasi

		FROM transaksi_gadai 
		LIMIT 1
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) { 
			$jml_blm_lunas_transaksi_pinjam=$row1['jml_peminjaman'];
			$jml_blm_lunas_transaksi_gadai=$row1['jml_gadai'];
			$jml_blm_lunas_transaksi_investasi=$row1['jml_investasi'];
		}
		?>
		name: 'Belum Selesai',
		data: [<?php echo $jml_blm_lunas_transaksi_pinjam; ?>, <?php echo $jml_blm_lunas_transaksi_gadai; ?>, <?php echo $jml_blm_lunas_transaksi_investasi; ?>],
		point: {
			events: {
				click: function () {
					// alert('ok');
				}
			}
		}
	},  
	]
});

function showValues() {
	$('#alpha-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.alpha);
	$('#beta-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.beta);
	$('#depth-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
	chart_div_chart_kec_count_penduduk.options.chart.options3d[this.id] = parseFloat(this.value);
	showValues();
	chart_div_chart_kec_count_penduduk.redraw(false);
});

showValues();


Highcharts.chart('chart_pie', {
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: false,
		type: 'pie'
	},
	title: {
		text: 'Jumlah Jaminan'
	},
	tooltip: {
		pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	},
	plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,
				format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				style: {
					color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
				}
			}
		}
	},
	series: [{
		name: 'Persentase',
		colorByPoint: true,
		data: [  
		<?php
		$sql1 = "
		SELECT 
		CASE 
		WHEN jaminan = 0 
		THEN 'BPKB'
		WHEN jaminan = 1
		THEN 'Kendaraan'
		ELSE 'BPKB + KENDARAAN'
		END as jenis_jaminan 
		, count(no_kontrak) as jumlah
		from transaksi_peminjaman
		group by jaminan
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jenis_jaminan = $row1['jenis_jaminan'];
			$jumlah = $row1['jumlah'];
			?> 
			{
				name: '<?php echo $jenis_jaminan; ?> (<?php echo $jumlah; ?>)',
				y: <?php echo $jumlah; ?>,
				sliced: true,
				selected: true
			},
			<?php 
		} 
		?>
		// {
		// 	name: 'testes (54654)',
		// 	y: 4545,
		// 	sliced: true,
		// 	selected: true
		// },
		]
	}]
});
</script>

</body>	 
</html>





<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>