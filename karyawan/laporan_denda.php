<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php';

$jumlahNotifikasi = 0;
$tws = getDateNow();

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!-- BOOTSTRAP FOR SEARCHING -->
	<link  rel="stylesheet" href="../js/lib/dataTables/media/DT_bootstrap.css">
	<link rel="stylesheet" href="../js/lib/dataTables/extras/TableTools/media/css/TableTools.css">
</head>
<body class="sidebar_hidden">
	<?php 
	$pesan_transaksi_peminjaman = ''; 
	if (isset($_SESSION['pesan_transaksi_investasi'])) {
		$pesan_transaksi_investasi = $_SESSION['pesan_transaksi_investasi'];
		unset($_SESSION['pesan_transaksi_investasi']);
		echo '<script>alert("'.$pesan_transaksi_investasi.'")</script>';
	}

	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>




					<!-- NOTIFIKASI  --> 
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						

						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>




						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<!--<span class="label label-danger">12</span>-->
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>            
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li class="active">             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>
		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 13%">
				<ul>
					<li><a href="#">Laporan</a></li>
					<li><span>Laporan Denda</span></li>						
				</ul>
			</div>
		</section>

		<?php
		$pilihBulan = 0;
		$pilihTahun = 0;
		if(isset($_POST['bulan'])){
			$pilihBulan = $_POST['bulan'];
		}
		if(isset($_POST['tahun'])){
			$pilihTahun = $_POST['tahun'];
		}
		?>

		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix">
				<div id="main_content">

					<!-- main content -->
					<div class="col-sm-12">
						<form class="form-inline" method="POST">
							<div class="row">
								<div class="col-sm-2"><center><strong>BULAN : </strong></center></div>
								<div class="col-sm-3">
									<select name="bulan">
										<?php echo generateBulanFilter($pilihBulan); ?>
									</select>
								</div>
								<div class="col-sm-2"><center><strong>TAHUN : </strong></center></div>
								<div class="col-sm-3">
									<select name="tahun">
										<?php echo generateTahunFilter($conn, $pilihTahun); ?>
									</select>
								</div>
								<div class="col-sm-2">
									<input type="hidden" name="tampil_laporan">
									<!-- <button type="submit" class="btn btn-danger">Cek</button> -->
									<button type="submit" class="btn btn-danger btn-block">Cek</button>
								</div>
							</div>
						</form>
					</div>
					<?php
					$hari_ini = date("$pilihTahun-$pilihBulan-01");
					$tgl_pertama = date('Y-m-01', strtotime($hari_ini)) . ' 00:00:00';
					$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)) . ' 23:59:59';
					//echo '<script type="text/javascript">alert(\'' . $tgl_pertama . '_' . $tgl_terakhir . '_' . $hari_ini . '\')</script>';
					$angka_awal = (int)substr($tgl_pertama,8,2);
					$angka_akhir = (int)substr($tgl_terakhir,8,2);
					$array_data_tabel = array();
					$jumlah = 0;
					if(isset($_POST['tampil_laporan'])){
						$iArr = 0;
						$sql1 = "SELECT  DATE_FORMAT(tanggal, '%d-%m-%Y') as tanggal, sum(denda) as 'total_denda' FROM (
										SELECT id as 'id', date(real_tgl_bayar) as 'tanggal', jumlah_denda as 'denda', jumlah_bayar as 'bayar' FROM `transaksi_angsuran` WHERE real_tgl_bayar IS NOT NULL
										UNION ALL
										SELECT id as 'id', date(real_tgl_bayar) as 'tanggal', jumlah_denda as 'denda', jumlah_bayar as 'bayar' FROM `transaksi_angsuran_gadai` WHERE real_tgl_bayar IS NOT NULL
										UNION ALL
										SELECT id as 'id', date(tanggal), 0 as 'denda', jumlah_uang as 'bayar' FROM `transaksi_investasi` where transaksi_investasi_id IS NULL
									)t WHERE (tanggal BETWEEN '$tgl_pertama' AND '$tgl_terakhir') GROUP BY tanggal";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { die("SQL Error Result1 "); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jumlah += $row1['total_denda'];
							$array_data_tabel[$iArr] = array($row1['tanggal'], $row1['total_denda']);
							$iArr++;
						}
					}
					?>
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: rgb(0, 128, 128)">
								<h4 class="panel-title" style="color: white">PENDAPATAN DENDA</h4>
							</div>
							<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table id="dt_table_tools" class="table table-striped">
									<thead>
										<tr>
											<th><center>Tanggal</center></th>
											<th><center>Total Denda</center></th>
										</tr>
									</thead>
									<tbody role="alert" aria-live="polite" aria-relevant="all">
										<?php
										if(isset($_POST['tampil_laporan'])){
											for($i = $angka_awal; $i <= $angka_akhir; $i++){
												$tglLoop = date('d-m-Y', strtotime(date("$pilihTahun-$pilihBulan-$i")));
												$tl = substr($tglLoop, 6, 4) . "-" . substr($tglLoop, 3, 2) . "-" . substr($tglLoop, 0, 2);
												?>
												<tr>
													<td>
														<center><a onclick="buatSession('<?php echo $tl; ?>')" data-toggle="modal" data-target="#modal_detail_laporan_denda"><?php echo $tglLoop; ?></a></center>
													</td>
													<td>
														<center>
															<?php
															$txtTest = "";
															for($j = 0; $j < count($array_data_tabel); $j++){
																if($tglLoop == $array_data_tabel[$j][0] && $array_data_tabel[$j][1] > 0){
																	$txtTest = "Rp. " . number_format($array_data_tabel[$j][1],2,',','.');
																} 
															}
															if($txtTest == ""){
																$txtTest = "Rp. " . number_format(0,2,',','.');
															}
															echo $txtTest;
															?>
														</center>
													</td>
												</tr>
												<?php
											}
										}
										?>
										<tr>
											<td><center>Total : </center></td>

											<td><center>Rp. <?php echo number_format($jumlah,2,',','.'); ?></center></td>
										</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<div id="footer_space"></div>
</div>

<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"></div>
				<div class="col-sm-12 text-right">
					<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>
</div>




<div class="modal fade" id="modal_detail_laporan_denda">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Detail Laporan Denda</h4>
			</div>
			<div class="modal-body" style="overflow-x: auto;">

			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>






<script src="../js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.ba-resize.min.js"></script>
<script src="../js/jquery_cookie.min.js"></script>
<script src="../js/retina.min.js"></script>
<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
<script src="../js/tinynav.js"></script>
<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
<script src="../js/ebro_common.js"></script>
<script src="../js/lib/peity/jquery.peity.min.js"></script>
<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../js/lib/flot/jquery.flot.min.js"></script>
<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
<script src="../js/lib/flot/jquery.flot.resize.js"></script>
<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
<script src="../js/pages/ebro_dashboard.js"></script>


<!--[[ page specific plugins ]]-->
<!-- datatables -->
<script src="../js/lib/dataTables/media/js/jquery.dataTables.min.js"></script>
<!-- datatables column reorder -->
<script src="../js/lib/dataTables/extras/ColReorder/media/js/ColReorder.min.js"></script>
<!-- datatable fixed columns -->
<script src="../js/lib/dataTables/extras/FixedColumns/media/js/FixedColumns.min.js"></script>
<!-- datatables column toggle visibility -->
<script src="../js/lib/dataTables/extras/ColVis/media/js/ColVis.min.js"></script>
<!-- datatable table tools -->
<script src="../js/lib/dataTables/extras/TableTools/media/js/TableTools.min.js"></script>
<script src="../js/lib/dataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>
<!-- datatable bootstrap style -->
<script src="../js/lib/dataTables/media/DT_bootstrap.js"></script>

<script src="../js/pages/ebro_datatables.js"></script>

<script type="text/javascript">
	function buatSession(tgl){
		$(".modal-body").load("bagian/bagian_modal_laporan_denda.php?tgl_laporan="+tgl);
	}
</script>

</div>
</body>
</html>

<?php

function getDateNow(){
	$tgl_waktu_sekarang = date('d-m-Y h:i:s');
	return explode(" ", $tgl_waktu_sekarang);
}

function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}

function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}

function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}

function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}

function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}

function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

function generateBulanFilter($pilihBulan){
	$test = '';
	if($pilihBulan == 1){
		$test = $test . '<option value="1" selected>Januari</option>';
	} else {
		$test = $test . '<option value="1">Januari</option>';	
	}
	if($pilihBulan == 2){
		$test = $test . '<option value="2" selected>Februari</option>';
	} else {
		$test = $test . '<option value="2">Februari</option>';	
	}
	if($pilihBulan == 3){
		$test = $test . '<option value="3" selected>Maret</option>';
	} else {
		$test = $test . '<option value="3">Maret</option>';	
	}
	if($pilihBulan == 4){
		$test = $test . '<option value="4" selected>April</option>';
	} else {
		$test = $test . '<option value="4">April</option>';	
	}
	if($pilihBulan == 5){
		$test = $test . '<option value="5" selected>Mei</option>';
	} else {
		$test = $test . '<option value="5">Mei</option>';	
	}
	if($pilihBulan == 6){
		$test = $test . '<option value="6" selected>Juni</option>';
	} else {
		$test = $test . '<option value="6">Juni</option>';	
	}
	if($pilihBulan == 7){
		$test = $test . '<option value="7" selected>Juli</option>';
	} else {
		$test = $test . '<option value="7">Juli</option>';	
	}
	if($pilihBulan == 8){
		$test = $test . '<option value="8" selected>Agustus</option>';
	} else {
		$test = $test . '<option value="8">Agustus</option>';	
	}
	if($pilihBulan == 9){
		$test = $test . '<option value="9" selected>September</option>';
	} else {
		$test = $test . '<option value="9">September</option>';	
	}
	if($pilihBulan == 10){
		$test = $test . '<option value="10" selected>Oktober</option>';
	} else {
		$test = $test . '<option value="10">Oktober</option>';	
	}
	if($pilihBulan == 11){
		$test = $test . '<option value="11" selected>November</option>';
	} else {
		$test = $test . '<option value="11">November</option>';	
	}
	if($pilihBulan == 12){
		$test = $test . '<option value="12" selected>Desember</option>';
	} else {
		$test = $test . '<option value="12">Desember</option>';	
	}
	return $test;
}

function generateTahunFilter($conn, $pilihTahun){
	$test = '';
	$sql1 = "SELECT DISTINCT YEAR(tanggal) AS tahun FROM (SELECT date(real_tgl_bayar) as 'tanggal' FROM `transaksi_angsuran` WHERE real_tgl_bayar IS NOT NULL UNION ALL SELECT date(real_tgl_bayar) as 'tanggal' FROM `transaksi_angsuran_gadai` WHERE real_tgl_bayar IS NOT NULL UNION ALL SELECT date(tanggal) FROM `transaksi_investasi` where transaksi_investasi_id IS NULL)t GROUP BY tanggal";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		if($row1['tahun'] == $pilihTahun){
			$test = $test . '<option value="' . $row1['tahun'] . '" selected>' . $row1['tahun'] . '</option>';
		} else {
			$test = $test . '<option value="' . $row1['tahun'] . '">' . $row1['tahun'] . '</option>';
		}
	}
	return $test;
}

?>
<?php ob_end_flush(); ?>