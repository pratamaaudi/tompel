<?php
ob_start();
session_start();
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php 
	if(isset($_POST['sukuBunga'])){
		$sukuBunga = $_POST['sukuBunga'];
		$transaksi = $_POST['transaksi'];
		$result1 = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE jumlah_bunga=$sukuBunga AND kode_transaksi=$transaksi");
		if (!$result1) { die("SQL ERROR : result1"); }
		if (mysqli_num_rows($result1)) {
			$row1 = mysqli_fetch_array($result1);
			$idSukuBunga = $row1['id'];
			if($transaksi == 2){
				$result10 = mysqli_query($conn, "UPDATE suku_bunga SET shapus = 1 WHERE kode_transaksi = 2");
				if (!$result10) { die("SQL ERROR : result10"); }
			}
			$result2 = mysqli_query($conn, "UPDATE suku_bunga SET shapus = 0 WHERE id = $idSukuBunga");
			if (!$result2) { die("SQL ERROR : result2"); }
		} else {
			if($transaksi == 2){
				$result20 = mysqli_query($conn, "UPDATE suku_bunga SET shapus = 1 WHERE kode_transaksi = 2");
				if (!$result20) { die("SQL ERROR : result20"); }
			}
			$result3 = mysqli_query($conn, "INSERT INTO suku_bunga(jumlah_bunga, kode_transaksi, shapus) VALUES ($sukuBunga, $transaksi, 0)");
			if (!$result3) { die("SQL ERROR : result3"); }
		}
		header('Location: bunga.php');
	}
	if(isset($_GET['dIdBunga'])){
		$idSukuBunga = $_GET['dIdBunga'];
		$result1 = mysqli_query($conn, "UPDATE suku_bunga SET shapus = 1 WHERE id = $idSukuBunga");
		if (!$result1) { die("SQL ERROR : result1"); }
		header('Location: bunga.php');
	} 
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					
					<div class="navbar-header">	
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>




					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">		
											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>


					
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li class="active">               
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<!--ul>
					<li><a href="#">Ebro Admin</a></li>
					<li><span>Dashboard</span></li>						
				</ul-->
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(224, 246, 255); padding: 20px 5px 20px 5px">
				<div id="main_content">

					<!-- main content -->
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: rgb(0, 128, 128)">
								<h4 style="font-family: arial; font-size: 30px; color: white" class="panel-title"><strong><center>DAFTAR SUKU BUNGA</center></strong></h4>
							</div>
							<div class="panel-body">
								<fieldset>
									<?php if ($_SESSION['status_pegawai'] == 1) { ?>
										<form class="form-inline" method="POST" style=" padding-top: 12px; margin-bottom: 15px; border-radius: 5px;">
											<div class="row">
												<div class="form-group" style="width: 100%;">
													<label class="col-sm-2 control-label" align="right" style="font-style: inherit;"><h4>Suku Bunga:</h4></label>
													<div class="col-sm-2">
														<input type="text" class="form-control" name="sukuBunga" required="">
													</div>
													<div class="col-sm-1">
														<input type="text" class="form-control" readonly="" value="%">
													</div>
													<label class="col-sm-2 control-label" align="right"><h4>Transaksi:</h4></label>
													<div class="col-sm-2">
														<select name="transaksi" class="form-control">
															<option value="0" selected="">Peminjaman</option>
															<option value="1">Gadai</option>
															<option value="2">Investasi</option>
														</select>
													</div>
													<div class="col-sm-2">
														<button type="submit" class="btn btn-success btn-success"> <i class="icon-plus"></i> Tambah</button>
													</div>
												</div>
											</div>
										</form>
									<?php } ?>
								</fieldset>
								<div class="col-sm-4">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">Bunga Peminjaman</h4>
										</div>
										<div class="dataTable_wrapper form-inline" role="grid">
											<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
												<thead>
													<tr role="row">
														<!--th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dt_basic" rowspan="1" colspan="1" aria-sort="ascending" aria-label="id: activate to sort column descending" style="width: 18px;">No</th-->
														<th>No</th>
														<th>Bunga</th>
														<th>Alat</th>
													</tr>
												</thead>
												<tbody role="alert" aria-live="polite" aria-relevant="all">
													<!--tr class="odd">
														<td class="  sorting_1">1</td>
														<td class=" ">Afghanistan</td>
														<td class=" ">Islamic Republic of Afghanistan</td>
														<td class=" ">Taek</td>
													</tr-->
													<?php 
													$no1 = 0;
													$result1 = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 0 AND shapus = 0");
													if (!$result1) { die("SQL Error Result1 "); }
													while ($allRow1 = mysqli_fetch_array($result1)) {
														$no1++;
														?>
														<tr class="odd">
															<td><?php echo $no1; ?></td>
															<td><?php echo $allRow1['jumlah_bunga']; ?> %</td>
															<td>
																<?php if ($_SESSION['status_pegawai'] == 1) { ?>
																	<a class="btn btn-danger" style="cursor: pointer;" href="bunga.php?dIdBunga=<?php echo $allRow2['id']; ?>"> <i class="icon-trash"></i> delete</a>
																<?php } ?>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">Bunga Gadai</h4>
										</div>
										<div class="dataTable_wrapper form-inline" role="grid">
											<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
												<thead>
													<tr role="row">
														<!--th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dt_basic" rowspan="1" colspan="1" aria-sort="ascending" aria-label="id: activate to sort column descending" style="width: 18px;">No</th-->
														<th>No</th>
														<th>Bunga</th>
														<th>Alat</th>
													</tr>
												</thead>
												<tbody role="alert" aria-live="polite" aria-relevant="all">
													<!--tr class="odd">
														<td class="  sorting_1">1</td>
														<td class=" ">Afghanistan</td>
														<td class=" ">Islamic Republic of Afghanistan</td>
														<td class=" ">Taek</td>
													</tr-->
													<?php 
													$no2 = 0;
													$result2 = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 1 AND shapus = 0");
													if (!$result2) { die("SQL Error Result2 "); }
													while ($allRow2 = mysqli_fetch_array($result2)) {
														$no2++;
														?>
														<tr class="odd">
															<td><?php echo $no2; ?></td>
															<td><?php echo $allRow2['jumlah_bunga']; ?> %</td>
															<td>
																<?php if ($_SESSION['status_pegawai'] == 1) { ?>
																	<a class="btn btn-danger" style="cursor: pointer;" href="bunga.php?dIdBunga=<?php echo $allRow2['id']; ?>"> <i class="icon-trash"></i> delete</a>
																<?php } ?>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">Bunga Investasi</h4>
										</div>
										<div class="dataTable_wrapper form-inline" role="grid">
											<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
												<thead>
													<tr role="row">
														<!--th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="dt_basic" rowspan="1" colspan="1" aria-sort="ascending" aria-label="id: activate to sort column descending" style="width: 18px;">No</th-->
														<th>No</th>
														<th>Bunga</th>
														<th>Alat</th>
													</tr>
												</thead>
												<tbody role="alert" aria-live="polite" aria-relevant="all">
													
													<!-- <?php 
													$no3 = 0;
													$result3 = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 2 AND shapus = 0");
											    	if (!$result3) { die("SQL Error Result3 "); }
											    	while ($allRow3 = mysqli_fetch_array($result3)) {
													$no3++;
													?>
													<tr class="odd">
														<td><?php echo $no3; ?></td>
														<td><?php echo $allRow3['jumlah_bunga']; ?> %</td>
														<td><center>Active</center></td>
													</tr>
													<?php } ?> -->



													<?php 
													$no3 = 0;
													$result3 = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 2 AND shapus = 0");
													if (!$result3) { die("SQL Error Result3 "); }
													while ($allRow3 = mysqli_fetch_array($result3)) {
														$no3++;
														?>
														<tr class="odd">
															<td><?php echo $no3; ?></td>
															<td><?php echo $allRow3['jumlah_bunga']; ?> %</td>
															<td>
																<?php if ($_SESSION['status_pegawai'] == 1) { ?>
																	<a class="btn btn-danger" style="cursor: pointer;" href="bunga.php?dIdBunga=<?php echo $allRow2['id']; ?>"> <i class="icon-trash"></i> delete</a>
																<?php } ?>
															</td>
														</tr>
													<?php } ?>




												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>
	
	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/peity/jquery.peity.min.js"></script>
	<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="../js/lib/flot/jquery.flot.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.resize.js"></script>
	<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
	<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
	<script src="../js/pages/ebro_dashboard.js"></script>
	

</div>
</body>
</html>




<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>