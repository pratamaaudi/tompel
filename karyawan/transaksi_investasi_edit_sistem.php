<?php

ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php';

if (isset($_POST['update_selesai_investasi'])) {
    $id_investasi = $_POST['id_investasi'];
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `status_transaksi` = '1' WHERE `transaksi_investasi`.`id` = $id_investasi;");
    if (!$result) {
        die("SQL Error Result ");
    }
    backToTransaksiInvestasi();
}

if (isset($_POST['update_status_pencairan'])) {
    $id_investasi = $_POST['id_investasi'];
    $tglSekarang = new DateTime();
    $tglSekarangString = $tglSekarang->format('Y-m-d');
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `status_cair` = '1', `tanggal_cair` = '$tglSekarangString' WHERE `transaksi_investasi`.`id` = $id_investasi;");
    if (!$result) {
        die("SQL Error Result ");
    }
    backToTransaksiInvestasi();
}

if (isset($_POST['update_setujui_pencairan'])) {
    $id = $_POST['id'];
    $pegawai_id = $_POST['id_pegawai'];
    $tglSekarang = new DateTime();
    $tglSekarangString = $tglSekarang->format('Y-m-d');
    $result = mysqli_query($conn, "UPDATE `transaksi_pencairan_dana` SET `status` = '1', `pegawai_id` = '$pegawai_id' WHERE `transaksi_pencairan_dana`.`id` = $id;");
    if (!$result) {
        die("SQL Error Result ");
    }

    kurangiSaldoInvestasi($_POST['jumlah'], $_POST['investasiId'], $conn);

    $investasiId = $_POST['investasiId'];
    kirimNotifikasiKeNasbah($conn, $investasiId, getUserIdNasabah($conn, $_POST['nasabah_id']), "permohon mencariran dana investasi no $investasiId anda telah disetujui");
    
    backToPencairanPending();
}

function kurangiSaldoInvestasi($jumlah, $investasiId, $conn){
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `jumlah_uang` = (jumlah_uang-$jumlah) WHERE `transaksi_investasi`.`id` = $investasiId;");
    if (!$result) {
        die("SQL Error Result ");
    }
}

if (isset($_POST['update_tolak_pencairan'])) {
    $id = $_POST['id'];
    $pegawai_id = $_POST['id_pegawai'];
    $tglSekarang = new DateTime();
    $tglSekarangString = $tglSekarang->format('Y-m-d');
    $result = mysqli_query($conn, "UPDATE `transaksi_pencairan_dana` SET `shapus` = '1', `pegawai_id` = '$pegawai_id' WHERE `transaksi_pencairan_dana`.`id` = $id;");
    if (!$result) {
        die("SQL Error Result ");
    }

    $investasiId = $_POST['investasiId'];

    kirimNotifikasiKeNasbah($conn, $investasiId, getUserIdNasabah($conn, $_POST['nasabah_id']), "permohon mencariran dana investasi no $investasiId anda telah ditolak");
    
    backToPencairanPending();
}

if (isset($_POST['update_setujui_investasi'])) {
    $id = $_POST['id'];
    $pegawaiId = $_POST['id_pegawai'];
    $investasiId = $_POST['id'];
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `status_transaksi` = '0', `pegawai_id` = '$pegawaiId' WHERE `transaksi_investasi`.`id` = $investasiId;");
    if (!$result) {
        die("SQL Error Result ");
    }
    kirimNotifikasiKeNasbah($conn, $investasiId, getUserIdNasabah($conn, $_POST['id_nasabah']), "permohonan investasi anda dengan no investasi $investasiId telah disetujui oleh koperasi");
    backToTransaksiInvestasi($conn);
}

if(isset($_POST['update_status_cair_bunga'])){
    $dateNow = getDateNow();
    $id = $_POST['id'];
    $result = mysqli_query($conn, "UPDATE `transaksi_pendapatan_bunga_investasi` SET `status_cair` = '1', `tanggal_cair` = '$dateNow[0]' WHERE `transaksi_pendapatan_bunga_investasi`.`id` = $id;");
    if (!$result) {
        die("SQL Error Result ");
    }
    backToPendapatanBunga($conn);
}

function getDateNow(){
    $tgl_waktu_sekarang = date('Y-m-d h:i:s');
    return explode(" ", $tgl_waktu_sekarang);
}

function kirimNotifikasiKeNasbah($conn, $investasiId, $userId, $msg){
    $result = mysqli_query($conn, "INSERT INTO `notifikasi` (
        `id`, 
        `keterangan`, 
        `transaksi_peminjaman_no_kontrak`, 
        `transaksi_investasi_id`, 
        `transaksi_gadai_id`, 
        `user_id`, 
        `status_baca`) 
        VALUES (
        NULL, 
        '$msg', 
        NULL, 
        '$investasiId', 
        NULL, 
        '$userId', 
        '0');");
    if (!$result) { die("SQL ERROR : gagal insert data"); }else{
        $_SESSION['alert_invesasi_insert'] = true;
    }
}

function getUserIdNasabah($conn, $nasabahId){
    $result4 = mysqli_query($conn, "SELECT user_id FROM `nasabah` WHERE id = $nasabahId");
    if (!$result4) { die("SQL ERROR : result4"); }  
    $row4 = mysqli_fetch_array($result4);
    return $row4['user_id'];
}

function backToTransaksiInvestasi(){
    header('Location: transaksi_investasi.php');
}

function backToPencairanPending(){
    header('Location: transaksi_investasi_pencairan_pending.php');
}

function backToPendapatanBunga(){
    header('Location: transaksi_pendapatan_bunga_investasi.php');
}

if (isset($_POST['update_tolak_investasi'])) {
    $id = $_POST['id'];
    $pegawaiId = $_POST['id_pegawai'];
    $investasiId = $_POST['id'];
    $result = mysqli_query($conn, "UPDATE `transaksi_investasi` SET `shapus` = '1', `pegawai_id` = '$pegawaiId' WHERE `transaksi_investasi`.`id` = $investasiId;");
    if (!$result) {
        die("SQL Error Result ");
    }
    kirimNotifikasiKeNasbah($conn, $investasiId, getUserIdNasabah($conn, $_POST['id_nasabah']), "permohonan investasi anda dengan no investasi $investasiId telah ditolak oleh koperasi");
    backToTransaksiInvestasi($conn);
}

?>