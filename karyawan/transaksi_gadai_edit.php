<?php
ob_start();
session_start();
date_default_timezone_set('Asia/Jakarta');
if (!isset($_SESSION['login_pegawai'])) {
	header("Location: login.php");
}
require '../config.php'; 
$jumlahNotifikasi = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php
	$id_transaksi_gadai = 0;
	if (isset($_GET['id_transaksi'])) {
		$id_transaksi_gadai = $_GET['id_transaksi'];
	} else {
		header("Location: transaksi_peminjaman.php");
	}

	$result2 = mysqli_query($conn, "SELECT *, tg.shapus AS shapus_tg FROM transaksi_gadai tg LEFT JOIN perubahan_suku_bunga psb ON tg.id = psb.id_transaksi_gadai LEFT JOIN suku_bunga sb ON psb.id_suku_bunga = sb.id WHERE tg.id = $id_transaksi_gadai");
	$row2 = mysqli_fetch_array($result2);
	if(!mysqli_num_rows($result2)) {
		$_SESSION['pesan_transaksi_gadai'] = 'Transaksi Gadai Dengan Id Transaksi' . $id_transaksi_gadai . " Belum Di Buat";
		header("Location: transaksi_gadai.php");
	}
	if($row2['shapus_tg'] == 1){
		$_SESSION['pesan_transaksi_gadai'] = 'Transaksi Gadai Dengan Id Transaksi-' . $id_transaksi_gadai . " Sudah Di Hapus";
		unset($_SESSION['pesan_transaksi_gadai_edit']);
		header("Location: transaksi_gadai.php");
	}
	$nasabah = $row2['nasabah_id'];
	$suku_bunga = $row2['id_suku_bunga'];
	$s_suku_bunga = $row2['jumlah_bunga'];
	$jangka_waktu = $row2['jangka_waktu'];
	$tgl_waktu_sekarang = $row2['tanggal'];
	$tws = explode(" ", $tgl_waktu_sekarang);
	$tgl = substr($tws[0], 8, 2)."-".substr($tws[0], 5, 2)."-".substr($tws[0], 0, 4);
	$sts_transaksi = $row2['status_transaksi'];
	$sts_uang = $row2['status_cair'];
	
	$s_sts_transaksi = 'BELUM LUNAS';
	if($sts_transaksi == 0){ $s_sts_transaksi = 'LUNAS'; }
	
	$s_sts_uang = 'Belum Cair';
	if($sts_uang == 1){ $s_sts_uang = 'Sudah Cair'; }

	if(isset($_SESSION['pesan_transaksi_gadai_edit'])){ 
		$pesan = $_SESSION['pesan_transaksi_gadai_edit'];
		echo '<script type="text/javascript">alert("' . $pesan . '");</script>';
		unset($_SESSION['pesan_transaksi_gadai_edit']);
	}
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<a class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></a>	
						</div>	
					</div>




					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>	
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">

											<?php echo generateNotification($conn); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>	
					</div>


					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</header>						
		<div class="col-sm-2"></div>
		<div class="col-sm-8">					
			<nav id="top_navigation">
				<div class="container">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="nasabah.php">
								<i class="icon-group icon-2x"></i>
								<span class="menu_label">Nasabah</span>
							</a>
						</li>
						<li>             
							<a href="pegawai.php">
								<i class="icon-user icon-2x"></i>
								<span class="menu_label">Pegawai</span>
							</a>
						</li>
						<li>          
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
						<li>             
							<a href="bunga.php">
								<i class="icon-beaker icon-2x"></i>
								<span class="menu_label">Bunga</span>
							</a>
						</li>
						<li>             
							<a href="setting.php">
								<i class="icon-wrench icon-2x"></i>
								<span class="menu_label">Settings</span>
							</a>
						</li>
						<li>             
							<a href="laporan.php">
								<i class="icon-book icon-2x"></i>
								<span class="menu_label">Laporan</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>
		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 12.5%">
				<ul>
					<li><a href="pendataan.php">Pendataan</a></li>
					<li><a href="transaksi_gadai.php">Transaksi Gadai</a></li>
					<li><span>Detail Transaksi Gadai</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px; padding-bottom: 20px">
				<div id="main_content">
					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">

							<!-- START MODAL HAPUS-->
							<div class="modal fade" id="modalHapus">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Hapus Transaksi Gadai</h4>
										</div>
										<div class="modal-body">
											Apakah Anda Yakin ingin menghapus Transaksi Gadai Dengan Id Gadai-<b><?php echo $id_transaksi_gadai; ?> ?</b>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
											<a href="transaksi_gadai_edit_sistem.php?del_id_transaksi_gadai=<?php echo $id_transaksi_gadai ?>" class="btn btn-danger">Ya Yakin!</a>
										</div>
									</div>
								</div>
							</div>
							<!-- END MODAL HAPUS -->

							<div class="user_heading">
								<div class="row">
									<div class="col-sm-1 hidden-xs"></div>
									<div class="col-sm-10">
										<div class="user_heading_info">
											<div class="user_actions pull-right">
												<?php if($sts_transaksi != 5){?>
													<a href="#" class="edit_form" data-toggle="tooltip" data-placement="top auto" title="" data-original-title="Edit"><span class="icon-edit"></span></a>
													<a  data-toggle="modal" href="#modalHapus"  title="Remove Transaksi"><span class="icon-remove"></span></a>
												<?php } ?>
											</div>
											<center>
												<div class="user_actions pull-right"></div>
												<h1 style="color: white; font-size: 300%">Detail Gadai</h1>
											</center>
										</div>
									</div>
								</div>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-3">
										<form class="form-horizontal user_form" action="transaksi_gadai_edit_sistem.php" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-8 control-label" style="color: white"><strong>Tanggal</strong></label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white"><?php echo $tgl; ?></p>
													<div class="hidden_control">
														<input id="tgl_sekarang" type="date" class="form-control" name="tgl_sekarang" required="" value="<?php echo $tws[0]; ?>">
														<!---->
														<input type="hidden" name="tgl_lama" value="<?php echo $tgl_waktu_sekarang; ?>">	
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a col-sm-12" style="background-color: rgb(0, 191, 255)"><strong>Nasabah</strong></h3>
											<!-- <label class="col-sm-2" style="color: white; text-align: right;"><strong>No Kontrak : <?php echo $id_transaksi_gadai; ?></strong></label> -->
											<div class="form-group">
												<!--<label class="col-sm-2 control-label">Nasabah</label>-->
												<div class="col-sm-10 editable">
													<div class="hidden_control">
														<select id="nasabah" onchange="isi_data_nasabah()" name="nasabah" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">NIK :</label>
												<div class="col-sm-10">
													<p id="noKtp" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nama Lengkap :</label>
												<div class="col-sm-10">
													<p id="nama" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">Alamat :</label>
												<div class="col-sm-10">
													<p id="alamat" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">Pekerjaan :</label>
												<div class="col-sm-10">
													<p id="namaPekerjaan" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">Alamat Kerja :</label>
												<div class="col-sm-10">
													<p id="alamatPekerjaan" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">No. Telpon :</label>
												<div class="col-sm-10">
													<p id="telp" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"  style="color: white">No. Handphone :</label>
												<div class="col-sm-10">
													<p id="noHp" class="form-control-static"style="color: white"></p>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"style="background-color: rgb(0, 191, 255) class="heading_a"><strong>Barang</strong></h3>
											<div class="form-group">
												<!--label class="col-sm-2 control-label">Barang</label-->
												<div class="col-sm-10 editable">
													<div class="hidden_control">
														<select id="barang" onchange="isi_data_barang()" name="barang" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group" id="div_barang_gadai">
												<?php
												$jum_barang = 0;
												$resultBar = mysqli_query($conn, "SELECT * FROM barang b JOIN barang_transaksi_gadai btg ON b.id = btg.id_barang WHERE btg.id_transaksi_gadai = $id_transaksi_gadai");
												if (!$resultBar) { die("SQL Error ResultBar "); }
												if (mysqli_num_rows($resultBar)) {
													while ($rowBar = mysqli_fetch_array($resultBar)) { 
														$jum_barang++; 
														$sba = $rowBar['status_barang'];
														$sb = '';
														if($rowBar['status_barang'] == 1){
															$sb = 'Di Sita';
														} else if($rowBar['status_barang'] == 2){
															$sb = 'Di Kembalikan';
														} else if($rowBar['status_barang'] == 0){
															$sb = 'Di Tahan';
														} 
														?> 
														<div class="col-sm-3 editable" id="div1_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static" style="color: white"><?php echo $rowBar['nama_barang']; ?></p>
															<div id="div1_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<input type="text" name="nama_barang[]" class="form-control" readonly="" placeholder="nama barang" id="nama_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['nama_barang']; ?>">
																<input type="hidden" name="id_barang[]" id="id_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['id_barang']; ?>">
															</div> 
														</div> 
														<div class="col-sm-3 editable" id="div2_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static" style="color: white; display: none;"><?php echo 'Jumlah : ' . $rowBar['jumlah_barang']; ?></p>
															<div id="div2_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<input type="number" name="jumlah_barang[]" class="form-control" placeholder="jumlah barang" required="" id="jumlah_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['jumlah_barang']; ?>" style="display: none;"> 
															</div> 
														</div> 
														<div class="col-sm-3 editable" id="div3_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static" style="color: white"><?php echo 'Status : ' . $sb; ?></p>
															<div id="div3_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<select id="status_barang<?php echo $jum_barang;?>" name="status_barang[]" class="form-control"> 
																	<option value="0"<?php if($sba==0){ echo ' selected=""'; } ?>>
																		Di Tahan
																	</option> 
																	<option value="1"<?php if($sba==1){ echo ' selected=""'; } ?>>
																		Di Sita
																	</option> 
																	<option value="2"<?php if($sba==2){ echo ' selected=""'; } ?>>
																		Di Kembalikan
																	</option>
																</select> 
															</div> 
														</div> 
														<div class="col-sm-1 editable" id="div4_1_<?php echo $jum_barang;?>">
															<!-- <p class="form-control-static"style="color: white">-</p> -->
															<div id="div4_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<a class="btn btn-danger btn-block" id="btn<?php echo $jum_barang;?>" onclick="hapus_barang_gadai(<?php echo $jum_barang;?>)">X</a> 
															</div> 
														</div>
													<div class="col-sm-3 editable" id="div5_1_<?php echo $jum_barang;?>"> 
														<p class="form-control-static"style="color: rgb(0, 128, 128)">hidden</p>
													</div>
												<?php }
											} ?>
										</div>
										<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Terusan</strong></h3>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Nominal Pinjaman :</label>
											<div class="col-sm-4 editable">
												<!-- <p class="form-control-static"style="color: white"><?php echo $row2['jumlah_pinjaman']; ?></p> -->
												<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['jumlah_pinjaman'],2,',','.');?></p>
												
												<div class="hidden_control">
													<input id="nominal_pinjaman" type="number" class="form-control" name="nominal_pinjaman" required="" value="<?php echo $row2['jumlah_pinjaman']; ?>">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Jangka Waktu :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white"><?php echo $jangka_waktu . ' Bulan'; ?></p>
												<div class="hidden_control">
													<input type="hidden" name="jangka_waktu_lama" value="<?php echo $jangka_waktu; ?>">
													<select id="jangka_waktu" name="jangka_waktu" class="form-control" style="width: 70%; float: left;">
													</select>
													<label style="color: white"><strong>&nbsp BULAN</strong></label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Suku Bunga :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white"><?php echo $s_suku_bunga . ' %'; ?></p>
												<div class="hidden_control">
													<select id="suku_bunga" name="suku_bunga" class="form-control" style="width: 70%; float: left;">
													</select>
													<label style="color: white"><strong>&nbsp %</strong></label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Status Transaksi :</label>
											<div class="col-sm-4 editable">
												<p class="form-control-static"style="color: white"><?php echo $s_sts_transaksi; ?></p>
												<div class="hidden_control">
													<select id="status_transaksi" name="status_transaksi" class="form-control">
														<option value="1"<?php if($sts_transaksi == 1){ echo ' selected=""'; } ?>>Belum Lunas</option>
														<option value="0"<?php if($sts_transaksi == 0){ echo ' selected=""'; } ?>>Lunas</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Status Dana :</label>
											<div class="col-sm-4 editable">
												<p class="form-control-static"style="color: white"><?php echo $s_sts_uang; ?></p>
												<div class="hidden_control">
													<select id="status_cair" name="status_cair" class="form-control">
														<option value="0"<?php if($sts_uang == 0){ echo ' selected=""'; } ?>>Belum Cair</option>
														<option value="1"<?php if($sts_uang == 1){ echo ' selected=""'; } ?>>Sudah Cair</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label"></label>
											<div class="col-sm-8 editable">
												<p class="form-control-static"style="color: white"></p>
												<div class="hidden_control">
													<a class="btn btn-danger" style="float: right;" onclick="isiBiayaTotal()"><i class="icon-refresh"></i> SINKRONISASI</a>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Biaya Administrasi :</label>
											<div class="col-sm-8 editable">
												<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['biaya_administrasi'],2,',','.'); ?></p>

												<div class="hidden_control">
													<input id="biaya_administrasi" type="number" class="form-control" name="biaya_administrasi" style="width: 34.5%; float: left;" value="<?php echo $row2['biaya_administrasi']; ?>" readonly>
													<label style="color: white"><strong>&nbsp 5% * NOMINAL PEMINJAMAN</strong></label>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Total :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static" style="color: white">Rp. <?php echo number_format($row2['total'],2,',','.'); ?></p>

												<div class="hidden_control">
													<input id="total" type="number" class="form-control" name="total" value="<?php echo $row2['total']; ?>" readonly>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Nominal Bunga :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['jumlah_angsuran'],2,',','.'). " / Bulan"; ?></p>

												<div class="hidden_control">
													<input id="nominal_angsuran" type="number" class="form-control" name="nominal_angsuran" style="width: 70%; float: left;" value="<?php echo $row2['jumlah_angsuran']; ?>" readonly>
													<label style="color: white"><strong>&nbsp/ BULAN</strong></label>
												</div>
											</div>
										</div>

										<?php 
										$result_cek_lunas = mysqli_query($conn, "SELECT tg.status_transaksi, DATE_FORMAT(DATE_ADD(MAX(tag.tanggal_bayar), INTERVAL 7 DAY), '%d-%m-%Y') AS tgl_pemutihan FROM transaksi_gadai tg LEFT JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai");
										if (mysqli_num_rows($result_cek_lunas)) {
											$row_cek_lunas = mysqli_fetch_array($result_cek_lunas);
											if($row_cek_lunas['status_transaksi'] == 5){ 
												?>												
												<div class="form-group">
													<label class="col-sm-9 control-label" style="color: white"><strong>Tanggal Akhir Pemutihan</strong></label>
													<div class="col-sm-3 editable">
														<p class="form-control-static"style="color: white"><?php echo $row_cek_lunas['tgl_pemutihan']; ?></p>
														<div class="hidden_control">
														</div>
													</div>
												</div>
												<?php
											} 
										}
										?>


										<div class="form_submit clearfix" style="display: none;">
											<div class="row">
												<div class="col-sm-10 col-sm-offset-2">
													<input name="wkt_sekarang" type="hidden" value="<?php echo $tws[1]; ?>">
													<input name="edit_transaksi_gadai" type="hidden" value="<?php echo $id_transaksi_gadai; ?>">
													<input name="status_transaksi_lama" type="hidden" value="<?php echo $row2['status_transaksi']; ?>">
													<button type="submit" class="btn btn-success btn-success"><i class="icon-save"></i> SIMPAN</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12"><center>
						<div class="panel panel-default" style=" width: 98%">
							<div class="panel-heading">
								<h4 class="panel-title"><strong>Transaksi Bunga</strong>
									<?php 
									if($sts_transaksi == 1){ 
										?>
										<a class="btn btn-danger" style="float: right;" href="transaksi_gadai_angsur.php?id_transaksi=<?php echo $id_transaksi_gadai; ?>">BAYAR BUNGA</a>
									<?php } else if($sts_transaksi == 5){
										$result_cek_lunas = mysqli_query($conn, "SELECT tg.status_transaksi, DATEDIFF(DATE(NOW()), DATE(MAX(tag.tanggal_bayar))) AS sjt_akhir FROM transaksi_gadai tg LEFT JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai");
										if (mysqli_num_rows($result_cek_lunas)) {
											$row_cek_lunas = mysqli_fetch_array($result_cek_lunas);
											if($row_cek_lunas['sjt_akhir'] < 8){ 
												?>
												<a class="btn btn-danger" style="float: right;" href="transaksi_gadai_angsur.php?id_transaksi=<?php echo $id_transaksi_gadai; ?>">BAYAR BUNGA</a>
												<?php
											} 
										}
									} ?>
								</h4>
							</div>
							<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<div class="dt-top-row">
									<div class="dt-wrapper">
										<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
											<thead>
												<tr role="row">
													<th>Bunga Ke</th>
													<th>Jatuh Tempo</th>
													<th>Tanggal Transaksi</th>
													<th>Nominal</th>
													<th>Bayar</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody role="alert" aria-live="polite" aria-relevant="all">
												<?php 
												$result3 = mysqli_query($conn, "SELECT * FROM transaksi_angsuran_gadai WHERE transaksi_gadai_id = $id_transaksi_gadai");
												if (!$result3) { die("SQL Error Result3 "); }
												while ($row3 = mysqli_fetch_array($result3)) {
													?>
													<tr class="odd">
														<td><?php echo $row3['angsuran_ke'] ?></td>
														<td><?php echo substr($row3['tanggal_bayar'], 8, 2)."-".substr($row3['tanggal_bayar'], 5, 2)."-".substr($row3['tanggal_bayar'], 0, 4); ?></td>
														<td><?php echo substr($row3['real_tgl_bayar'], 8, 2)."-".substr($row3['real_tgl_bayar'], 5, 2)."-".substr($row3['real_tgl_bayar'], 0, 4); ?></td>
														<td>Rp. <?php echo number_format($row2['jumlah_angsuran'],0,',','.'); ?></td>
														<td><?php echo $row3['jumlah_bayar']; ?></td>
														<td>
															<?php 
															$sts_transaksi_angsuran = 'Belum Lunas';
															if($row2['jumlah_angsuran'] <= $row3['jumlah_bayar'] || $row3['status_angsuran'] == 1){ 
																$sts_transaksi_angsuran = 'Lunas'; 
															}
															if($row3['status_angsuran'] == 2 || $row3['status_angsuran'] == 3 || $row3['status_angsuran'] == 4){
																$sts_transaksi_angsuran = 'Pending';	
															}
															echo $sts_transaksi_angsuran; 
															?>
														</td>
													</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</center>
			</div>
		</div>
	</div>
</section>
<div id="footer_space"></div>
</div>

<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"></div>
				<div class="col-sm-12 text-right">
					<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.ba-resize.min.js"></script>
<script src="../js/jquery_cookie.min.js"></script>
<script src="../js/retina.min.js"></script>
<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
<script src="../js/tinynav.js"></script>
<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
<script src="../js/ebro_common.js"></script>
<script src="../js/lib/bootbox/bootbox.min.js"></script>
<script src="../js/pages/ebro_user_profile.js"></script>
<script type="text/javascript">
	isiCboNasabah(parseInt(<?php echo $nasabah; ?>));
	function isiCboNasabah(id_nasabah){
		var optIsi = '';
		if(id_nasabah == 0){
			optIsi = optIsi + '<option value="" selected="">--pilih--</option>';	
		} else {
			optIsi = optIsi + '<option value="">--pilih--</option>';
		}
		<?php
		$result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.User_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
		if (!$result1) { die("SQL Error Result1 "); }
		if (mysqli_num_rows($result1)) {
			while ($allRow1 = mysqli_fetch_array($result1)) {
				?>
				var id = parseInt(<?php echo $allRow1['id']; ?>);
				var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
				if(id == id_nasabah){
					optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
				}
				<?php 
			}
		} ?>

		document.getElementById('nasabah').innerHTML = '';
		document.getElementById('nasabah').innerHTML = optIsi;
		isi_data_nasabah();
	}

	var set_awal = 0;
	function isi_data_nasabah(){ 
		var id_nasabah = $('#nasabah').val();
		if(id_nasabah != 0){
			$.post("transaksi_gadai_edit_sistem.php", {
				id_nasabah : id_nasabah,
				isi_data_nasabah : ''
			}, function(result){
				if(result != ''){
					var isi1 = result.split("-");
					document.getElementById('noKtp').innerHTML = isi1[0];
					document.getElementById('nama').innerHTML = isi1[1];
					document.getElementById('alamat').innerHTML = isi1[2];
					document.getElementById('namaPekerjaan').innerHTML = isi1[3];
					document.getElementById('alamatPekerjaan').innerHTML = isi1[4];
					document.getElementById('telp').innerHTML = isi1[5];
					document.getElementById('noHp').innerHTML = isi1[6];
					isiCboBarang(0, id_nasabah);
					if(set_awal == 0){
						set_awal = 1;
					} else {
						hapus_semua_barang_gadai();
					}
				} else {
					alert('Error');
				}
			}); 
		} else {
			document.getElementById('noKtp').innerHTML = '';
			document.getElementById('nama').innerHTML = '';
			document.getElementById('alamat').innerHTML = '';
			document.getElementById('namaPekerjaan').innerHTML = '';
			document.getElementById('alamatPekerjaan').innerHTML = '';
			document.getElementById('telp').innerHTML = '';
			document.getElementById('noHp').innerHTML = '';
			isiCboBarang(0, 0);
			hapus_semua_barang_gadai();
		}
	}
</script>
<script type="text/javascript">
	function isiCboBarang(id_barang, id_nasabah){
		var optIsi = '';
		optIsi = optIsi + '<option value="'+id_barang+'" selected="">--pilih--</option>';
		<?php
		$resultK = mysqli_query($conn, "SELECT b.*, btg.status_barang, btg.id_transaksi_gadai, tg.status_transaksi FROM barang b LEFT JOIN barang_transaksi_gadai btg ON b.id = btg.id_barang LEFT JOIN transaksi_gadai tg ON btg.id_transaksi_gadai = tg.id LEFT JOIN nasabah n ON tg.nasabah_id = n.id WHERE b.shapus = 0 ORDER BY b.nama_barang ASC");
		if (!$resultK) { die("SQL Error ResultK "); }
		if (mysqli_num_rows($resultK)) {
			while ($rowK = mysqli_fetch_array($resultK)) {
				?>
				var id = parseInt(<?php echo $rowK['id']; ?>);
				var id_nasabah1 = parseInt(<?php echo $rowK['nasabah_id']; ?>);
				var status_transaksi = parseInt(<?php echo $rowK['status_transaksi']; ?>);
				var sst = status_transaksi.toString();
				var tulis = '<?php echo $rowK['nama_barang']; ?>';
				var id_tg = '<?php echo $rowK['id_transaksi_gadai']; ?>';
				if(sst != '1' || sst == '' || sst == null){
					if(id_nasabah == id_nasabah1){
						if(id == id_barang){
							optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
						} else {
							optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
						}
					}
				} else {
					if(id_nasabah == id_nasabah1){
						if(id_tg == parseInt(<?php echo $id_transaksi_gadai; ?>)){
							optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
						}
					}
				}
			<?php }
		} ?>
		document.getElementById('barang').innerHTML = '';
		document.getElementById('barang').innerHTML = optIsi;
	}

	var add_barang_gadai = <?php echo $jum_barang; ?>;
	function isi_data_barang(){
		add_barang_gadai++;
		var id_barang = $("#barang").find('option:selected').val();
		var nama_barang = $("#barang").find('option:selected').text();
		var s_ada = 0;
		for(var i = 1; i <= add_barang_gadai; i++){
			var cek_id = document.getElementById('id_barang'+i);
			if(cek_id){
				if(cek_id.value == id_barang){
					s_ada = 1;
				}
			}
		}
		if(s_ada == 0){
			if(id_barang != 0){
				var tulis_isi = '<div class="col-sm-3 editable" id="div1_1_'+add_barang_gadai+'"> <div id="div1_2_'+add_barang_gadai+'"> <input type="text" name="nama_barang[]" class="form-control" readonly="" placeholder="nama barang" id="nama_barang'+add_barang_gadai+'" value="'+nama_barang+'"> <input type="hidden" name="id_barang[]" id="id_barang'+add_barang_gadai+'" value="'+id_barang+'"> </div> </div> <div class="col-sm-3 editable" id="div2_1_'+add_barang_gadai+'"> <div id="div2_2_'+add_barang_gadai+'"> <input type="number" name="jumlah_barang[]" class="form-control" placeholder="jumlah barang" id="jumlah_barang'+add_barang_gadai+'" style="display: none;"> </div> </div> <div class="col-sm-3 editable" id="div3_1_'+add_barang_gadai+'"> <div id="div3_2_'+add_barang_gadai+'"> <select id="status_barang'+add_barang_gadai+'" name="status_barang[]" class="form-control"> <option value="0">Di Tahan</option> <option value="1">Di Sita</option> <option value="2">Di Kembalikan</option> </select> </div> </div> <div class="col-sm-1 editable" id="div4_1_'+add_barang_gadai+'"> <div id="div4_2_'+add_barang_gadai+'"> <a class="btn btn-danger btn-block" id="btn'+add_barang_gadai+'" onclick="hapus_barang_gadai('+add_barang_gadai+')">X</a> </div> </div> <div class="col-sm-3 editable" id="div5_1_'+add_barang_gadai+'"><p class="form-control-static"style="color: rgb(0, 128, 128); display: none;">hidden</p></div>';
				document.getElementById('div_barang_gadai').innerHTML += tulis_isi;
			}
		}
	}

	function hapus_semua_barang_gadai() {
		for(var i = 1; i <= add_barang_gadai; i++){
			var cek_id = document.getElementById('id_barang'+i);
			if(cek_id){
				hapus_barang_gadai(i);
			}
		}
		add_barang_gadai = 1;
	}

	function hapus_barang_gadai(ke) {
		$('#lbl'+ke).remove();
		$('#nama_barang'+ke).remove();
		$('#id_barang'+ke).remove();
		$('#status_barang'+ke).remove();
		$('#btn'+ke).remove();
		$('#div1_1_'+ke).remove();
		$('#div1_2_'+ke).remove();
		$('#div2_1_'+ke).remove();
		$('#div2_2_'+ke).remove();
		$('#div3_1_'+ke).remove();
		$('#div3_2_'+ke).remove();
		$('#div4_1_'+ke).remove();
		$('#div4_2_'+ke).remove();
		$('#div5_1_'+ke).remove();
	}
</script>

<script type="text/javascript">
	isiCboSukuBunga(parseInt(<?php echo $suku_bunga; ?>));
	function isiCboSukuBunga(id_suku_bunga){
		var optIsi = '';
		<?php
		$resultSB = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 1 AND shapus = 0 ORDER BY jumlah_bunga ASC");
		if (!$resultSB) { die("SQL Error ResultSB "); }
		if (mysqli_num_rows($resultSB)) {
			while ($rowSB = mysqli_fetch_array($resultSB)) {
				?>
				var id = <?php echo $rowSB['id']; ?>;
				var bunga = <?php echo $rowSB['jumlah_bunga']; ?>;
				if(id == id_suku_bunga){
					optIsi = optIsi + '<option value="' + id + '" selected="">' + bunga + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + id + '">' + bunga + '</option>';
				}
			<?php }} ?>
			document.getElementById('suku_bunga').innerHTML = '';
			document.getElementById('suku_bunga').innerHTML = optIsi;
		}
	</script>
	<script type="text/javascript">
		isiCboJangkaWaktu(parseInt(<?php echo $jangka_waktu; ?>));
		function isiCboJangkaWaktu(jangka_waktu){
			var optIsi = '';
			for(var i = 1; i <= 12; i++){
				if(i == jangka_waktu){
					optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
				}
			}
			document.getElementById('jangka_waktu').innerHTML = '';
			document.getElementById('jangka_waktu').innerHTML = optIsi;
		}
	</script>
	<script type="text/javascript">
		function isiBiayaTotal(){
			var nominal_pinjaman = document.getElementById('nominal_pinjaman').value;
			var cboJangkaWaktu = $('#jangka_waktu').val();
			var cboSukuBunga = $("#suku_bunga").find('option:selected').text();
			if(nominal_pinjaman != '' && parseInt(nominal_pinjaman) != 0){
				var np = parseFloat(nominal_pinjaman);
				var jw = parseFloat(cboJangkaWaktu);
				var sb = parseFloat(cboSukuBunga);
				var biayaAdmin = np * 5 / 100;
				var angsuran = np * sb / 100;
				var total = np + (angsuran * jw);
				document.getElementById('biaya_administrasi').value = biayaAdmin;
				document.getElementById('total').value = total;
				document.getElementById('nominal_angsuran').value = angsuran;
			} else {
				alert('Nominal Pinjaman Tidak Boleh kosong/nol');
			}
		}
	</script>
</div>
</body>
</html>


<?php
function getJumlahNotifikasi($conn){
	$jumlahNotifikasi = 0;

	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahNotifikasi++;
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahNotifikasi++;;
	}

	return $jumlahNotifikasi;
}


function cekNotifikasiPengajuanPinjaman($conn){

	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPengajuanGadai($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiInvestasiPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}
function cekNotifikasiPencairanDanaPending($conn){
	$notifikasi = false;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$notifikasi = true;
	}

	return $notifikasi;
}



function generateNotification($conn){
	if(cekNotifikasiPengajuanPinjaman($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanPinjamanPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_peminjaman.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pinjaman yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiPengajuanGadai($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanGadaiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_gadai.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan gadai yang belum di proses</button></a>
		</li>
		<?php
	}
	if(cekNotifikasiInvestasiPending($conn)){
		$jumlahPengajuanPending = getJumlahPengajuanInvestasiPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan investasi yang belum di proses</button></a>
		</li>
		<?php
	}

	if(cekNotifikasiPencairanDanaPending($conn)){
		$jumlahPengajuanPending = getJumlahPencairanDanaPending($conn);
		?>
		<li>
			<a href="../karyawan/transaksi_investasi_pencairan_pending.php"><button type="submit" style="border: none;background-color: inherit;">Terdapat <?php echo $jumlahPengajuanPending; ?> pengajuan pencairan dana yang belum di proses</button></a>
		</li>
		<?php
	}
}

function getJumlahPengajuanPinjamanPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_peminjaman` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPengajuanGadaiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_gadai` WHERE status_transaksi = 3";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}
function getJumlahPengajuanInvestasiPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_investasi` where shapus = 0 and status_transaksi=2";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function getJumlahPencairanDanaPending($conn){
	$jumlah = 0;

	$sql1 = "SELECT * FROM `transaksi_pencairan_dana` WHERE status = 0 and shapus = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah++;
	}

	return $jumlah;
}

function cekStatusCair($statusCair){
	if($statusCair==='0'){
		return 'belum cair';
	}else{
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair){
	if(cekStatusCair($statusCair)==='belum cair'){
		return '-';
	}else{
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi){
	if($statusTransaksi==="0"){
		return "Belum Selesai";
	}else if($statusTransaksi==="1"){
		return "sudah selesai";
	}else if($statusTransaksi==="2"){
		return "pending";
	}
}

function getNamaNasabah($idNasabah, $conn){
	$sql1 = "SELECT nama FROM `nasabah` where id = $idNasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function getNamaPegawai($idPegawai, $conn){
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal){
	if($autoRenewal===NULL){
		return "transaksi baru";
	}else{
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($conn, $statusTransaksi, $id, $pegawaiId, $nasabahId){

	$button = NULL;
	$setBunga = NULL;
	$btnPenarikan = null;
	$buttonDetail = <<<TOMBOL
	<form action="transaksi_investasi_detail.php" method="POST" >
	<input type="hidden" name="id_investasi" value="$id">
	<button type="submit" class="btn btn-success btn-block">Detail</button>
	</form>
TOMBOL;
	if(cekStatusTransaksi($statusTransaksi)==='Belum Selesai'){
		$btnPenarikan = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Cairkan Dana</button>
		</form>
TOMBOL;
		
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_selesai_investasi" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-danger btn-block">Selesai</button>
		</form>
TOMBOL;
		
	}else if(cekStatusTransaksi($statusTransaksi)==='pending'){
		$button = <<<TOMBOL
		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-success btn-block">Setujui</button>
		</form>

		<form action="transaksi_investasi_edit_sistem.php" method="POST" >
		<input type="hidden" name="update_tolak_investasi" value="true">

		<input type="hidden" name="id" value="$id">
		<input type="hidden" name="id_pegawai" value="$pegawaiId">
		<input type="hidden" name="id_nasabah" value="$nasabahId">

		<button type="submit" class="btn btn-danger btn-block">Tolak</button>
		</form>
TOMBOL;
	}

	if(cekBunga($conn, $id)==false){
		$setBunga = <<<TOMBOL
		<form action="transaksi_investasi_insert_bunga.php" method="POST" >
		<input type="hidden" name="update_setujui_investasi" value="true">
		<input type="hidden" name="id_investasi" value="$id">
		<button type="submit" class="btn btn-danger btn-block">Set Bunga</button>
		</form>
TOMBOL;
	}

	return $buttonDetail.$btnPenarikan.$button.$setBunga;
}

function generateBunga($conn, $idTransaksiInvestasi){
	if(cekBunga($conn, $idTransaksiInvestasi)!=false){
		return cekBunga($conn, $idTransaksiInvestasi);
	}else{
		return '-';
	}
}

function cekBunga($conn, $idTransaksiInvestasi){
	$sql1 = "SELECT jumlah_bunga FROM `perubahan_suku_bunga` INNER JOIN suku_bunga on perubahan_suku_bunga.id_suku_bunga=suku_bunga.id WHERE id_transaksi_investasi = $idTransaksiInvestasi ";
	$jumlah_bunga = null;
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { return("-"); }
	while ($row1 = mysqli_fetch_array($result1)) {
		$jumlah_bunga = $row1['jumlah_bunga'];
	}

	if($jumlah_bunga==null){
		return false;
	}else{
		return $jumlah_bunga;
	}
}

?>
<?php ob_end_flush(); ?>