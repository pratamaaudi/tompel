<div class="modal fade" id="modal_bunga">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Pendapatan Bunga Investasi</h4>
			</div>
			<form method="POST">
				<div class="modal-body">
					<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
						<thead>
							<tr role="row">
								<th><center>Tanggal</center></th>
								<th><center>Nominal Bunga</center></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$nasabahId = $_SESSION['login_nasabah'];
							$totalSaldo = 0;
							$sql1 = "SELECT date(tpbi.tanggal_cair) as 'tanggal', tpbi.jumlah_pendapatan_bunga as 'jumlah' FROM nasabah n inner join transaksi_investasi ti on n.id = ti.nasabah_id inner join transaksi_pendapatan_bunga_investasi tpbi on ti.id = tpbi.transaksi_investasi_id where n.id = $nasabahId and tpbi.status_cair = 1";
							$result1 = mysqli_query($conn, $sql1);
							while ($row = mysqli_fetch_array($result1)) {
								$totalSaldo += $row['jumlah'];
								?>

								<tr>
									<td><center><?php echo $row['tanggal']; ?></center></td>
									<td><center>Rp. <?php echo $row['jumlah']; ?></center></td>
								</tr>

								<?php
							}
							?>
							<tr>
								<td><center><strong>Total : </strong></center></td>
								<td><center>Rp. <?php echo $totalSaldo; ?></center></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				</div>
			</form>
		</div>
	</div>
</div>