<?php

ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php';

if (isset($_POST['ajukan_investasi'])) {
    $tanggalSekarang = $_POST['tgl_sekarang'];
    $jumlah_uang = $_POST['jumlah_uang'];
    $jangka_waktu = $_POST['jangka_waktu'];
    $nasabah_id = $_SESSION['login_nasabah'];

    $tanggalSekarangDate = DateTime::createFromFormat('Y-m-d', $tanggalSekarang);
    $tanggalJatuhTempo = getTanggalJatuhTempo($tanggalSekarangDate, $jangka_waktu);
    $tanggalJatuhTempoString = $tanggalJatuhTempo->format('Y-m-d');

    $investasiId = insertInvestasi($conn, $tanggalSekarang, $jumlah_uang, $jangka_waktu, $tanggalJatuhTempoString, $biaya_administrasi, $nasabah_id);

    $bungaId = $_POST['bungaId'];

    insertPerubahanSukuBunga($conn, $bungaId, $investasiId, $tanggalSekarang);

    $_SESSION['alertInvestasi'] = "investasi telah dijukan, mohon menunggu persetujuan admin";
}

function insertInvestasi($conn, $tanggalSekarang, $jumlah_uang, $jangka_waktu, $tanggalJatuhTempoString, $biaya_administrasi, $nasabah_id){
    $sql = "INSERT INTO `transaksi_investasi` (
    `id`, 
    `tanggal`, 
    `jumlah_uang`, 
    `jangka_waktu`, 
    `tanggal_jatuh_tempo`,
    `status_transaksi`, 
    `nasabah_id`, 
    `pegawai_id`, 
    `transaksi_investasi_id`) 
    VALUES (
    NULL, 
    '$tanggalSekarang', 
    '$jumlah_uang', 
    '$jangka_waktu', 
    '$tanggalJatuhTempoString',
    '2', 
    '$nasabah_id', 
    NULL, 
    NULL);";
    $result1 = mysqli_query($conn, $sql);
    if (!$result1) {
        die("SQL ERROR : Result1");
    }

    $result4 = mysqli_query($conn, "SELECT id FROM `transaksi_investasi` ORDER BY id DESC LIMIT 1");
    if (!$result4) { die("SQL ERROR : result4"); }  
    $row4 = mysqli_fetch_array($result4);
    return $row4['id'];
}

function insertPerubahanSukuBunga($conn, $sukuBungaId, $transaksiInvestasiId, $tanggal){
    $sql = "INSERT INTO `perubahan_suku_bunga` (
    `id`, 
    `id_suku_bunga`, 
    `id_transaksi_peminjaman`, 
    `id_transaksi_gadai`, 
    `id_transaksi_investasi`, 
    `tgl_perubahan_suku_bunga`) 
    VALUES (
    NULL, 
    '$sukuBungaId', 
    NULL, 
    NULL, 
    '$transaksiInvestasiId', 
    '$tanggal');";
    $result1 = mysqli_query($conn, $sql);
    if (!$result1) {
        die("SQL ERROR : Result1");
    }
}

function getTanggalJatuhTempo($tanggaltransaksi, $jangka_waktu){
    return $tanggaltransaksi->modify('+'.$jangka_waktu.' months');
}

if (isset($_POST['ajukan_pencairan_investasi'])) {
    $jumlah = $_POST['jumlah'];
    $nasabahId = $_POST['nasbahId'];
    $transaksiId = $_POST['transaksiId'];
    $tanggal = $_POST['tgl_sekarang'];
    $sql = "INSERT INTO `transaksi_pencairan_dana` (
    `id`, 
    `status`, 
    `jumlah`, 
    `tanggal`, 
    `nasabah_id`, 
    `pegawai_id`, 
    `transaksi_investasi_id`) 
    VALUES (NULL, '0', '$jumlah', '$tanggal', '$nasabahId', NULL, '$transaksiId');";
    $result1 = mysqli_query($conn, $sql);
    if (!$result1) {
        die("SQL ERROR : Result1");
    }
}

header('Location: transaksi_investasi.php');

?>