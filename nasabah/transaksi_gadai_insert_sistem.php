<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 

if (isset($_POST['insert_transaksi_gadai'])) {
	$id_nasabah = $_SESSION['login_nasabah'];
	$tanggal_gadai = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	//INSERT INTO `transaksi_gadai`(`id`, `tanggal`, `status_cair`, `tgl_cair`, `status_transaksi`, `jumlah_pinjaman`, `jangka_waktu`, `jumlah_angsuran`, `biaya_administrasi`, `total`, `nasabah_id`, `pegawai_id`, `shapus`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13])
	$sql1 = "INSERT INTO `transaksi_gadai`(`tanggal`, `status_transaksi`, `jumlah_pinjaman`, `jangka_waktu`, `nasabah_id`, `shapus`) VALUES ('$tanggal_gadai', 3, $jumlah_pinjaman, $jangka_waktu, $id_nasabah, 0)";
	$result1 = mysqli_query($conn, $sql1);
	if(!$result1){ die("SQL ERROR : Result " . $sql1); }

	$result2 = mysqli_query($conn, "SELECT * FROM transaksi_gadai WHERE tanggal = '$tanggal_gadai'");
	if(!$result2){ die("SQL ERROR : Result2"); }
	$row2 = mysqli_fetch_array($result2);
	$id_transaksi_gadai = $row2['id'];

	$result3 = mysqli_query($conn, "INSERT INTO `perubahan_suku_bunga`(`id_suku_bunga`, `id_transaksi_gadai`, `tgl_perubahan_suku_bunga`) VALUES (0, $id_transaksi_gadai, '$tanggal_gadai')");
	if(!$result3){ die("SQL ERROR : Result3"); }

	$jum_barang = count($_POST['id_barang']);
	for($i = 0; $i < $jum_barang; $i++){
		$id_barang = $_POST['id_barang'][$i];
		$jumlah_barang = $_POST['jumlah_barang'][$i];
		$status_barang = $_POST['status_barang'][$i];
		$result5 = mysqli_query($conn, "INSERT INTO `barang_transaksi_gadai`(`id_barang`, `id_transaksi_gadai`, `jumlah_barang`, `status_barang`) VALUES ($id_barang, $id_transaksi_gadai, 0, 3)");
		if(!$result5){ die("SQL ERROR : result5"); }	
	}
	header('Location: transaksi_gadai.php'); 
}
ob_end_flush(); ?>