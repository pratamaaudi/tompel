<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php';
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<?php
	//unset($_SESSION['pesan_jaminan_kendaraan_detail']);
	$pesan_jaminan_kendaraan_detail = ''; 
	if (isset($_SESSION['pesan_jaminan_kendaraan_detail'])) {
		$pesan_jaminan_kendaraan_detail = $_SESSION['pesan_jaminan_kendaraan_detail'];
		unset($_SESSION['pesan_jaminan_kendaraan_detail']);
		echo '<script>alert("'.$pesan_jaminan_kendaraan_detail.'")</script>';
	}

	if(isset($_POST['foto_delete'])){
		$idKendaraan = $_POST['idKendaraan'];
		$foto_delete = $_POST['foto_delete'];
		unlink($foto_delete);
		header("Location: jaminan_kendaraan_detail.php?idKendaraan=" . $idKendaraan);
	}

	if(isset($_POST['foto_asli']) && isset($_POST['tempat_foto'])){
		$idKendaraan = $_POST['idKendaraan'];
		$foto_asli = $_POST['foto_asli'];
		$tempat_foto = $_POST['tempat_foto'];
		echo $idKendaraan . "-" . $tempat_foto . "-" . $foto_asli . "-" . $_FILES['foto_ganti']['name'];
		unlink($tempat_foto . $foto_asli);
		$fg = explode(".", $_FILES['foto_ganti']['name']);
		// RANDOM UNTUK UPDATE FOTO DI INDEX BERIKUTNYA JIKA DI UPDATE
		$foto_ganti = substr(md5($_FILES['foto_ganti']['name'] . date("h:i:s")), 0, 10) . "." . $fg[count($fg) - 1];
		move_uploaded_file($_FILES['foto_ganti']['tmp_name'], $tempat_foto . $foto_ganti);
		header("Location: jaminan_kendaraan_detail.php?idKendaraan=" . $idKendaraan);
	}
	$idKendaraan = $_GET['idKendaraan'];
	$sql1 = "SELECT * FROM  jenis_jaminan WHERE id = $idKendaraan AND nasabah_id = $id_nasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL ERROR : result1"); }
	if (!mysqli_num_rows($result1)) { 
		$_SESSION['massage_jaminan'] = 'Kendaraan Ini Bukan Milik Anda';
		header("Location: jaminan.php"); 
	}
	$row1 = mysqli_fetch_array($result1);
	$idJaminanKendaraan = $row1['id'];
	$noPolisi = $row1['no_polisi'];
	$namaPemilikSTNK = $row1['nama_pemilik_stnk'];
	$merk = $row1['merk'];
	$type = $row1['type'];
	$jenis = $row1['jenis'];
	$stringJenis = '-';
	if($jenis == 1){ 
		$stringJenis = 'Roda Dua'; 
	} else if($jenis == 2){ 
		$stringJenis = 'Roda Tiga'; 
	} else if($jenis == 3){ 
		$stringJenis = 'Roda Empat'; 
	}
	$model = $row1['model'];
	$thnPembuatan = $row1['tahun_pembuatan'];
	$isiSilinder = $row1['isi_silinder'];
	$noRangka = $row1['no_rangka'];
	$noMesin = $row1['no_mesin'];
	$warna = $row1['warna'];
	$thnRegistrasi = $row1['tahun_registrasi'];
	$thnKendaraan = $row1['tahun_kendaraan'];
	$status = $row1['status'];
	$stringStatus = 'AN';
	if($status == 1){ $stringStatus = 'OL'; }
	$fotoDocumen = $row1['foto_dokumen'];
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header" >   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						

						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="jaminan_kendaraan_detail.php?idKendaraan=<?php echo $idKendaraan ?>" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>




						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<!--<span class="label label-danger">12</span>-->
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<!-- <img src="../gallery/smile.png" alt=""> -->
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					<!-- <div class="col-xs-12 col-sm-pull-6 col-sm-4">
						<form class="main_search">
							<input type="text" id="search_query" name="search_query" class="typeahead form-control">
							<button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
						</form> 
					</div> -->
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li class="active">             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<ul>
					<li><a href="jaminan.php">Jaminan</a></li>
					<li><span>Detail Jaminan Kendaraan</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix"  style="background-color: rgb(0, 128, 128); border-radius: 20px">
				<div id="main_content">
					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<?php $row1 = mysqli_fetch_array($result1); ?>



							<!-- START MODAL HAPUS / UNTUK MENAMPILKAN POP UP PEMBERITAUNAN-->
							<div class="modal fade" id="modalHapus">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Hapus  Data Jaminan</h4>
										</div>
										<div class="modal-body">
											Apakah Anda Yakin ingin menghapus <b><?php echo $row1['nama']; ?> ?</b>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
											<a href="jaminan_kendaraan_sistem.php?dIdJaminanKendaraan=<?php echo $idJaminanKendaraan; ?>" class="btn btn-danger">Ya Yakin!</a>
										</div>
									</div>
								</div>
							</div>
							<!-- END MODAL HAPUS -->





							<!--
							<div class="user_heading">
								<div class="row">
									<div class="col-sm-2 hidden-xs"></div>
									<div class="col-sm-10">
										<div class="user_heading_info">
											<div class="user_actions pull-right">
												<a href="#" class="edit_form" data-toggle="tooltip" data-placement="top auto" title="Edit Jaminan"><span class="icon-edit"></span></a>
												<a href="jaminan_kendaraan_sistem.php?dIdJaminanKendaraan=<?php echo $idJaminanKendaraan; ?>" title="Remove Jaminan"><span class="icon-remove"></span></a>
											</div>
											<h2><STRONG>JAMINAN KENDARAAN</STRONG></h2>
										</div>
									</div>
								</div>
							</div>
						-->


						<div class="user_heading">
							<div class="row">
								<div class="col-sm-1 hidden-xs">
								</div>
								<div class="col-sm-10">
									<div class="user_heading_info">
										<div class="user_actions pull-right">
											<a href="#" class="edit_form" data-toggle="tooltip" data-placement="top auto" title="Edit profile"><span class="icon-edit"></span></a>
											<a  data-toggle="modal" href="#modalHapus"  title="Remove User"><span class="icon-remove"></span></a>
										</div>
										<h1><?php echo $row1['nama']; ?></h1>
										<h3 style="font-size: 250%; color: white";><strong>DATA JAMINAN</strong></h3>
									</div>
								</div>
							</div>
						</div>






						<div class="user_content">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-3">
									<form action="jaminan_kendaraan_sistem.php" class="form-horizontal user_form" method="POST" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">No Polisi :</label>
											<div class="col-sm-2 editable">
												<p class="form-control-static" style="color: white"><?php echo $noPolisi; ?></p>
												<div class="hidden_control">
													<input type="text" class="form-control" name="noPolisi" value="<?php echo $noPolisi; ?>" required="">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Nama Pemilik STNK :</label>
											<div class="col-sm-5 editable">
												<p class="form-control-static"style="color: white"><?php echo $namaPemilikSTNK; ?></p>
												<div class="hidden_control">
													<input type="text" class="form-control" name="namaPemilikSTNK" value="<?php echo $namaPemilikSTNK; ?>" required="">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Merk :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white"><?php echo $merk; ?></p>
												<div class="hidden_control">
													<input type="text" class="form-control" name="merk" value="<?php echo $merk; ?>" required="">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Type :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white"><?php echo $type; ?></p>
												<div class="hidden_control">
													<input type="text" class="form-control" name="type" value="<?php echo $type; ?>" required="">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Jenis :</label>
											<div class="col-sm-2 editable">
												<p class="form-control-static"style="color: white"><?php echo $stringJenis; ?></p>
												<div class="hidden_control">
													<select name="jenis" class="form-control">
														<option value="0"<?php if($jenis == 0){echo ' selected=""';} ?>>
															--pilih--
														</option>
														<option value="1"<?php if($jenis == 1){echo ' selected=""';} ?>>
															Roda Dua
														</option>
														<option value="2"<?php if($jenis == 2){echo ' selected=""';} ?>>
															Roda Tiga
														</option>
														<option value="3"<?php if($jenis == 3){echo ' selected=""';} ?>>
															Roda Empat
														</option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Model :</label>
											<div class="col-sm-3 editable">
												<p class="form-control-static"style="color: white"><?php echo $model; ?></p>
												<div class="hidden_control">
													<input type="text" class="form-control" name="model" value="<?php echo $model; ?>" required="">
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" style="color: white">Tahun Pembuatan :</label>
											<div class="col-sm-2 editable">
												<p class="form-control-static"style="color: white"><?php echo $thnPembuatan; ?></p>
												<div class="hidden_control">
													<select id="thnPembuatan" name="thnPembuatan" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnPembuatan; ?>'), 1);
															</script-->
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Isi Silinder :</label>
												<div class="col-sm-2 editable">
													<p class="form-control-static"style="color: white"><?php echo $isiSilinder; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="isiSilinder" value="<?php echo $isiSilinder; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No Rangka :</label>
												<div class="col-sm-5 editable">
													<p class="form-control-static"style="color: white"><?php echo $noRangka; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="noRangka" value="<?php echo $noRangka; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No Mesin :</label>
												<div class="col-sm-5 editable">
													<p class="form-control-static"style="color: white"><?php echo $noMesin; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="noMesin" value="<?php echo $noMesin; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Warna :</label>
												<div class="col-sm-2 editable">
													<p class="form-control-static"style="color: white"><?php echo $warna; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="warna" value="<?php echo $warna; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Tahun Registrasi :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white"><?php echo $thnRegistrasi; ?></p>
													<div class="hidden_control">
														<select id="thnRegistrasi" name="thnRegistrasi" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnRegistrasi; ?>'), 2);
															</script-->
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Tahun Kendaraan :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white"><?php echo $thnKendaraan; ?></p>
													<div class="hidden_control">
														<select id="thnKendaraan" name="thnKendaraan" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnKendaraan; ?>'), 3);
															</script-->>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status :</label>
												<div class="col-sm-2 editable">
													<p class="form-control-static"style="color: white"><?php echo $stringStatus; ?></p>
													<div class="hidden_control">
														<select name="status" class="form-control">
															<?php if($status == 0){ ?>
																<option value="0" selected="">AN</option>
																<option value="1">OL</option>
															<?php } else { ?>
																<option value="0">AN</option>
																<option value="1" selected="">OL</option>
															<?php } ?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Foto Dokumen :</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static">
														<?php
														$tempat_foto = '../gallery/jaminan_kendaraan/' . $fotoDocumen . '/';
														if (file_exists($tempat_foto)) {
															$baca_folder = scandir($tempat_foto);
															$panjang_isi_folder = count($baca_folder);
															// looping untuk mengupload foto lebih dari 1, karena foto
															// selalu di mulai dari indek ke 2
															for($i = 2; $i < $panjang_isi_folder; $i++){ ?>
																<img src="<?php echo $tempat_foto . $baca_folder[$i]; ?>" alt="" style="width: 100px; height: 100px; margin-top: 5px; padding: 3px 3px 3px 3px;" data-toggle="modal" href="#modal_foto_dokumen<?php echo $i; ?>">
															<?php }
														}
														?>
													</p>
													<div class="hidden_control" style="color: white">
														<input type="hidden" name="folder" value="<?php echo $fotoDocumen; ?>">
														<small style="color: white; "><u> #UBAH FOTO, KLIK TOMBOL DIBAWAH -FOTO SAAT INI = (<?php echo ($panjang_isi_folder-2); ?>) </u>
														</small>
														<input type="file" name="fotoDocumen[]" multiple="">
													</div>
												</div>
											</div>
											<div class="form_submit clearfix" style="display:none">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<input type="hidden" class="form-control" name="update_jaminan_kendaraan" value="<?php echo $idJaminanKendaraan; ?>">
														<button type="submit" class="btn btn-primary btn-lg"><i class="icon-save"></i> SIMPAN</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>
	<?php
	$tempat_foto = '../gallery/jaminan_kendaraan/' . $fotoDocumen . '/';
	if (file_exists($tempat_foto)) {
		$baca_folder = scandir($tempat_foto);
		$panjang_isi_folder = count($baca_folder);
		// looping untuk mengupload foto lebih dari 1 
		for($i = 2; $i < $panjang_isi_folder; $i++){ ?>



			<!-- START MODAL HAPUS-->
			<div class="modal fade" id="modal_foto_dokumen<?php echo $i; ?>">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Zoom Foto</h4>
						</div>
						<div class="modal-body"><center>
							<img src="<?php echo $tempat_foto . $baca_folder[$i]; ?>" alt="" style="width: 300px; height: 300px; margin-top: 5px; padding: 3px 3px 3px 3px;"></center>
						</div>
						<div class="modal-footer">
							<div class="row">
								<form method="POST" enctype="multipart/form-data">
									<div class="col-sm-2">
										<input type="hidden" name="foto_delete" value="<?php echo $tempat_foto . $baca_folder[$i]; ?>">
										<input type="hidden" name="idKendaraan" value="<?php echo $idKendaraan; ?>">
										<button type="submit" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-trash"></span></button>
									</div>
								</form>
								<form method="POST" enctype="multipart/form-data">
									<div class="col-sm-4">
										<p>Ganti Foto : </p>
									</div>
									<div class="col-sm-4">
										<input type="file" name="foto_ganti" required="">
										<input type="hidden" name="foto_asli" value="<?php echo $baca_folder[$i]; ?>">
										<input type="hidden" name="tempat_foto" value="<?php echo $tempat_foto; ?>">
										<input type="hidden" name="idKendaraan" value="<?php echo $idKendaraan; ?>">
									</div>
									<div class="col-sm-2">
										<button type="submit" class="btn btn-primary btn-block">Ubah</button>
									</div>
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- END MODAL HAPUS -->
		<?php }
	}
	?>
	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		isiThn(<?php echo $thnPembuatan; ?>, 1);
		isiThn(<?php echo $thnRegistrasi; ?>, 2);
		isiThn(<?php echo $thnKendaraan; ?>, 3);

		function isiThn(thnPilih, id){
			var batas = parseInt("<?php echo date('Y'); ?>");
			var optIsi = '';
			for(var i = 1945; i <= batas; i++){
				if(i == thnPilih){
					optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
				}
			}
			if(id == 1){
				document.getElementById('thnPembuatan').innerHTML = '';
				document.getElementById('thnPembuatan').innerHTML = optIsi;
			} else if(id == 2){
				document.getElementById('thnRegistrasi').innerHTML = '';
				document.getElementById('thnRegistrasi').innerHTML = optIsi;
			} else {
				document.getElementById('thnKendaraan').innerHTML = '';
				document.getElementById('thnKendaraan').innerHTML = optIsi;
			}
		}
	</script>
	
</body>
</html>





<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}

?>
<?php ob_end_flush(); ?>