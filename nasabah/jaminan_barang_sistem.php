<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 
if (isset($_POST['update_jaminan_barang'])) {
	$idJaminanBarang = $_POST['update_jaminan_barang'];
	$nama_barang = $_POST['nama_barang'];
	$jenis_barang = $_POST['jenis_barang'];

	$folder_foto = '../gallery/jaminan_barang/' . $_POST['folder'] . '/';
	$baca_folder = scandir($folder_foto);
	$panjang_isi_folder = count($baca_folder) - 2;
	
	$total = count($_FILES['foto_barang']['name']);


	// LOOPING UNTUK UPLOAD FOTO SESUAI PANJANG FOLDER
	$jumlah_foto = 0 + $panjang_isi_folder;
	for($i = 0; $i < $total; $i++) {
		$fb = explode(".", $_FILES['foto_barang']['name'][$i]);
		$foto_barang = substr(md5($_FILES['foto_barang']['name'][$i] . date("h:i:s")), 0, 10) . "." . $fb[count($fb) - 1];
		if(strtoupper($fb[count($fb) - 1]) == 'JPG' || strtoupper($fb[count($fb) - 1]) == 'PNG'){
			if($jumlah_foto < 6){
				$jumlah_foto++;
				move_uploaded_file($_FILES['foto_barang']['tmp_name'][$i], $folder_foto . $foto_barang);
			}
		}
	}
	$result1 = mysqli_query($conn, "UPDATE `barang` SET `nama_barang`='$nama_barang',`jenis_barang`='$jenis_barang',`shapus`=0 WHERE `id`=$idJaminanBarang");
	if ($result1) {
		$_SESSION['jaminan'] = 'barang'; 
		header('Location: jaminan.php'); 
	} else { 
		die("SQL ERROR : Result1"); 
	}
}
if (isset($_GET['dIdJaminanBarang'])) {
	$idJaminanBarang = $_GET['dIdJaminanBarang'];
	$result1 = mysqli_query($conn, "UPDATE `barang` SET `shapus`=1 WHERE `id`=$idJaminanBarang");
	if ($result1) {
		$_SESSION['jaminan'] = 'barang'; 
		header('Location: jaminan.php'); 
	} else { 
		die("SQL ERROR : Result1"); 
	}
}
ob_end_flush(); ?>