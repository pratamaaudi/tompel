<?php
ob_start();
session_start();
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php'; 
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<!-- BOOTSTRAP FOR SEARCHING -->
	<link  rel="stylesheet" href="../js/lib/dataTables/media/DT_bootstrap.css">
	<link rel="stylesheet" href="../js/lib/dataTables/extras/TableTools/media/css/TableTools.css">
</head>
<body class="sidebar_hidden">
	
	<?php 
	$pesan_transaksi_peminjaman = ''; 
	if (isset($_SESSION['pesan_transaksi_peminjaman'])) {
		$pesan_transaksi_peminjaman = $_SESSION['pesan_transaksi_peminjaman'];
		unset($_SESSION['pesan_transaksi_peminjaman']);
		echo '<script>alert("'.$pesan_transaksi_peminjaman.'")</script>';
	}
	?>
	<div id="wrapper_all">
		<header id="top_header"  style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header" >   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						


						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="transaksi_peminjaman.php" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>

						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<!--<span class="label label-danger">12</span>-->
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<!-- <img src="../gallery/smile.png" alt=""> -->
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>

								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">               
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<ul>
					<li><a href="pendataan.php">Pendataan</a></li>
					<li><span>Transaksi Peminjaman</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix">
				<div id="main_content">

					<!-- main content -->
					<div class="col-sm-12">
						<a href="transaksi_peminjaman_insert.php" class="btn btn-default"><i class="icon-plus"></i> Ajukan Transaksi Peminjaman</a><br><br>
						<div class="panel panel-default">
							<div class="panel-heading" style="background-color: rgb(0, 128, 128)">
								<h4 class="panel-title" style="color: white">DATA PEMINJAMAN</h4>
							</div>
							<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table id="dt_basic" class="table table-striped">
									<thead>
										<tr>
											<!-- <th>No</th> -->
											<th>No Kontrak</th>
											<th>Nasabah</th>
											<th>Kendaraan</th>
											<!-- <th>Tanggal Pinjam</th> -->
											<th>Nominal</th>
											<!-- <th>Jaminan</th> -->
											<th>Status</th>
											<th>Detail</th>
											<th>Deal</th>
											<th>Foto</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$no = 0;
										$sql1 = "SELECT day(tp.tanggal_pinjam) as hari, month(tp.tanggal_pinjam) as bulan, year(tp.tanggal_pinjam) as tahun, n.nama, jj.id AS id_jj, jj.merk, jj.type, jj.no_polisi, tp.tanggal_pinjam, tp.jumlah_pinjaman, tp.status_transaksi, tp.jaminan, tp.status_jaminan, tp.no_kontrak, n.* FROM transaksi_peminjaman tp JOIN nasabah n ON tp.nasabah_id = n.id JOIN jenis_jaminan jj ON tp.jenis_jaminan_id = jj.id WHERE tp.shapus = 0 AND n.id = $id_nasabah ORDER BY tp.tanggal_pinjam DESC";
										$result1 = mysqli_query($conn, $sql1);
										if (!$result1) { die("SQL Error Result1 "); }
										while ($row1 = mysqli_fetch_array($result1)) {
											$no++;
											$nasabah = $row1['nama'];
											$kendaraan = $row1['merk']." ".$row1['type']."(".$row1['no_polisi'].")";
											$tgl = substr($row1['tanggal_pinjam'], 8, 2)."/".substr($row1['tanggal_pinjam'], 5, 2)."/".substr($row1['tanggal_pinjam'], 0, 4);
											$nominal = $row1['jumlah_pinjaman'];
											$obj_jaminan = "BPKB";
											if($row1['jaminan'] == 1){
												$obj_jaminan = "KENDARAAN";
											} else if($row1['jaminan'] == 2){
												$obj_jaminan = "BPKB + KENDARAAN";
											}
											$sts_jaminan = "Di Tahan";
											if($row1['status_jaminan'] == 1){
												$sts_jaminan = "Di Sita";
											} else if($row1['status_jaminan'] == 2){
												$sts_jaminan = "Di Kembalikan";
											}
											$jaminan = $obj_jaminan."(".$sts_jaminan.")";
											$status_transaksi = "BELUM LUNAS";
											if($row1['status_transaksi'] == 0){ $status_transaksi = 'LUNAS'; }




											$tgl_waktu_sekarang = $row1['tanggal_pinjam'];
											$tws = explode(" ", $tgl_waktu_sekarang);
											$tgl = substr($tws[0], 8, 2)."-".substr($tws[0], 5, 2)."-".substr($tws[0], 0, 4);
											?>
											<tr >
												<!-- <td><?php echo $no; ?></td> -->
												<td><?php echo $row1['no_kontrak']. "/" . $row1['hari'].'-'.$row1['bulan'].'-'.$row1['tahun']; ?></td>
												<td><?php echo $nasabah; ?></td>
												<td><?php echo $kendaraan; ?></td>
												<!-- <td><?php echo $tgl; ?></td> -->
												<!-- <td>Rp. <?php echo $nominal; ?></td> -->
												<td><?php echo number_format($nominal,0,',','.'); ?></td>
												<!-- <td><?php echo $jaminan; ?></td> -->
												<td><?php echo $status_transaksi; ?></td>
												<td>
													<a class="btn btn-success" href=" transaksi_peminjaman_edit.php?id_transaksi=<?php echo $row1['no_kontrak']; ?>"><i class="icon-eye-open"></i> Detail</a>
												</td>
												<td>
													<?php 
													if($row1['status_transaksi'] == 3){ 
														echo '<label style="color: red">PENDING</label>'; 
													} else if($row1['status_transaksi'] == 2){
														echo '<label style="color: red">PENDING</label>';
													} else{
														echo '<label style="color: green">DISETUJUI</label>';
													}?>
												</td>
												<td>
													<a class="btn btn-danger" href="jaminan_kendaraan_detail.php?idKendaraan=<?php echo $row1['id_jj']; ?>"><i class="icon-picture"></i> Gambar</a>
												</td>
											</tr>
											<?php 
										} ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="footer_space"></div>
</div>

<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"></div>
				<div class="col-sm-12 text-right">
					<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.ba-resize.min.js"></script>
<script src="../js/jquery_cookie.min.js"></script>
<script src="../js/retina.min.js"></script>
<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
<script src="../js/tinynav.js"></script>
<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
<script src="../js/ebro_common.js"></script>
<script src="../js/lib/peity/jquery.peity.min.js"></script>
<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../js/lib/flot/jquery.flot.min.js"></script>
<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
<script src="../js/lib/flot/jquery.flot.resize.js"></script>
<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
<script src="../js/pages/ebro_dashboard.js"></script>




<!--[[ page specific plugins ]]-->
<!-- datatables -->
<script src="../js/lib/dataTables/media/js/jquery.dataTables.min.js"></script>
<!-- datatables column reorder -->
<script src="../js/lib/dataTables/extras/ColReorder/media/js/ColReorder.min.js"></script>
<!-- datatable fixed columns -->
<script src="../js/lib/dataTables/extras/FixedColumns/media/js/FixedColumns.min.js"></script>
<!-- datatables column toggle visibility -->
<script src="../js/lib/dataTables/extras/ColVis/media/js/ColVis.min.js"></script>
<!-- datatable table tools -->
<script src="../js/lib/dataTables/extras/TableTools/media/js/TableTools.min.js"></script>
<script src="../js/lib/dataTables/extras/TableTools/media/js/ZeroClipboard.js"></script>
<!-- datatable bootstrap style -->
<script src="../js/lib/dataTables/media/DT_bootstrap.js"></script>

<script src="../js/pages/ebro_datatables.js"></script>
</body>
</html>





<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}
?>


<?php ob_end_flush(); ?>