<?php
ob_start();
session_start();
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php'; 
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<?php
	$noPolisi = '';
	$namaPemilikSTNK = '';
	$merk = '';
	$type = '';
	$jenis = 0;
	$model = '';
	$thnPembuatan = date('Y');
	$isiSilinder = '';
	$noRangka = '';
	$noMesin = '';
	$warna = '';
	$thnRegistrasi = date('Y');
	$thnKendaraan = date('Y');
	$status = 0;
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="jaminan_kendaraan_insert.php" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>




						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<!-- <img src="../gallery/smile.png" alt=""> -->
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					<!-- <div class="col-xs-12 col-sm-pull-6 col-sm-4">
						<form class="main_search">
							<input type="text" id="search_query" name="search_query" class="typeahead form-control">
							<button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
						</form> 
					</div> -->
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li class="active">            
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<ul>
					<li><a href="jaminan.php">Jaminan</a></li>
					<li><span>Tambah Kendaraan Jaminan</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px">
				<div id="main_content">
					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="col-sm-12">
								<center>
									<div class="user_actions pull-right"></div>
									<h2 style="color: white">TAMBAH KENDARAAN JAMINAN</h2>	
								</center>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-1">



										<?php 
										$ikpi=0;
										if(isset($_GET['ikpi'])){
											$ikpi=1;
										}
										?>
										<form class="form-horizontal user_form" method="POST" action="../gallery/jaminan_kendaraan/buat_folder.php" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">No Polisi :</label>
												<div class="col-sm-2 editable">
													<div>
														<input type="text" class="form-control" name="noPolisi" value="<?php echo $noPolisi; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Nama Pemilik STNK :</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="namaPemilikSTNK" value="<?php echo $namaPemilikSTNK; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Merk :</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="merk" value="<?php echo $merk; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Type :</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="type" value="<?php echo $type; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Jenis :</label>
												<div class="col-sm-2 editable">
													<div>
														<select name="jenis" class="form-control">
															<option value="0"<?php if($jenis == 0){echo ' selected=""';} ?>>
																--pilih--
															</option>
															<option value="1"<?php if($jenis == 1){echo ' selected=""';} ?>>
																Roda Dua
															</option>
															<option value="2"<?php if($jenis == 2){echo ' selected=""';} ?>>
																Roda Tiga
															</option>
															<option value="3"<?php if($jenis == 3){echo ' selected=""';} ?>>
																Roda Empat
															</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Model :</label>
												<div class="col-sm-3 editable">
													<div>
														<input type="text" class="form-control" name="model" value="<?php echo $model; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Tahun Pembuatan :</label>
												<div class="col-sm-2 editable">
													<div>
														<select id="thnPembuatan" name="thnPembuatan" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnPembuatan; ?>'), 1);
															</script-->
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Isi Silinder :</label> 
												<div class="col-sm-2 editable">
													<div>
														<input type="text" class="form-control" name="isiSilinder" value="<?php echo $isiSilinder; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">No Rangka :</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="noRangka" value="<?php echo $noRangka; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">No Mesin :</label>
												<div class="col-sm-5 editable">
													<div>
														<input type="text" class="form-control" name="noMesin" value="<?php echo $noMesin; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Warna :</label>
												<div class="col-sm-2 editable">
													<div>
														<input type="text" class="form-control" name="warna" value="<?php echo $warna; ?>" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Tahun Registrasi :</label>
												<div class="col-sm-2 editable">
													<div>
														<select id="thnRegistrasi" name="thnRegistrasi" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnRegistrasi; ?>'), 2);
															</script-->
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Tahun Kendaraan :</label>
												<div class="col-sm-2 editable">
													<div>
														<select id="thnKendaraan" name="thnKendaraan" class="form-control">
															<!--script type="text/javascript">
																isiThn(parseInt('<?php echo $thnKendaraan; ?>'), 3);
															</script-->>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-5 control-label" style="color: white">Status :</label>
												<div class="col-sm-2 editable">
													<div>
														<select name="status" class="form-control">
															<?php if($status == 0){ ?>
																<option value="0" selected="">AN</option>
																<option value="1">OL</option>
															<?php } else { ?>
																<option value="0">AN</option>
																<option value="1" selected="">OL</option>
															<?php } ?>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group" style="color: white">
												<label class="col-sm-5 control-label" style="color: white">Foto Dokumen :</label>
												<div class="col-sm-5 editable">
													<div>
														<small style="color: white;">-Max 6 files(fomat: PNG, JPG)</small>
														<input type="file" name="fotoDocumen[]" multiple="" required="">
													</div>
												</div>
											</div>
											<div class="form_submit clearfix">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-5">
														<input type="hidden" class="form-control" name="insert_jaminan_kendaraan">
														<input type="hidden" class="form-control" name="dari_nasabah">
														<input type="hidden" class="form-control" name="nasabah" value="<?php echo $_SESSION['login_nasabah']?>">
														<input type="hidden" class="form-control" name="ikpi" value="<?php echo $ikpi; ?>">
														<button type="submit" class="btn btn-success btn-lg"><i class="icon-save"></i> SIMPAN</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>

	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>


	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		isiThn(<?php echo $thnPembuatan; ?>, 1);
		isiThn(<?php echo $thnRegistrasi; ?>, 2);
		isiThn(<?php echo $thnKendaraan; ?>, 3);

		function isiThn(thnPilih, id){
			var batas = parseInt("<?php echo date('Y'); ?>");
			var optIsi = '';
			for(var i = 1945; i <= batas; i++){
				if(i == thnPilih){
					optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
				}
			}
			if(id == 1){
				document.getElementById('thnPembuatan').innerHTML = '';
				document.getElementById('thnPembuatan').innerHTML = optIsi;
			} else if(id == 2){
				document.getElementById('thnRegistrasi').innerHTML = '';
				document.getElementById('thnRegistrasi').innerHTML = optIsi;
			} else {
				document.getElementById('thnKendaraan').innerHTML = '';
				document.getElementById('thnKendaraan').innerHTML = optIsi;
			}
		}
		isiCboNasabah(parseInt(<?php echo $id_nasabah; ?>));
		function isiCboNasabah(id_nasabah){
			var optIsi = '';

			// UNTUK OPTION PILIH 
			optIsi = optIsi + '<option value="" selected="">--pilih--</option>';
			<?php
			$result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.user_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
			if (!$result1) { die("SQL Error Result1 "); }
			if (mysqli_num_rows($result1)) {
				while ($allRow1 = mysqli_fetch_array($result1)) {
					?>
					var id = parseInt(<?php echo $allRow1['id']; ?>);
					var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
					if(id == id_nasabah){
						optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
					} else {
						optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
					}
				<?php }} ?>

				document.getElementById('nasabah').innerHTML = '';
				document.getElementById('nasabah').innerHTML = optIsi;
			}
		</script>
	</body>
	</html>




	

	<?php

	function getIdUser($conn, $nasabahId){
		$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['user_id'];
		}
	}

	function getJumlahNotifikasi($conn, $userId){
		$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['jumlah_notifikasi'];
		}
	}

	function generateNotification($conn, $userId){
		$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { die("SQL Error Result1 "); }
		while ($row1 = mysqli_fetch_array($result1)) {

			if(isset($row1['transaksi_investasi_id'])){
				$id = $row1['transaksi_investasi_id'];
				$keterangan = $row1['keterangan'];
				?> 

				<li>
					<form action="transaksi_investasi_detail.php" method="POSt">
						<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
						<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
					</form>
				</li>

				<?php

			}

			if(isset($row1['transaksi_peminjaman_no_kontrak'])){
				$id = $row1['transaksi_peminjaman_no_kontrak'];
				$keterangan = $row1['keterangan'];
				?> 
				<li>
					<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
				</li>
				<?php

			}

			if(isset($row1['transaksi_gadai_id'])){
				$id = $row1['transaksi_gadai_id'];
				$keterangan = $row1['keterangan'];
				?> 
				<li>
					<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
				</li>
				<?php

			}
		}
	}
	function bersihkanNotifikasi($conn, $userId){
		if(isset($_POST['bersihkan_notifikasi'])){
			$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
			if (!$result) {
				die("SQL Error Result ");
			}
			unset($_POST['bersihkan_notifikasi']);
		}
	}

	function cekStatusCair($statusCair)
	{
		if ($statusCair === '0') {
			return 'belum cair';
		} else {
			return 'sudah cair';
		}
	}

	function cekTanggalCair($statusCair, $tanggalCair)
	{
		if (cekStatusCair($statusCair) === 'belum cair') {
			return '-';
		} else {
			return $tanggalCair;
		}
	}

	function cekStatusTransaksi($statusTransaksi)
	{
		if ($statusTransaksi === "0") {
			return "belum selesai";
		} else if ($statusTransaksi === "2") {
			return "pending";
		} else {
			return "sudah selesai";
		}
	}

	function getNamaNasabah($idNasabah, $conn)
	{
		$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) {
			die("SQL Error Result1 ");
		}
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['username'];
		}
	}

	function getNamaPegawai($idPegawai, $conn)
	{
		$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) {
			return "-";
		}
		while ($row1 = mysqli_fetch_array($result1)) {
			return $row1['nama'];
		}
	}

	function cekAutoRenewal($autoRenewal)
	{
		if ($autoRenewal === NULL) {
			return "transaksi baru";
		} else {
			return "transaksi lanjutan dari transaksi $autoRenewal";
		}
	}

	function generateButton($statusTransaksi, $id)
	{
		if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
			$button = <<<TOMBOL
			<form action="transaksi_investasi_penarikan.php" method="POST" >
			<input type="hidden" name="update_status_pencairan" value="true">

			<input type="hidden" name="id_investasi" value="$id">

			<button type="submit" class="btn btn-success">Cairkan Dana</button>
			</form>
			<?php
TOMBOL;
			return $button;
		}
	}

	function cekNotifikasi()
	{
		if (isset($_SESSION['alertInvestasi'])) {
			return true;
		}
	}

	function notifikasi()
	{
		if (cekNotifikasi()) {

			$notifikasi = $_SESSION['alertInvestasi'];
			unset($_SESSION['alertInvestasi']);

			return <<<notifikasi
			<div class="alert alert-success alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Berhasil</strong> $notifikasi
			</div>
notifikasi;
		}
	}

	?>
	<?php ob_end_flush(); ?>