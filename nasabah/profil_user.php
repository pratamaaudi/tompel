<?php
ob_start();
session_start();
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php'; 
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<?php
	$pesan_profil_user = ''; 
	if (isset($_SESSION['pesan_profil_user'])) {
		$pesan_profil_user = $_SESSION['pesan_profil_user'];
		unset($_SESSION['pesan_profil_user']);
		echo '<script>alert("'.$pesan_profil_user.'")</script>';
	}
	$sql1 = "SELECT n.*, u.img, u.username, u.password, u.tgl_regist, u.hak_akses, u.email FROM user u JOIN nasabah n ON u.id = n.User_id WHERE n.id = $id_nasabah";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL ERROR : result1"); }
	$row1 = mysqli_fetch_array($result1);
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="transaksi_investasi.php" method="POST" style="float: right;">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>
						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<!--<span class="label label-danger">12</span>-->
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" title="<?php echo('User') ?>" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>

								
							</ul>
						</div>
					</div>
					<!-- <div class="col-xs-12 col-sm-pull-6 col-sm-4">
						<form class="main_search">
							<input type="text" id="search_query" name="search_query" class="typeahead form-control">
							<button type="submit" class="btn btn-primary btn-xs"><i class="icon-search icon-white"></i></button>
						</form> 
					</div> -->
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix" style="position: center">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<ul>
					<li><span>Profil User</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix">
				<div id="main_content">

					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="user_heading">
								<div class="row">
									<div class="col-sm-2 hidden-xs">
										<center>
											<img src="../gallery/<?php echo $row1['img']; ?>" style="width: 80%; height: 80%;" class="img-thumbnail user_avatar">
										</center>
									</div>
									<div class="col-sm-10">
										<div class="user_heading_info">
											<div class="user_actions pull-right">
												<a class="edit_form" data-toggle="tooltip" data-placement="top auto" title="Edit profile"><span class="icon-edit"></span></a>
											</div>
											<h1><?php echo $row1['nama']; ?></h1>
											<h2 style="color: black; font-size: 30px"><strong>NASABAH</strong></h2>
										</div>
									</div>
								</div>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-2">
										<form class="form-horizontal user_form" action="profil_user_sistem.php" method="POST" enctype="multipart/form-data">
											<h3 style="font-size: 250%; class="heading_a">UMUM</h3>
											<div class="form-group">
												<label class="col-sm-2 control-label">Foto Profil</label>

												<!-- Untuk menampilkan foto profil -->
												<div class="col-sm-10 editable">
													<img src="../gallery/<?php echo $row1['img'];?>" height="42" width="42">
													<div class="hidden_control">
														<input type="file" class="form-control" name="img">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">NIK</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['no_ktp']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="noKtp" value="<?php echo $row1['no_ktp']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Username</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['username']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="username" value="<?php echo $row1['username']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Name</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['nama']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="nama" value="<?php echo $row1['nama']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Alamat</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['alamat']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="alamat" value="<?php echo $row1['alamat']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Pekerjaan</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['nama_pekerjaan']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="namaPekerjaan" value="<?php echo $row1['nama_pekerjaan']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Alamat Kerja</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['alamat_pekerjaan']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="alamatPekerjaan" value="<?php echo $row1['alamat_pekerjaan']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Password</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static">*****</p>
													<div class="hidden_control">
														<input id="p" type="password" class="form-control" name="password" disabled="" style="float: left; width: 80%;"><a onclick="BukaPassword()" class="btn btn-default" style="float: right; width: 20%;" title="ubah kata sandi"><span class="glyphicon glyphicon-pencil"></span></a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">No. Rekening</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['no_rekening']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="noRekening" value="<?php echo $row1['no_rekening']; ?>">
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; class="heading_a">INFO KONTAK</h3>
											<div class="form-group">
												<label class="col-sm-2 control-label">Email</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['email']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="email" value="<?php echo $row1['email']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Phone</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['telp']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="telp" value="<?php echo $row1['telp']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">SmartPhone</label>
												<div class="col-sm-10 editable">
													<p class="form-control-static"><?php echo $row1['no_hp']; ?></p>
													<div class="hidden_control">
														<input type="text" class="form-control" name="noHp" value="<?php echo $row1['no_hp']; ?>">
													</div>
												</div>
											</div>
											<div class="form_submit clearfix" style="display:none">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<input type="hidden" class="form-control" name="idUser" value="<?php echo $row1['user_id']; ?>">
														<input type="hidden" class="form-control" name="idNasabah" value="<?php echo $row1['id']; ?>">
														<input type="hidden" class="form-control" name="datime" value="<?php echo $row1['tgl_regist']; ?>">
														<button type="submit" class="btn btn-primary btn-lg"><i class="icon-save"></i> SIMPAN</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>

	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		var key=0;
		function BukaPassword (){
			if(key == 0){
				document.getElementById("p").disabled = false;
				key = 1;
			} else{
				document.getElementById("p").disabled = true;
				document.getElementById("p").value = '';
				key = 0;
			}
		}
	</script>
	<div class="style_items" id="sidebar_switch">
		<p class="style_title">Sidebar position</p>
		<label class="radio-inline">
			<input type="radio" name="sidebar_position" id="sidebar_left" value="left" checked> Left
		</label>
		<label class="radio-inline">
			<input type="radio" name="sidebar_position" id="sidebar_right" value="right"> Right
		</label>
	</div>
	

</div>
</body>
</html>



<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}

?>
<?php ob_end_flush(); ?>