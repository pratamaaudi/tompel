<?php
ob_start();
session_start();
date_default_timezone_set('Asia/Jakarta');
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php'; 
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<?php
	$nasabah = 0;
	$kendaraan = 0;
	$suku_bunga = 0;
	$jangka_waktu = 0;
	$tgl_waktu_sekarang = date('Y-m-d h:i:s');
	$tws = explode(" ", $tgl_waktu_sekarang);
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="transaksi_peminjaman_insert.php" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>






						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">               
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav><!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container">
				<ul>
					<li><a href="pendataan.php">Pendataan</a></li>
					<li><a href="transaksi_peminjaman.php">Transaksi Peminjaman</a></li>
					<li><span>Ajukan Transaksi Peminjaman</span></li>						
				</ul>
			</div>
		</section>
		<section class="container clearfix main_section">
			<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px">
				<div id="main_content">

					<!-- main content -->
					<div class="row">
						<div class="col-sm-12">
							<div class="user_heading">
								<div class="row">
									<div class="col-sm-1 hidden-xs"></div>
									<div class="col-sm-10">
										<div class="user_heading_info">
											<center>
												<div class="user_actions pull-right"></div>
												<h1 style="color: white; font-size: 300%" >Transaksi Peminjaman</h1>
											</center>
										</div>
									</div>
								</div>
							</div>
							<div class="user_content">
								<div class="row">
									<div class="col-sm-10 col-sm-offset-2">
										<form class="form-horizontal user_form" action="transaksi_peminjaman_insert_sistem.php" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label class="col-sm-9 control-label" style="color: white">Tanggal</label>
												<div class="col-sm-3 editable">
													<div>
														<input id="tgl_sekarang" type="date" class="form-control" name="tgl_sekarang" readonly="" value="<?php echo $tws[0]; ?>">
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Kendaraan</strong>
												<a href="jaminan_kendaraan_insert.php?ikpi=1" class="btn" style="float: right; background-color: white">
													<i class="icon-plus-sign-alt"></i>TAMBAH KENDARAAN
												</a>
											</h3>
											<!-- COMBO BOX KENDARAAN -->
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="kendaraan" onchange="isi_data_kendaraan()" name="kendaraan" class="form-control">
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-10 editable">
													<div>
														<p id="no_bpkb" class="form-control-static"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Atas Nama STNK :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="nama_pemilik_stnk" class="form-control-static" style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="status_kendaraan" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jenis Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="jenis_kendaraan" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No Polisi :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_polisi" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white" >Tahun Kendaraan :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="thn_kendaraan" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">No. Mesin :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_mesin" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group" style="margin-bottom: 15px;">
												<label class="col-sm-2 control-label" style="color: white">No. Rangka :</label>
												<div class="col-sm-10 editable">
													<div>
														<p id="no_rangka" class="form-control-static"style="color: white"></p>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jaminan :</label>
												<div class="col-sm-10 editable">
													<div>
														<select id="jaminan" name="jaminan" class="form-control">
															<option value="0">BPKB</option>
															<option value="1">KENDARAAN</option>
															<option value="2">BPKB + KENDARAAN</option>
														</select>
													</div>
												</div>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Terusan</strong></h3>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nominal Pinjaman :</label>
												<div class="col-sm-4 editable">
													<div>
														<input id="nominal_pinjaman" type="number" class="form-control" min="0" name="nominal_pinjaman" required="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jangka Waktu :</label>
												<div class="col-sm-3 editable">
													<div>
														<select id="jangka_waktu" name="jangka_waktu" class="form-control" style="width: 70%; float: left;">
														</select>
														<label style="color: white">&nbsp / BULAN</label>
													</div>
												</div>
											</div>
											<div class="form_submit clearfix">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<input name="wkt_sekarang" type="hidden" value="<?php echo $tws[1]; ?>">
														<input name="insert_transaksi_peminjaman" type="hidden">
														<button type="submit" class="btn btn-success btn-lg"><i class="icon-save"></i> AJUKAN</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<div id="footer_space"></div>
	</div>


	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>


	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/bootbox/bootbox.min.js"></script>
	<script src="../js/pages/ebro_user_profile.js"></script>
	<script type="text/javascript">
		isiCboKendaraan(<?php echo $id_nasabah; ?>);
		function isiCboKendaraan(id_nasabah){
			var optIsi = '';
			<?php
			$resultK = mysqli_query($conn, "SELECT * FROM jenis_jaminan WHERE shapus = 0 ORDER BY no_polisi ASC");
			if (mysqli_num_rows($resultK)) {
				while ($rowK = mysqli_fetch_array($resultK)) { 
					$tulis = $rowK['no_polisi']."(".$rowK['model']." ".$rowK['merk']."-".$rowK['type'].")";
					$idK = $rowK['id'];
					$rcst = mysqli_query($conn, "SELECT * FROM transaksi_peminjaman WHERE jenis_jaminan_id = $idK AND status_transaksi = 1");
					if (!mysqli_num_rows($rcst)) {
						?>
						if(id_nasabah == <?php echo $rowK['nasabah_id']; ?>){
							optIsi = optIsi + '<option value="<?php echo $idK; ?>"><?php echo $tulis; ?></option>';
						}
						<?php 
					}
				}
			} ?>
			document.getElementById('kendaraan').innerHTML = '';
			document.getElementById('kendaraan').innerHTML = optIsi;
			isi_data_kendaraan();
		}

		function isi_data_kendaraan(){ 
			var id_kendaraan = $('#kendaraan').val();
			if(id_kendaraan != null){
				$.post("transaksi_peminjaman_insert_sistem.php", {
					id_kendaraan : id_kendaraan,
					isi_data_kendaraan : ''
				}, function(result){
					if(result != ''){
	            //alert(result);
	            var isi1 = result.split("-");;
	            document.getElementById('no_bpkb').innerHTML = isi1[0];
	            document.getElementById('nama_pemilik_stnk').innerHTML = isi1[1];
	            document.getElementById('status_kendaraan').innerHTML = isi1[2];
	            document.getElementById('jenis_kendaraan').innerHTML = isi1[3];
	            document.getElementById('no_polisi').innerHTML = isi1[4];
	            document.getElementById('thn_kendaraan').innerHTML = isi1[5];
	            document.getElementById('no_mesin').innerHTML = isi1[6];
	            document.getElementById('no_rangka').innerHTML = isi1[7];
	        } else {
	        	alert('Error');
	        }
	    }); 
			} else {
				document.getElementById('no_bpkb').innerHTML = '';
				document.getElementById('nama_pemilik_stnk').innerHTML = '';
				document.getElementById('status_kendaraan').innerHTML = '';
				document.getElementById('jenis_kendaraan').innerHTML = '';
				document.getElementById('no_polisi').innerHTML = '';
				document.getElementById('thn_kendaraan').innerHTML = '';
				document.getElementById('no_mesin').innerHTML = '';
				document.getElementById('no_rangka').innerHTML = '';
			}
		}
		
		isiCboJangkaWaktu(parseInt(<?php echo $jangka_waktu; ?>));
		function isiCboJangkaWaktu(jangka_waktu){
			var optIsi = '';
			for(var i = 1; i <= 12; i++){
				if(i == jangka_waktu){
					optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
				}
			}
			document.getElementById('jangka_waktu').innerHTML = '';
			document.getElementById('jangka_waktu').innerHTML = optIsi;
		}
	</script>
</body>
</html>





<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}
?>
<?php ob_end_flush(); ?>