<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 
if (isset($_POST['update_jaminan_kendaraan'])) {
	$idJaminanKendaraan = $_POST['update_jaminan_kendaraan'];
	$noPolisi = $_POST['noPolisi'];
	$namaPemilikSTNK = $_POST['namaPemilikSTNK'];
	$merk = $_POST['merk'];
	$type = $_POST['type'];
	$jenis = $_POST['jenis'];
	$model = $_POST['model'];
	$thnPembuatan = $_POST['thnPembuatan'];
	$isiSilinder = $_POST['isiSilinder'];
	$noRangka = $_POST['noRangka'];
	$noMesin = $_POST['noMesin'];
	$warna = $_POST['warna'];
	$thnRegistrasi = $_POST['thnRegistrasi'];
	$thnKendaraan = $_POST['thnKendaraan'];
	$status = $_POST['status'];
	
	$folder_foto = '../gallery/jaminan_kendaraan/' . $_POST['folder'] . '/';
	$baca_folder = scandir($folder_foto);
	$panjang_isi_folder = count($baca_folder) - 2;
	
	$total = count($_FILES['fotoDocumen']['name']);


	// LOOPING UNTUK UPLOAD FOTO SESUAI PANJANG FOLDER
	$jumlah_foto = 0 + $panjang_isi_folder;
	for($i = 0; $i < $total; $i++) {
		$fd = explode(".", $_FILES['fotoDocumen']['name'][$i]);
		$fotoDocumen = substr(md5($_FILES['fotoDocumen']['name'][$i] . date("h:i:s")), 0, 10) . "." . $fd[count($fd) - 1];
		if(strtoupper($fd[count($fd) - 1]) == 'JPG' || strtoupper($fd[count($fd) - 1]) == 'PNG'){
			if($jumlah_foto < 6){
				$jumlah_foto++;
				move_uploaded_file($_FILES['fotoDocumen']['tmp_name'][$i], $folder_foto . $fotoDocumen);
			}
		}
	}	

	$result1 = mysqli_query($conn, "UPDATE `jenis_jaminan` SET `jenis`=$jenis,`no_mesin`='$noMesin',`tahun_kendaraan`=$thnKendaraan,`no_polisi`='$noPolisi',`merk`='$merk',`type`='$type',`model`='$model',`tahun_pembuatan`=$thnPembuatan,`isi_silinder`='$isiSilinder',`warna`='$warna',`tahun_registrasi`=$thnRegistrasi,`status`=$status,`nama_pemilik_stnk`='$namaPemilikSTNK',`no_rangka`='$noRangka' WHERE `id`=$idJaminanKendaraan");
	if ($result1) {
		$_SESSION['jaminan'] = 'kendaraan'; 
		header('Location: jaminan.php'); 
	} else { 
		die("SQL ERROR : Result1"); 
	}
}


//  SOURCE CODE UPDATE STATUS HAPUS JENIS JAMINAN KENDARAAN, AGAR DATA TIDAK HILANG 
if (isset($_GET['dIdJaminanKendaraan'])) {
	$idJaminanKendaraan = $_GET['dIdJaminanKendaraan'];
	$cekTunggaanJenisJaminan = 0;
	$pesan = '';
	$result2 = mysqli_query($conn, "SELECT * FROM jenis_jaminan jj JOIN transaksi_peminjaman tp ON jj.id = tp.jenis_jaminan_id WHERE tp.status_transaksi = 1 AND jj.id = $idJaminanKendaraan");
	if(!$result2){ die("SQL ERROR : result2"); } 
	if (mysqli_num_rows($result2)) {
		$cekTunggaanJenisJaminan = 1;
		$pesan = 'Masih dibuat jaminan dan jaminan masih belum lunas';
	}
	if($cekTunggaanJenisJaminan == 0){
		$result1 = mysqli_query($conn, "UPDATE `jenis_jaminan` SET `shapus`=1 WHERE `id`=$idJaminanKendaraan");
		if ($result1) {
			$_SESSION['jaminan'] = 'kendaraan'; 
			header('Location: jaminan.php'); 
		} else { 
			die("SQL ERROR : Result1"); 
		}
	} else {
		$_SESSION['pesan_jaminan_kendaraan_detail'] = $pesan;
		header("Location: jaminan_kendaraan_detail.php?idKendaraan=".$idJaminanKendaraan);
	}
}
ob_end_flush(); ?>