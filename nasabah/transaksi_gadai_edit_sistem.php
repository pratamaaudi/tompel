<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 


// QUERY MENAMPILKAN DATA NASABAH
if(isset($_POST['isi_data_nasabah'])){
	$id_nasabah = $_POST['id_nasabah'];
	$result = mysqli_query($conn, "SELECT * FROM nasabah WHERE id = $id_nasabah");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	echo $row['no_ktp'] . "-" . $row['nama'] . "-" . $row['alamat'] . "-" . $row['nama_pekerjaan'] . "-" . $row['alamat_pekerjaan'] . "-" . $row['telp'] . "-" . $row['no_hp'];
}

if (isset($_POST['edit_transaksi_gadai'])) {
	$id_transaksi_gadai = $_POST['edit_transaksi_gadai'];
	$id_pegawai = $_SESSION['login_pegawai'];
	$id_nasabah = $_POST['nasabah'];
	$id_suku_bunga = $_POST['suku_bunga'];
	$tanggal_gadai = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	$status_transaksi = $_POST['status_transaksi'];
	$jumlah_angsuran = $_POST['nominal_angsuran'];
	$biaya_administrasi = $_POST['biaya_administrasi'];
	$total = $_POST['total'];
	$status_cair = $_POST['status_cair'];
	$tgl_cair = date('Y-m-d h:i:s');
	$tanggal_gadai_lama = $_POST['tgl_lama'];
	$jangka_waktu_lama = $_POST['jangka_waktu_lama'];

	$result9 = mysqli_query($conn, "SELECT tg.status_transaksi, SUM(tag.jumlah_bayar) AS sudah_angsur FROM transaksi_gadai tg JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai AND tg.status_transaksi = 1 GROUP BY tg.id");
	if(!$result9){ die("SQL Error result 9"); }
	if (mysqli_num_rows($result9)) {
		$row9 = mysqli_fetch_array($result9);
		$sudah_angsur = $row9['sudah_angsur'];
		if($sudah_angsur > 0){ 
			$_SESSION['pesan_transaksi_gadai_edit'] = 'Maaf Transaksi Gadai tidak dapat di EDIT.\nKarena Transaksi Angsuran pada Transaksi Gadai Ini, Ada yang sudah dibayar';
			header('Location: transaksi_gadai_edit.php?id_transaksi='.$id_transaksi_gadai);
		} else {
			$sql1 = "UPDATE `transaksi_gadai` SET `tanggal` = '$tanggal_gadai', `status_cair` = $status_cair";
			if($status_cair != 0){
				$sql1 = $sql1 . ", `tgl_cair` = '$tgl_cair'";
			}
			$sql1 = $sql1 . ", `status_transaksi` = $status_transaksi, `jumlah_pinjaman` = $jumlah_pinjaman, `jangka_waktu` = $jangka_waktu, `jumlah_angsuran` = $jumlah_angsuran, `biaya_administrasi` = $biaya_administrasi, `total` = $total, `nasabah_id` = $id_nasabah, `pegawai_id` = $id_pegawai WHERE `id` = $id_transaksi_gadai";
			$result1 = mysqli_query($conn, $sql1);
			if(!$result1){ die("SQL ERROR : Result1"); }

			$result3 = mysqli_query($conn, "UPDATE `perubahan_suku_bunga` SET `id_suku_bunga` = $id_suku_bunga, `id_transaksi_gadai` = $id_transaksi_gadai, `tgl_perubahan_suku_bunga` = '$tanggal_pinjam' WHERE `tgl_perubahan_suku_bunga`='$tanggal_gadai_lama'");
			if(!$result3){ die("SQL ERROR : Result3"); }

			$result2 = mysqli_query($conn, "DELETE FROM `barang_transaksi_gadai` WHERE id_transaksi_gadai = $id_transaksi_gadai");
			if(!$result2){ die("SQL ERROR : Result2"); }
			
			$jum_barang = count($_POST['id_barang']);
			for($i = 0; $i < $jum_barang; $i++){
				$id_barang = $_POST['id_barang'][$i];
				$jumlah_barang = $_POST['jumlah_barang'][$i];
				$status_barang = $_POST['status_barang'][$i];
				
				$result5 = mysqli_query($conn, "INSERT INTO `barang_transaksi_gadai`(`id_barang`, `id_transaksi_gadai`, `jumlah_barang`, `status_barang`) VALUES ($id_barang, $id_transaksi_gadai, $jumlah_barang, $status_barang)");
				if(!$result5){ die("SQL ERROR : Result5"); }	
				
			}

			//jangan lupa or nya nanti dihapus dari if dibawah  || $tanggal_gadai_lama != $tanggal_gadai
			if($jangka_waktu_lama != $jangka_waktu || $tanggal_gadai_lama != $tanggal_gadai){
				$result6 = mysqli_query($conn, "DELETE FROM `transaksi_angsuran_gadai` WHERE transaksi_gadai_id = $id_transaksi_gadai");
				if(!$result6){ die("SQL ERROR : Result6"); }
				$tgl_angsur = intval(substr($tanggal_gadai, 8, 2));
				$bln_angsur = intval(substr($tanggal_gadai, 5, 2));
				$thn_angsur = intval(substr($tanggal_gadai, 0, 4));
				$wkt_angsur = substr($tanggal_gadai, 11, 8);
				for($i = 1; $i <= intval($jangka_waktu); $i++){
					$bln_angsur++;
					if($bln_angsur == 13){
						$bln_angsur = 0;
						$thn_angsur++;
						$i--;
					} else {
						$last_day = $thn_angsur . "-" . $bln_angsur;
						if(strlen(strval($bln_angsur)) == 1){
							$last_day = $thn_angsur . "-0" . $bln_angsur;
						}
						$sesuai_tgl_wkt_angsur = $last_day . "-" . $tgl_angsur . " " . $wkt_angsur;
						if(strlen(strval($tgl_angsur)) == 1){
							$sesuai_tgl_wkt_angsur = $last_day . "-0" . $tgl_angsur . " " . $wkt_angsur;
						}
						$date = new DateTime($last_day);
						$date->modify('last day of this month');
						$tgl_wkt_last = $date->format('Y-m-d') . " " . $wkt_angsur;

						$tgl_wkt_pasti_angsur = '';
						if($sesuai_tgl_wkt_angsur > $tgl_wkt_last){
							$tgl_wkt_pasti_angsur = $tgl_wkt_last;
						} else {
							$tgl_wkt_pasti_angsur = $sesuai_tgl_wkt_angsur;
						}
						$result4 = mysqli_query($conn, "INSERT INTO `transaksi_angsuran_gadai`(`tanggal_bayar`, `angsuran_ke`, `status_angsuran`, `transaksi_gadai_id`) VALUES ('$tgl_wkt_pasti_angsur', $i, 0, $id_transaksi_gadai)");
						if(!$result4){ die("SQL ERROR : Result4"); }
					}
				}
			}
			header('Location: transaksi_gadai.php'); 
		}
	} else {
		header('Location: transaksi_gadai_edit.php?id_transaksi='.$id_transaksi_gadai);
	}
}
if(isset($_GET['del_id_transaksi_gadai'])){
	$id_transaksi = $_GET['del_id_transaksi_gadai'];
	$result1 = mysqli_query($conn, "UPDATE `transaksi_gadai` SET `shapus` = 1 WHERE id = $id_transaksi AND status_transaksi = 0");
	if(!$result1){ die("SQL ERROR : Result1"); }
	$_SESSION['pesan_transaksi_gadai_edit'] = "Transaksi Gadai Tidak Bisa Di Hapus, Karena Angsuran Belum Lunas";
	header("Location: transaksi_gadai_edit.php?id_transaksi=".$id_transaksi);
}

if(isset($_GET['nasabah_setuju'])){
	$id = $_GET['nasabah_setuju'];
	$result1 = mysqli_query($conn, "UPDATE `transaksi_gadai` SET status_transaksi = 1 WHERE id = $id AND status_transaksi = 2");
	if(!$result1){ 
		die("SQL ERROR : Result1"); 
	} else {
		$_SESSION['pesan_transaksi_gadai_edit'] = "Transaksi Gadai Telah Anda Setujui, Silahkan Datang Ke Koperasi Beserta Barang yang Anda Gadaikan";
		header("Location: transaksi_gadai_edit.php?id_transaksi=".$id);
	}
}

if(isset($_GET['nasabah_batal'])){
	$id = $_GET['nasabah_batal'];
	$result1 = mysqli_query($conn, "DELETE FROM `transaksi_angsuran_gadai` WHERE transaksi_gadai_id = $id");
	if(!$result1){ die("SQL ERROR : Result1"); }
	$result2 = mysqli_query($conn, "DELETE FROM `transaksi_gadai` WHERE id = $id");
	if(!$result2){ die("SQL ERROR : Result2"); }
	$result3 = mysqli_query($conn, "DELETE FROM `perubahan_suku_bunga` WHERE id_transaksi_gadai = $id");
	if(!$result3){ die("SQL ERROR : Result3"); }
	$result4 = mysqli_query($conn, "DELETE FROM `barang_transaksi_gadai` WHERE id_transaksi_gadai = $id");
	if(!$result4){ die("SQL ERROR : Result4"); }
	$_SESSION['pesan_transaksi_gadai'] = "Transaksi Gadai Telah Anda Batalkan";
	header("Location: transaksi_gadai.php");
}
ob_end_flush(); ?>