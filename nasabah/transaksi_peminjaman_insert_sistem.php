<?php
ob_start();
date_default_timezone_set('Asia/Jakarta');
session_start();
require '../config.php'; 
// QUERY MENAMPILKAN DATA NASABAH
if(isset($_POST['isi_data_kendaraan'])){
	$id_kendaraan = $_POST['id_kendaraan'];
	$result = mysqli_query($conn, "SELECT * FROM jenis_jaminan WHERE id = $id_kendaraan");
	if (!$result) { die("SQL Error Result "); }
	$row = mysqli_fetch_array($result);
	$status = 'AN';
	if($row['status'] == 1){ $status = 'OL'; }

	$jenis = '';
	if($row['jenis'] == 1){ $jenis = 'Roda Dua'; }
	else if($row['jenis'] == 2){ $jenis = 'Roda Tiga'; }
	else if($row['jenis'] == 3){ $jenis = 'Roda Empat'; }

	echo "-" . $row['nama_pemilik_stnk'] . "-" . $status . "-" . $jenis . "-" . $row['no_polisi'] . "-" . $row['tahun_kendaraan'] . "-" . $row['no_mesin'] . "-" . $row['no_rangka'];
}
if (isset($_POST['insert_transaksi_peminjaman'])) {
	$id_nasabah = $_SESSION['login_nasabah'];
	$id_jenis_jaminan = $_POST['kendaraan'];
	$tanggal_pinjam = $_POST['tgl_sekarang'] . " " . $_POST['wkt_sekarang'];
	$jangka_waktu = $_POST['jangka_waktu'];
	$jumlah_pinjaman = $_POST['nominal_pinjaman'];
	$jaminan = $_POST['jaminan'];
	
	$sql1 = "INSERT INTO `transaksi_peminjaman`(`tanggal_pinjam`, `jangka_waktu`, `jumlah_pinjaman`, `status_transaksi`, `jaminan`, `nasabah_id`, `jenis_jaminan_id`) VALUES ('$tanggal_pinjam', $jangka_waktu, $jumlah_pinjaman, 3, $jaminan, $id_nasabah, $id_jenis_jaminan)";
	$result1 = mysqli_query($conn, $sql1);
	if(!$result1){ die("SQL ERROR : Result1"); }

	$result2 = mysqli_query($conn, "SELECT * FROM transaksi_peminjaman WHERE tanggal_pinjam = '$tanggal_pinjam'");
	if(!$result2){ die("SQL ERROR : Result2"); }
	$row2 = mysqli_fetch_array($result2);
	$no_transaksi_peminjaman = $row2['no_kontrak'];

	$result3 = mysqli_query($conn, "INSERT INTO `perubahan_suku_bunga`(`id_suku_bunga`, `id_transaksi_peminjaman`, `tgl_perubahan_suku_bunga`) VALUES (0, $no_transaksi_peminjaman, '$tanggal_pinjam')");
	if(!$result3){ die("SQL ERROR : Result3"); }

	header('Location: transaksi_peminjaman.php'); 
}
ob_end_flush(); ?>