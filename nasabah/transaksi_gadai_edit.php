<?php
ob_start();
session_start();
date_default_timezone_set('Asia/Jakarta');
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php'; 
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ebro Admin Template v1.3</title>

	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<?php
	$id_transaksi_gadai = 0;
	if (isset($_GET['id_transaksi'])) {
		$id_transaksi_gadai = $_GET['id_transaksi'];
	} else {
		header("Location: transaksi_peminjaman.php");
	}

	$result2 = mysqli_query($conn, "SELECT *, tg.shapus AS shapus_tg FROM transaksi_gadai tg LEFT JOIN perubahan_suku_bunga psb ON tg.id = psb.id_transaksi_gadai LEFT JOIN suku_bunga sb ON psb.id_suku_bunga = sb.id WHERE tg.id = $id_transaksi_gadai");
	$row2 = mysqli_fetch_array($result2);
	if(!mysqli_num_rows($result2)) {
		$_SESSION['pesan_transaksi_gadai'] = 'Transaksi Gadai Dengan Id Transaksi' . $id_transaksi_gadai . " Belum Di Buat";
		header("Location: transaksi_gadai.php");
	}
	if($row2['shapus_tg'] == 1){
		$_SESSION['pesan_transaksi_gadai'] = 'Transaksi Gadai Dengan Id Transaksi-' . $id_transaksi_gadai . " Sudah Di Hapus";
		unset($_SESSION['pesan_transaksi_gadai_edit']);
		header("Location: transaksi_gadai.php");
	}
	$nasabah = $row2['nasabah_id'];
	$suku_bunga = $row2['id_suku_bunga'];
	$s_suku_bunga = $row2['jumlah_bunga'];
	$jangka_waktu = $row2['jangka_waktu'];
	$tgl_waktu_sekarang = $row2['tanggal'];
	$tws = explode(" ", $tgl_waktu_sekarang);
	$tgl = substr($tws[0], 8, 2)."-".substr($tws[0], 5, 2)."-".substr($tws[0], 0, 4);
	$sts_transaksi = $row2['status_transaksi'];
	$sts_uang = $row2['status_cair'];
	
	$s_sts_transaksi = 'BELUM LUNAS';
	if($sts_transaksi == 0){ $s_sts_transaksi = 'LUNAS'; }
	
	$s_sts_uang = 'Belum Cair';
	if($sts_uang == 1){ $s_sts_uang = 'Sudah Cair'; }

	$sjt_akhir = 0;
	$result_cek_lunas = mysqli_query($conn, "SELECT tg.status_transaksi, DATEDIFF(DATE(NOW()), DATE(MAX(tag.tanggal_bayar))) AS sjt_akhir FROM transaksi_gadai tg LEFT JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai");
	if (mysqli_num_rows($result_cek_lunas)) {
		$row_cek_lunas = mysqli_fetch_array($result_cek_lunas);
		if($row_cek_lunas['status_transaksi'] == 5){ 
			$sjt_akhir = $row_cek_lunas['sjt_akhir'];
			$wktSisaPemutihan = 7 - $sjt_akhir;
			if($wktSisaPemutihan == 0){
				$_SESSION['pesan_transaksi_gadai_edit'] = "Transaksi ini dalam masa jatuh tempo terakhir pelunasan pemutihan." . '\n' . " Segera lunasi angsuran anda!";
			} else if($wktSisaPemutihan > 0 && $wktSisaPemutihan < 8){
				$_SESSION['pesan_transaksi_gadai_edit'] = "Transaksi ini dalam masa pemutihan." . '\n' . " Waktu pelunasan angsuran kurang " . $wktSisaPemutihan . " hari!";
			} 
		}  
	}

	if(isset($_SESSION['pesan_transaksi_gadai_edit'])){ 
		$pesan = $_SESSION['pesan_transaksi_gadai_edit'];
		echo '<script type="text/javascript">alert("' . $pesan . '");</script>';
		unset($_SESSION['pesan_transaksi_gadai_edit']);
	}
	?>
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="navbar-header">   
							<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
							">ARTA MULIA</strong></p>	
						</div>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="transaksi_gadai_edit.php?id_transaksi=<?php echo $id_transaksi_gadai ?>" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div> 
						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">

								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix">
						<li>
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li class="active">             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
			<!-- mobile navigation -->
			<nav id="mobile_navigation"></nav>

			<section id="breadcrumbs">
				<div class="container">
					<ul>
						<li><a href="pendataan.php">Pendataan</a></li>
						<li><a href="transaksi_gadai.php">Transaksi Gadai</a></li>
						<li><span>Edit Transaksi Gadai</span></li>						
					</ul>
				</div>
			</section>
			<section class="container clearfix main_section">
				<div id="main_content_outer" class="clearfix" style="background-color: rgb(0, 128, 128); border-radius: 20px; padding-bottom: 20px">
					<div id="main_content">
						<!-- main content -->
						<div class="row">
							<div class="col-sm-12">

								<!-- START MODAL HAPUS-->
								<div class="modal fade" id="modalHapus">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Hapus Transaksi Gadai</h4>
											</div>
											<div class="modal-body">
												Apakah Anda Yakin ingin menghapus Transaksi Gadai Dengan Id Gadai-<b><?php echo $id_transaksi_gadai; ?> ?</b>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
												<a href="transaksi_gadai_edit_sistem.php?del_id_transaksi_gadai=<?php echo $id_transaksi_gadai ?>" class="btn btn-danger">Ya Yakin!</a>
											</div>
										</div>
									</div>
								</div>
								<!-- END MODAL HAPUS -->

								<div class="user_heading">
									<div class="row">
										<div class="col-sm-1 hidden-xs"></div>
										<div class="col-sm-10">
											<div class="user_heading_info">
												<div class="user_actions pull-right">

												</div>
												<center>
													<div class="user_actions pull-right"></div>
													<h1 style="color: white; font-size: 300%">Transaksi Gadai</h1>
												</center>
											</div>
										</div>
									</div>
								</div>
								<div class="user_content">
									<div class="row">
										<div class="col-sm-10 col-sm-offset-3">
											<form class="form-horizontal user_form" action="transaksi_gadai_edit_sistem.php" method="POST" enctype="multipart/form-data">
												<div class="form-group">
													<label class="col-sm-8 control-label" style="color: white"><strong>Tanggal :</strong></label>
													<div class="col-sm-3 editable">
														<p class="form-control-static" style="color: white"><?php echo $tgl; ?></p>
														<div class="hidden_control">
															<input id="tgl_sekarang" type="date" class="form-control" name="tgl_sekarang" required="" value="<?php echo $tws[0]; ?>">
															<!---->
															<input type="hidden" name="tgl_lama" value="<?php echo $tgl_waktu_sekarang; ?>">	
														</div>
													</div>
												</div>
												<h3 style="font-size: 250%; color: white" class="heading_a col-sm-6" style="background-color: rgb(0, 191, 255)"><strong>Barang</strong></h3>
												<!-- <label class="col-sm-2" style="color: white; text-align: right;"><strong>No Kontrak : <?php echo $id_transaksi_gadai; ?></strong></label> -->
												<div class="form-group">
													<!--label class="col-sm-2 control-label">Barang</label-->
													<div class="col-sm-10 editable">
														<div class="hidden_control">
															<select id="barang" onchange="isi_data_barang()" name="barang" class="form-control">
															</select>
														</div>
													</div>
												</div>
												<div class="form-group" id="div_barang_gadai">
													<?php
													$jum_barang = 0;
													$resultBar = mysqli_query($conn, "SELECT * FROM barang b JOIN barang_transaksi_gadai btg ON b.id = btg.id_barang WHERE btg.id_transaksi_gadai = $id_transaksi_gadai");
													if (!$resultBar) { die("SQL Error ResultBar "); }
													if (mysqli_num_rows($resultBar)) {
														while ($rowBar = mysqli_fetch_array($resultBar)) { 
															$jum_barang++; 
															$sba = $rowBar['status_barang'];
															$sb = '';
															if($rowBar['status_barang'] == 1){
																$sb = 'Di Sita';
															} else if($rowBar['status_barang'] == 2){
																$sb = 'Di Kembalikan';
															} else if($rowBar['status_barang'] == 0){
																$sb = 'Di Tahan';
															} 
															?>
														</label> 
														<div class="col-sm-3 editable" id="div1_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static" style="color: white"><?php echo $rowBar['nama_barang']; ?></p>
															<div id="div1_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<input type="text" name="nama_barang[]" class="form-control" readonly="" placeholder="nama barang" id="nama_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['nama_barang']; ?>">
																<input type="hidden" name="id_barang[]" id="id_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['id_barang']; ?>">
															</div> 
														</div> 
														<div class="col-sm-3 editable" id="div2_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static"style="color: rgb(0, 128, 128);"><?php echo 'Jumlah : ' . $rowBar['jumlah_barang']; ?></p>
															<div id="div2_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<input type="number" name="jumlah_barang[]" class="form-control" placeholder="jumlah barang" required="" id="jumlah_barang<?php echo $jum_barang;?>" value="<?php echo $rowBar['jumlah_barang']; ?>"> 
															</div> 
														</div> 
														<div class="col-sm-3 editable" id="div3_1_<?php echo $jum_barang;?>"> 
															<p class="form-control-static"style="color: white"><?php echo 'Status : ' . $sb; ?></p>
															<div id="div3_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<select id="status_barang<?php echo $jum_barang;?>" name="status_barang[]" class="form-control"> 
																	<option value="0"<?php if($sba==0){ echo ' selected=""'; } ?>>
																		Di Tahan
																	</option> 
																	<option value="1"<?php if($sba==1){ echo ' selected=""'; } ?>>
																		Di Sita
																	</option> 
																	<option value="2"<?php if($sba==2){ echo ' selected=""'; } ?>>
																		Di Kembalikan
																	</option>
																</select> 
															</div> 
														</div> 
														<div class="col-sm-1 editable" id="div4_1_<?php echo $jum_barang;?>">
															<p class="form-control-static" style="color:rgb(0, 128, 128)">-</p>
															<div id="div4_2_<?php echo $jum_barang;?>" class="hidden_control"> 
																<a class="btn btn-danger btn-block" id="btn<?php echo $jum_barang;?>" onclick="hapus_barang_gadai(<?php echo $jum_barang;?>)">X</a> 
															</div> 
														</div>
													<?php }
												} ?>
											</div>
											<h3 style="font-size: 250%; color: white" class="heading_a"><strong>Terusan</strong></h3>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Nominal Pinjaman :</label>
												<div class="col-sm-4 editable">
													<p class="form-control-static" style="color: white">Rp. <?php echo number_format($row2['jumlah_pinjaman'],2,',','.');?></p>

													<div class="hidden_control">
														<input id="nominal_pinjaman" type="number" class="form-control" name="nominal_pinjaman" required="" value="<?php echo $row2['jumlah_pinjaman']; ?>">
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Jangka Waktu :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white"><?php echo $jangka_waktu . ' Bulan'; ?></p>
													<div class="hidden_control">
														<input type="hidden" name="jangka_waktu_lama" value="<?php echo $jangka_waktu; ?>">
														<select id="jangka_waktu" name="jangka_waktu" class="form-control" style="width: 70%; float: left;">
														</select>
														<input class="form-control" value="Bulan" style="width: 28%; float: right;" disabled>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Suku Bunga :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white"><?php echo $s_suku_bunga . ' %'; ?></p>
													<div class="hidden_control">
														<select id="suku_bunga" name="suku_bunga" class="form-control" style="width: 70%; float: left;">
														</select>
														<input class="form-control" value="%" style="width: 28%; float: right;" disabled>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Transaksi :</label>
												<div class="col-sm-4 editable">
													<p class="form-control-static"style="color: white"><?php echo $s_sts_transaksi; ?></p>
													<div class="hidden_control">
														<select id="status_transaksi" name="status_transaksi" class="form-control">
															<option value="1"<?php if($sts_transaksi == 1){ echo ' selected=""'; } ?>>Belum Lunas</option>
															<option value="0"<?php if($sts_transaksi == 0){ echo ' selected=""'; } ?>>Lunas</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Status Dana :</label>
												<div class="col-sm-4 editable">
													<p class="form-control-static"style="color: white"><?php echo $s_sts_uang; ?></p>
													<div class="hidden_control">
														<select id="status_cair" name="status_cair" class="form-control">
															<option value="0"<?php if($sts_uang == 0){ echo ' selected=""'; } ?>>Belum Cair</option>
															<option value="1"<?php if($sts_uang == 1){ echo ' selected=""'; } ?>>Sudah Cair</option>
														</select>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label"></label>
												<div class="col-sm-8 editable">
													<p class="form-control-static"style="color: white"></p>
													<div class="hidden_control">
														<a class="btn btn-danger" style="float: right;" onclick="isiBiayaTotal()">SINKRONISASI</a>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Biaya Administrasi :</label>
												<div class="col-sm-8 editable">
													<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['biaya_administrasi'],2,',','.');?></p>
													<div class="hidden_control">
														<input id="biaya_administrasi" type="number" class="form-control" name="biaya_administrasi" style="width: 34.5%; float: left;" value="<?php echo $row2['biaya_administrasi']; ?>" readonly>
														<input class="form-control" value="5% * Nominal Pinjaman" style="width: 50%; float: right;" disabled>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Total :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['total'],2,',','.');?></p>
													<div class="hidden_control">
														<input id="total" type="number" class="form-control" name="total" value="<?php echo $row2['total']; ?>" readonly>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" style="color: white">Angsuran Bunga :</label>
												<div class="col-sm-3 editable">
													<p class="form-control-static"style="color: white">Rp. <?php echo number_format($row2['jumlah_angsuran'],2,',','.');?> / BULAN</p>

													<div class="hidden_control">
														<input id="nominal_angsuran" type="number" class="form-control" name="nominal_angsuran" style="width: 70%; float: left;" value="<?php echo $row2['jumlah_angsuran']; ?>" readonly>
														<input class="form-control" value="/ Bulan" style="width: 28%; float: right;" disabled>
													</div>
												</div>
											</div>

											<?php 
											$result_cek_lunas = mysqli_query($conn, "SELECT tg.status_transaksi, DATE_FORMAT(DATE_ADD(MAX(tag.tanggal_bayar), INTERVAL 7 DAY), '%d-%m-%Y') AS tgl_pemutihan FROM transaksi_gadai tg LEFT JOIN transaksi_angsuran_gadai tag ON tg.id = tag.transaksi_gadai_id WHERE tg.id = $id_transaksi_gadai");
											if (mysqli_num_rows($result_cek_lunas)) {
												$row_cek_lunas = mysqli_fetch_array($result_cek_lunas);
												if($row_cek_lunas['status_transaksi'] == 5){ 
													?>												
													<div class="form-group">
														<label class="col-sm-9 control-label" style="color: white"><strong>Tanggal Akhir Pemutihan</strong></label>
														<div class="col-sm-3 editable">
															<p class="form-control-static"style="color: white"><?php echo $row_cek_lunas['tgl_pemutihan']; ?></p>
															<div class="hidden_control">
															</div>
														</div>
													</div>
													<?php
												} 
											}
											?>

											<div class="form_submit clearfix" style="display: none;">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<input name="wkt_sekarang" type="hidden" value="<?php echo $tws[1]; ?>">
														<input name="edit_transaksi_gadai" type="hidden" value="<?php echo $id_transaksi_gadai; ?>">
														<button type="submit" class="btn btn-primary btn-lg">SIMPAN</button>
													</div>
												</div>
											</div>
										</form>
										<?php if($row2['status_transaksi'] == 2){ ?>
											<div class="form_submit clearfix">
												<div class="row">
													<div class="col-sm-10 col-sm-offset-2">
														<div class="col-sm-3"><a class="btn btn-success" href="transaksi_gadai_edit_sistem.php?nasabah_setuju=<?php echo $id_transaksi_gadai; ?>"><i class="icon-thumbs-up"></i> Setuju</a></div>
														<div class="col-sm-3"><a class="btn btn-danger" href=" transaksi_gadai_edit_sistem.php?nasabah_batal=<?php echo $id_transaksi_gadai; ?>"><i class="icon-minus-sign"></i> Batalkan</a></div>
														<div class="col-sm-6"></div>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12"><center>
							<div class="panel panel-default" style=" width: 98%">
								<div class="panel-heading">
									<h4 class="panel-title"><strong>Transaksi Angsuran Bunga</strong>
										<?php if($sts_transaksi == 1){ ?>
											<a class="btn btn-danger" style="float: right;" href="transaksi_gadai_angsur.php?id_transaksi=<?php echo $id_transaksi_gadai; ?>">BAYAR BUNGA</a>
										<?php } else if($sts_transaksi == 5 && $sjt_akhir < 8){ ?>
											<a class="btn btn-danger" style="float: right;" href="transaksi_gadai_angsur.php?id_transaksi=<?php echo $id_transaksi_gadai; ?>">BAYAR BUNGA</a>
										<?php } ?>
									</h4>
								</div>
								<div id="dt_basic_wrapper" class="dataTables_wrapper form-inline" role="grid">
									<div class="dt-top-row">
										<div class="dt-wrapper">
											<table id="dt_basic" class="table table-striped dataTable" aria-describedby="dt_basic_info">
												<thead>
													<tr role="row">
														<th>Bunga Ke</th>
														<th>Jatuh Tempo</th>
														<th>Tanggal Transaksi</th>
														<th>Nominal</th>
														<th>Bayar</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody role="alert" aria-live="polite" aria-relevant="all">
													<?php 
													$result3 = mysqli_query($conn, "SELECT * FROM transaksi_angsuran_gadai WHERE transaksi_gadai_id = $id_transaksi_gadai");
													if (!$result3) { die("SQL Error Result3 "); }
													while ($row3 = mysqli_fetch_array($result3)) {
														?>
														<tr class="odd">
															<td><?php echo $row3['angsuran_ke'] ?></td>
															<td><?php echo substr($row3['tanggal_bayar'], 8, 2)."-".substr($row3['tanggal_bayar'], 5, 2)."-".substr($row3['tanggal_bayar'], 0, 4); ?></td>
															<td><?php echo substr($row3['real_tgl_bayar'], 8, 2)."-".substr($row3['real_tgl_bayar'], 5, 2)."-".substr($row3['real_tgl_bayar'], 0, 4); ?></td>
															<td><?php echo number_format($row2['jumlah_angsuran'],2,',','.');?></td>

															<td><?php echo number_format($row3['jumlah_bayar'],2,',','.');?></td>
															<td>
																<?php 
																$sts_transaksi_angsuran = 'Belum Lunas';
																if($row2['jumlah_angsuran'] <= $row3['jumlah_bayar'] || $row3['status_angsuran'] == 1){ 
																	$sts_transaksi_angsuran = 'Lunas'; 
																}
																if($row3['status_angsuran'] == 2 || $row3['status_angsuran'] == 3 || $row3['status_angsuran'] == 4){
																	$sts_transaksi_angsuran = 'Pending';	
																}
																echo $sts_transaksi_angsuran; 
																?>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</center>
				</div>
			</div>
		</div>
	</section>
	<div id="footer_space"></div>
</div>

<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
	<footer id="footer" style="background-color: rgb(0, 128, 128);">
		<div class="container">
			<div class="row">
				<div class="col-sm-8"></div>
				<div class="col-sm-12 text-right">
					<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
				</div>
			</div>
		</div>
	</footer>
</div>
<script src="../js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/jquery.ba-resize.min.js"></script>
<script src="../js/jquery_cookie.min.js"></script>
<script src="../js/retina.min.js"></script>
<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
<script src="../js/tinynav.js"></script>
<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
<script src="../js/ebro_common.js"></script>
<script src="../js/lib/bootbox/bootbox.min.js"></script>
<script src="../js/pages/ebro_user_profile.js"></script>
<script type="text/javascript">
	isiCboNasabah(parseInt(<?php echo $nasabah; ?>));
	function isiCboNasabah(id_nasabah){
		var optIsi = '';
		if(id_nasabah == 0){
			optIsi = optIsi + '<option value="" selected="">--pilih--</option>';	
		} else {
			optIsi = optIsi + '<option value="">--pilih--</option>';
		}
		<?php
		$result1 = mysqli_query($conn, "SELECT * FROM user u JOIN nasabah n ON u.id = n.User_id WHERE u.shapus = 0 ORDER BY n.nama ASC");
		if (!$result1) { die("SQL Error Result1 "); }
		if (mysqli_num_rows($result1)) {
			while ($allRow1 = mysqli_fetch_array($result1)) {
				?>
				var id = parseInt(<?php echo $allRow1['id']; ?>);
				var tulis = '<?php echo $allRow1['nama'] . "(" . $allRow1['no_ktp'] . ")"; ?>';
				if(id == id_nasabah){
					optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
				}
				<?php 
			}
		} ?>

		document.getElementById('nasabah').innerHTML = '';
		document.getElementById('nasabah').innerHTML = optIsi;
		isi_data_nasabah();
	}

	var set_awal = 0;
	function isi_data_nasabah(){ 
		var id_nasabah = $('#nasabah').val();
		if(id_nasabah != 0){
			$.post("transaksi_gadai_edit_sistem.php", {
				id_nasabah : id_nasabah,
				isi_data_nasabah : ''
			}, function(result){
				if(result != ''){
					var isi1 = result.split("-");
					document.getElementById('noKtp').innerHTML = isi1[0];
					document.getElementById('nama').innerHTML = isi1[1];
					document.getElementById('alamat').innerHTML = isi1[2];
					document.getElementById('namaPekerjaan').innerHTML = isi1[3];
					document.getElementById('alamatPekerjaan').innerHTML = isi1[4];
					document.getElementById('telp').innerHTML = isi1[5];
					document.getElementById('noHp').innerHTML = isi1[6];
					isiCboBarang(0, id_nasabah);
					if(set_awal == 0){
						set_awal = 1;
					} else {
						hapus_semua_barang_gadai();
					}
				} else {
					alert('Error');
				}
			}); 
		} else {
			document.getElementById('noKtp').innerHTML = '';
			document.getElementById('nama').innerHTML = '';
			document.getElementById('alamat').innerHTML = '';
			document.getElementById('namaPekerjaan').innerHTML = '';
			document.getElementById('alamatPekerjaan').innerHTML = '';
			document.getElementById('telp').innerHTML = '';
			document.getElementById('noHp').innerHTML = '';
			isiCboBarang(0, 0);
			hapus_semua_barang_gadai();
		}
	}
</script>
<script type="text/javascript">
	function isiCboBarang(id_barang, id_nasabah){
		var optIsi = '';
		optIsi = optIsi + '<option value="'+id_barang+'" selected="">--pilih--</option>';
		<?php
		$resultK = mysqli_query($conn, "SELECT b.*, btg.status_barang, btg.id_transaksi_gadai, tg.status_transaksi FROM barang b LEFT JOIN barang_transaksi_gadai btg ON b.id = btg.id_barang LEFT JOIN transaksi_gadai tg ON btg.id_transaksi_gadai = tg.id LEFT JOIN nasabah n ON tg.nasabah_id = n.id WHERE b.shapus = 0 ORDER BY b.nama_barang ASC");
		if (!$resultK) { die("SQL Error ResultK "); }
		if (mysqli_num_rows($resultK)) {
			while ($rowK = mysqli_fetch_array($resultK)) {
				?>
				var id = parseInt(<?php echo $rowK['id']; ?>);
				var id_nasabah1 = parseInt(<?php echo $rowK['nasabah_id']; ?>);
				var status_transaksi = parseInt(<?php echo $rowK['status_transaksi']; ?>);
				var sst = status_transaksi.toString();
				var tulis = '<?php echo $rowK['nama_barang']; ?>';
				var id_tg = '<?php echo $rowK['id_transaksi_gadai']; ?>';
				if(sst != '1' || sst == '' || sst == null){
					if(id_nasabah == id_nasabah1){
						if(id == id_barang){
							optIsi = optIsi + '<option value="' + id + '" selected="">' + tulis + '</option>';
						} else {
							optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
						}
					}
				} else {
					if(id_nasabah == id_nasabah1){
						if(id_tg == parseInt(<?php echo $id_transaksi_gadai; ?>)){
							optIsi = optIsi + '<option value="' + id + '">' + tulis + '</option>';
						}
					}
				}
			<?php }
		} ?>
		document.getElementById('barang').innerHTML = '';
		document.getElementById('barang').innerHTML = optIsi;
	}

	var add_barang_gadai = <?php echo $jum_barang; ?>;
	function isi_data_barang(){
		add_barang_gadai++;
		var id_barang = $("#barang").find('option:selected').val();
		var nama_barang = $("#barang").find('option:selected').text();
		var s_ada = 0;
		for(var i = 1; i <= add_barang_gadai; i++){
			var cek_id = document.getElementById('id_barang'+i);
			if(cek_id){
				if(cek_id.value == id_barang){
					s_ada = 1;
				}
			}
		}
		if(s_ada == 0){
			if(id_barang != 0){
				var tulis_isi = '<div class="col-sm-3 editable" id="div1_1_'+add_barang_gadai+'"> <div id="div1_2_'+add_barang_gadai+'"> <input type="text" name="nama_barang[]" class="form-control" readonly="" placeholder="nama barang" id="nama_barang'+add_barang_gadai+'" value="'+nama_barang+'"> <input type="hidden" name="id_barang[]" id="id_barang'+add_barang_gadai+'" value="'+id_barang+'"> </div> </div> <div class="col-sm-3 editable" id="div2_1_'+add_barang_gadai+'"> <div id="div2_2_'+add_barang_gadai+'"> <input type="number" name="jumlah_barang[]" class="form-control" placeholder="jumlah barang" required="" id="jumlah_barang'+add_barang_gadai+'"> </div> </div> <div class="col-sm-3 editable" id="div3_1_'+add_barang_gadai+'"> <div id="div3_2_'+add_barang_gadai+'"> <select id="status_barang'+add_barang_gadai+'" name="status_barang[]" class="form-control"> <option value="0">Di Tahan</option> <option value="1">Di Sita</option> <option value="2">Di Kembalikan</option> </select> </div> </div> <div class="col-sm-1 editable" id="div4_1_'+add_barang_gadai+'"> <div id="div4_2_'+add_barang_gadai+'"> <a class="btn btn-danger btn-block" id="btn'+add_barang_gadai+'" onclick="hapus_barang_gadai('+add_barang_gadai+')">X</a> </div> </div>';
				document.getElementById('div_barang_gadai').innerHTML += tulis_isi;
			}
		}
	}

	function hapus_semua_barang_gadai() {
		for(var i = 1; i <= add_barang_gadai; i++){
			var cek_id = document.getElementById('id_barang'+i);
			if(cek_id){
				hapus_barang_gadai(i);
			}
		}
		add_barang_gadai = 1;
	}

	function hapus_barang_gadai(ke) {
		$('#lbl'+ke).remove();
		$('#nama_barang'+ke).remove();
		$('#id_barang'+ke).remove();
		$('#status_barang'+ke).remove();
		$('#btn'+ke).remove();
		$('#div1_1_'+ke).remove();
		$('#div1_2_'+ke).remove();
		$('#div2_1_'+ke).remove();
		$('#div2_2_'+ke).remove();
		$('#div3_1_'+ke).remove();
		$('#div3_2_'+ke).remove();
		$('#div4_1_'+ke).remove();
		$('#div4_2_'+ke).remove();
	}
</script>

<script type="text/javascript">
	isiCboSukuBunga(parseInt(<?php echo $suku_bunga; ?>));
	function isiCboSukuBunga(id_suku_bunga){
		var optIsi = '';
		<?php
		$resultSB = mysqli_query($conn, "SELECT * FROM suku_bunga WHERE kode_transaksi = 1 AND shapus = 0 ORDER BY jumlah_bunga ASC");
		if (!$resultSB) { die("SQL Error ResultSB "); }
		if (mysqli_num_rows($resultSB)) {
			while ($rowSB = mysqli_fetch_array($resultSB)) {
				?>
				var id = <?php echo $rowSB['id']; ?>;
				var bunga = <?php echo $rowSB['jumlah_bunga']; ?>;
				if(id == id_suku_bunga){
					optIsi = optIsi + '<option value="' + id + '" selected="">' + bunga + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + id + '">' + bunga + '</option>';
				}
			<?php }} ?>
			document.getElementById('suku_bunga').innerHTML = '';
			document.getElementById('suku_bunga').innerHTML = optIsi;
		}
	</script>
	<script type="text/javascript">
		isiCboJangkaWaktu(parseInt(<?php echo $jangka_waktu; ?>));
		function isiCboJangkaWaktu(jangka_waktu){
			var optIsi = '';
			for(var i = 1; i <= 12; i++){
				if(i == jangka_waktu){
					optIsi = optIsi + '<option value="' + i + '" selected="">' + i + '</option>';
				} else {
					optIsi = optIsi + '<option value="' + i + '">' + i + '</option>';
				}
			}
			document.getElementById('jangka_waktu').innerHTML = '';
			document.getElementById('jangka_waktu').innerHTML = optIsi;
		}
	</script>
	<script type="text/javascript">
		function isiBiayaTotal(){
			var nominal_pinjaman = document.getElementById('nominal_pinjaman').value;
			var cboJangkaWaktu = $('#jangka_waktu').val();
			var cboSukuBunga = $("#suku_bunga").find('option:selected').text();
			if(nominal_pinjaman != '' && parseInt(nominal_pinjaman) != 0){
				var np = parseFloat(nominal_pinjaman);
				var jw = parseFloat(cboJangkaWaktu);
				var sb = parseFloat(cboSukuBunga);
				var biayaAdmin = np * 5 / 100;
				var angsuran = np * sb / 100;
				var total = np + (angsuran * jw);
				document.getElementById('biaya_administrasi').value = biayaAdmin;
				document.getElementById('total').value = total;
				document.getElementById('nominal_angsuran').value = angsuran;
			} else {
				alert('Nominal Pinjaman Tidak Boleh kosong/nol');
			}
		}
	</script>
</div>
</body>
</html>


<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}

?>
<?php ob_end_flush(); ?>