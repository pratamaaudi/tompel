<?php
ob_start();
session_start();
$id_nasabah = 0;
if (!isset($_SESSION['login_nasabah'])) {
	header("Location: login.php");
} else {
	$id_nasabah = $_SESSION['login_nasabah'];
}
require '../config.php';
bersihkanNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); 

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ARTA MULIA</title>
	<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">	
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/todc-bootstrap.min.css">
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="../img/flags/flags.css">
	<link rel="stylesheet" href="../css/retina.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/bootstrap-switch.css">
	<link rel="stylesheet" href="../js/lib/bootstrap-switch/stylesheets/ebro_bootstrapSwitch.css">	
	<link rel="stylesheet" href="../js/lib/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="../js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="../js/lib/magnific-popup/magnific-popup.css">
	<link rel="stylesheet" href="../css/linecons/style.css">
	<link rel="stylesheet" href="../js/lib/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/theme/color_1.css" id="theme">
	<link href='http://fonts.googleapis.com/css?family=Roboto:300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>
<body class="sidebar_hidden">
	<?php require 'modal_bunga.php'; ?>
	<!--?php echo $_SESSION['login_nasabah']; ?-->
	<div id="wrapper_all">
		<header id="top_header" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="navbar-header" >   
						<p class="navbar-brand"><img style="border-radius: 50%" src="../gallery/login.png" width="50" height="40"> <strong style="color: black
						">ARTA MULIA</strong></p>	
					</div>
					<div class="col-sm-push-4 col-sm-3 text-right hidden-xs">
						
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								<span class="label label-danger"><?php echo getJumlahNotifikasi($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?></span>
								<i class="icon-bell icon-2x"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<div class="dropdown_heading">
										<span>Notifikasi</span>                             
										<form action="index.php" method="POST" style="float: right;">
											<input type="hidden" name="id_investasi" value="<?php echo $_POST['id_investasi']; ?>">
											<input type="hidden" name="bersihkan_notifikasi" value="true">
											<input type="hidden" name="userId" value="<?php echo getIdUser($conn, $_SESSION['login_nasabah']); ?>">
											<button type="submit" class="btn btn-danger btn-block">Clear</button>
										</form>
									</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<?php echo generateNotification($conn, getIdUser($conn, $_SESSION['login_nasabah'])); ?>
										</ul>
									</div>
								</li>
							</ul>
						</div>




						<div class="notification_separator"></div>
						<div class="notification_dropdown dropdown">
							<a href="#" class="notification_icon dropdown-toggle" data-toggle="dropdown">
								
								<!-- <i class="icon-envelope-alt icon-2x"></i> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-wide">
								<li>
									<div class="dropdown_heading">Messages</div>
									<div class="dropdown_content">
										<ul class="dropdown_items">
											<li>
												<h3><a href="#">Lorem ipsum dolor sit amet</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aliquam assumenda&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
											</li>
											<li>
												<h3><a href="#">Lorem ipsum dolor&hellip;</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur&hellip;</p>
												<p class="large_info">Sean Walter, 24.05.2014</p>
												<i class="icon-exclamation-sign indicator"></i>
											</li>
										</ul>
									</div>
									<div class="dropdown_footer">
										<a href="#" class="btn btn-sm btn-default">Show all</a>
										<div class="pull-right dropdown_actions">
											<a href="#"><i class="icon-refresh"></i></a>
											<a href="#"><i class="icon-cog"></i></a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-6 col-sm-push-4 col-sm-3">
						<div class="pull-right dropdown">
							<a href="#" class="user_info dropdown-toggle" data-toggle="dropdown">
								<img src="../gallery/<?php echo $_SESSION['img_nasabah']; ?>" alt="">
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="profil_user.php">Profile</a></li>
								<li><a data-toggle="modal" data-target="#modal_bunga">Saldo</a></li>
								<li><a href="logout.php">Log Out</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</header>						
		<nav id="top_navigation">
			<div class="container" align="center">
				<div class="col-sm-2"></div><div class="col-sm-8">
					<ul id="icon_nav_h" class="top_ico_nav clearfix" style="position: center">
						<li class="active">
							<a href="index.php">
								<i class="icon-home icon-2x"></i>
								<span class="menu_label">Home</span>
							</a>
						</li>
						<li>             
							<a href="jaminan.php">
								<i class="icon-suitcase icon-2x"></i>
								<span class="menu_label">Jaminan</span>
							</a>
						</li>
						<li>             
							<a href="pendataan.php">
								<!--<span class="label label-danger">12</span>-->
								<i class="icon-tasks icon-2x"></i>
								<span class="menu_label">Pendataan</span>
							</a>
						</li>
						<li>             
							<a href="angsuran.php">
								<!--<span class="label label-success">$2 347</span>-->
								<i class="icon-money icon-2x"></i>
								<span class="menu_label">Angsuran</span>
							</a>
						</li>
					</ul>
				</div><div class="col-sm-2"></div>
			</div>
		</nav>
		<!-- mobile navigation -->
		<nav id="mobile_navigation"></nav>

		<section id="breadcrumbs">
			<div class="container" style="float: left; padding: 1% 0% 1% 12.5%">
			</div>
		</section>





		<!-- UNTUK MENAMPILKAN DATA NASABAH -->
		<section class="container clearfix main_section";>
			<div id="main_content_outer" class="clearfix">
				<div id="main_content" >
					<!-- main content -->
					<div class="col-sm-12">
						<?php 
						$jml_transaksi_pinjam=0;
						$jml_transaksi_gadai=0;
						$jml_transaksi_investasi=0;

						$sql1 = "
						SELECT count(*) as jumlah
						FROM transaksi_peminjaman 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_pinjam = $row1['jumlah'];
						}

						$sql1 = "
						SELECT count(id) as jumlah
						FROM transaksi_gadai 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_gadai = $row1['jumlah'];
						}

						$sql1 = "
						SELECT count(id) as jumlah
						FROM transaksi_gadai 
						";
						$result1 = mysqli_query($conn, $sql1);
						if (!$result1) { return("-"); }
						while ($row1 = mysqli_fetch_array($result1)) {
							$jml_transaksi_investasi = $row1['jumlah'];
						}
						?>
						<div class="row"> 
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<img src="../../koperasi/gallery/peminjaman.jpg" style="width: 100%;">
									</span>
									<h4><?php echo $jml_transaksi_pinjam; ?></h4>
									<small>Transkasi Peminjaman</small>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<img src="../../koperasi/gallery/gadai.jpg" style="width: 100%;">
									</span>
									<h4><?php echo $jml_transaksi_gadai; ?></h4>
									<small>Transaksi Gadai</small>
								</div>
							</div>
							<div class="col-lg-4 col-md-6">
								<div class="box_stat box_ico">
									<span class="stat_ico stat_ico_1">
										<img src="../../koperasi/gallery/investasi.jpg" style="width: 100%;">
									</span>
									<h4><?php echo $jml_transaksi_investasi; ?></h4>
									<small>Transaksi Invesatasi</small>
								</div>
							</div> 
						</div>
					</div>
					<div class="col-lg-12">
						<br>
						<br>
						<div id="div_chart_kec_count_penduduk"></div>
						<br>
						<div id="sliders">
							<table>
								<tr>
									<td>Alpha Angle</td>
									<td><input id="alpha" type="range" min="0" max="45" value="0"/> <span id="alpha-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Beta Angle</td>
									<td><input id="beta" type="range" min="-45" max="45" value="11"/> <span id="beta-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Depth</td>
									<td><input id="depth" type="range" min="20" max="100" value="100"/> <span id="depth-value" class="value"></span></td>
								</tr>
							</table>
						</div>
						<div class="row">
							<div class="col-lg-12"> 
								<dir id="chart_pie"></dir>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>




		<div id="footer_space"></div>
	</div>

	<div class="col-sm-12" style=" margin: 50px 0px 0px 0px; padding: 50px 0px 0px 0px; position: fixed; bottom: 0px; width: 100%;">
		<footer id="footer" style="background-color: rgb(0, 128, 128);">
			<div class="container">
				<div class="row">
					<div class="col-sm-8"></div>
					<div class="col-sm-12 text-right">
						<small class="text-muted" style="color: black; font-size: 15px; font-family: arial"><strong>ARTA MULIA</strong></small>
					</div>
				</div>
			</div>
		</footer>
	</div>


	<script src="../js/jquery.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../js/jquery.ba-resize.min.js"></script>
	<script src="../js/jquery_cookie.min.js"></script>
	<script src="../js/retina.min.js"></script>
	<script src="../js/lib/typeahead.js/typeahead.min.js"></script>
	<script src="../js/lib/typeahead.js/hogan-2.0.0.js"></script>
	<script src="../js/tinynav.js"></script>
	<script src="../js/lib/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	<script src="../js/lib/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="../js/lib/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<script src="../js/lib/navgoco/jquery.navgoco.min.js"></script>
	<script src="../js/ebro_common.js"></script>
	<script src="../js/lib/peity/jquery.peity.min.js"></script>
	<script src="../js/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../js/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../js/lib/easy-pie-chart/jquery.easy-pie-chart.js"></script>
	<script src="../js/lib/flot/jquery.flot.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.pie.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.time.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.tooltip.min.js"></script>
	<script src="../js/lib/flot/jquery.flot.resize.js"></script>
	<script src="../js/lib/FitVids/jquery.fitvids.js"></script>
	<script src="../js/lib/owl-carousel/owl.carousel.min.js"></script>
	<script src="../js/lib/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="../js/lib/jquery_ui/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="../js/lib/fullcalendar/fullcalendar.js"></script>
	<script src="../js/pages/ebro_dashboard.js"></script>

	<script src="../highcharts/highcharts.js"></script>
	<script src="../highcharts/highcharts-3d.js"></script>
	<script src="../highcharts/exporting.js"></script>
	<script src="../highcharts/export-data.js"></script>

	<div class="style_items" id="sidebar_switch">
	</div>

	<script type="text/javascript"> 

// Set up the chart
var chart_div_chart_kec_count_penduduk = new Highcharts.Chart({
	colors: ['#33cc33', '#ff3300', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
	chart: {
		renderTo: 'div_chart_kec_count_penduduk',
		type: 'column',
		options3d: {
			enabled: true,
			alpha: 0,
			beta: 11,
			depth: 100,
			viewDistance: 25
		}
	},
	title: {
		text: 'Jumlah Transaksi Lunas & Belum Lunas'
	}, 
	plotOptions: {
		column: {
			depth: 25
		}
	},
	xAxis: {
		categories: ['Transaksi Peminjaman', 'Transaksi Gadai', 'Transaksi Investasi'],
		labels: {
			skew3d: true,
			style: {
				fontSize: '16px'
			}
		}
	},
	yAxis: {
		title: {
			text: null
		}
	},
	series: [  
	{
		<?php 
		$jml_lunas_transaksi_pinjam=0;
		$jml_lunas_transaksi_gadai=0;
		$jml_lunas_transaksi_investasi=0;

		$sql1 = "SELECT 
		(SELECT COUNT(no_kontrak) FROM transaksi_peminjaman WHERE status_transaksi=0) as jml_peminjaman,
		(SELECT COUNT(id) FROM transaksi_gadai WHERE status_transaksi=0) as jml_gadai,
		(SELECT COUNT(id) FROM transaksi_investasi WHERE status_transaksi=1) as jml_investasi

		FROM transaksi_gadai 
		LIMIT 1
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) { 
			$jml_lunas_transaksi_pinjam=$row1['jml_peminjaman'];
			$jml_lunas_transaksi_gadai=$row1['jml_gadai'];
			$jml_lunas_transaksi_investasi=$row1['jml_investasi'];
		}
		?>
		name: 'Sudah Selesai ',
		data: [<?php echo $jml_lunas_transaksi_pinjam; ?>, <?php echo $jml_lunas_transaksi_gadai; ?>, <?php echo $jml_lunas_transaksi_investasi; ?>],
		point: {
			events: {
				click: function () {
					// alert('ok');
				}
			}
		}
	}, 
	{
		<?php 
		$jml_blm_lunas_transaksi_pinjam=0;
		$jml_blm_lunas_transaksi_gadai=0;
		$jml_blm_lunas_transaksi_investasi=0;

		$sql1 = "SELECT 
		(SELECT COUNT(no_kontrak) FROM transaksi_peminjaman WHERE status_transaksi=1) as jml_peminjaman,
		(SELECT COUNT(id) FROM transaksi_gadai WHERE status_transaksi=1) as jml_gadai,
		(SELECT COUNT(id) FROM transaksi_investasi WHERE status_transaksi=0) as jml_investasi

		FROM transaksi_gadai 
		LIMIT 1
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) { 
			$jml_blm_lunas_transaksi_pinjam=$row1['jml_peminjaman'];
			$jml_blm_lunas_transaksi_gadai=$row1['jml_gadai'];
			$jml_blm_lunas_transaksi_investasi=$row1['jml_investasi'];
		}
		?>
		name: 'Belum Selesai',
		data: [<?php echo $jml_blm_lunas_transaksi_pinjam; ?>, <?php echo $jml_blm_lunas_transaksi_gadai; ?>, <?php echo $jml_blm_lunas_transaksi_investasi; ?>],
		point: {
			events: {
				click: function () {
					// alert('ok');
				}
			}
		}
	},  
	]
});

function showValues() {
	$('#alpha-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.alpha);
	$('#beta-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.beta);
	$('#depth-value').html(chart_div_chart_kec_count_penduduk.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
	chart_div_chart_kec_count_penduduk.options.chart.options3d[this.id] = parseFloat(this.value);
	showValues();
	chart_div_chart_kec_count_penduduk.redraw(false);
});

showValues();


Highcharts.chart('chart_pie', {
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: false,
		type: 'pie'
	},
	title: {
		text: 'Jumlah Jaminan'
	},
	tooltip: {
		pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	},
	plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,
				format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				style: {
					color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
				}
			}
		}
	},
	series: [{
		name: 'Persentase',
		colorByPoint: true,
		data: [  
		<?php
		$sql1 = "
		SELECT 
		CASE 
		WHEN jaminan = 0 
		THEN 'BPKB'
		WHEN jaminan = 1
		THEN 'Kendaraan'
		ELSE 'BPKB + KENDARAAN'
		END as jenis_jaminan 
		, count(no_kontrak) as jumlah
		from transaksi_peminjaman
		group by jaminan
		";
		$result1 = mysqli_query($conn, $sql1);
		if (!$result1) { return("-"); }
		while ($row1 = mysqli_fetch_array($result1)) {
			$jenis_jaminan = $row1['jenis_jaminan'];
			$jumlah = $row1['jumlah'];
			?> 
			{
				name: '<?php echo $jenis_jaminan; ?> (<?php echo $jumlah; ?>)',
				y: <?php echo $jumlah; ?>,
				sliced: true,
				selected: true
			},
			<?php 
		} 
		?>
		// {
		// 	name: 'testes (54654)',
		// 	y: 4545,
		// 	sliced: true,
		// 	selected: true
		// },
		]
	}]
});
</script>

</div>
</body>
</html>

<?php

function getIdUser($conn, $nasabahId){
	$sql1 = "SELECT user_id FROM `nasabah` WHERE id = $nasabahId";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['user_id'];
	}
}

function getJumlahNotifikasi($conn, $userId){
	$sql1 = "SELECT count(*) as 'jumlah_notifikasi' FROM `notifikasi` WHERE user_id = $userId and status_baca = 0";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['jumlah_notifikasi'];
	}
}

function generateNotification($conn, $userId){
	$sql1 = "SELECT * FROM `notifikasi` WHERE user_id = $userId and status_baca = 0;";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) { die("SQL Error Result1 "); }
	while ($row1 = mysqli_fetch_array($result1)) {

		if(isset($row1['transaksi_investasi_id'])){
			$id = $row1['transaksi_investasi_id'];
			$keterangan = $row1['keterangan'];
			?> 

			<li>
				<form action="transaksi_investasi_detail.php" method="POSt">
					<input type="hidden" name="id_investasi" value="<?php echo $id ?>">
					<button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button>
				</form>
			</li>

			<?php

		}

		if(isset($row1['transaksi_peminjaman_no_kontrak'])){
			$id = $row1['transaksi_peminjaman_no_kontrak'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_peminjaman_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}

		if(isset($row1['transaksi_gadai_id'])){
			$id = $row1['transaksi_gadai_id'];
			$keterangan = $row1['keterangan'];
			?> 
			<li>
				<a href="transaksi_gadai_edit.php?id_transaksi=<?php echo $id ?>"><button type="submit" style="border: none;background-color: inherit;"><?php echo $keterangan; ?></button></a>
			</li>
			<?php

		}
	}
}

function bersihkanNotifikasi($conn, $userId){
	if(isset($_POST['bersihkan_notifikasi'])){
		$result = mysqli_query($conn, "UPDATE `notifikasi` SET `status_baca` = '1' WHERE user_id = $userId");
		if (!$result) {
			die("SQL Error Result ");
		}
		unset($_POST['bersihkan_notifikasi']);
	}
}

function cekStatusCair($statusCair)
{
	if ($statusCair === '0') {
		return 'belum cair';
	} else {
		return 'sudah cair';
	}
}

function cekTanggalCair($statusCair, $tanggalCair)
{
	if (cekStatusCair($statusCair) === 'belum cair') {
		return '-';
	} else {
		return $tanggalCair;
	}
}

function cekStatusTransaksi($statusTransaksi)
{
	if ($statusTransaksi === "0") {
		return "belum selesai";
	} else if ($statusTransaksi === "2") {
		return "pending";
	} else {
		return "sudah selesai";
	}
}

function getNamaNasabah($idNasabah, $conn)
{
	$sql1 = "SELECT username FROM `user` where id = $idNasabah ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		die("SQL Error Result1 ");
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['username'];
	}
}

function getNamaPegawai($idPegawai, $conn)
{
	$sql1 = "SELECT nama FROM `pegawai` where id = $idPegawai ";
	$result1 = mysqli_query($conn, $sql1);
	if (!$result1) {
		return "-";
	}
	while ($row1 = mysqli_fetch_array($result1)) {
		return $row1['nama'];
	}
}

function cekAutoRenewal($autoRenewal)
{
	if ($autoRenewal === NULL) {
		return "transaksi baru";
	} else {
		return "transaksi lanjutan dari transaksi $autoRenewal";
	}
}

function generateButton($statusTransaksi, $id)
{
	if (cekStatusTransaksi($statusTransaksi) === 'belum selesai') {
		$button = <<<TOMBOL
		<form action="transaksi_investasi_penarikan.php" method="POST" >
		<input type="hidden" name="update_status_pencairan" value="true">

		<input type="hidden" name="id_investasi" value="$id">

		<button type="submit" class="btn btn-success">Cairkan Dana</button>
		</form>
		<?php
TOMBOL;
		return $button;
	}
}

function cekNotifikasi()
{
	if (isset($_SESSION['alertInvestasi'])) {
		return true;
	}
}

function notifikasi()
{
	if (cekNotifikasi()) {

		$notifikasi = $_SESSION['alertInvestasi'];
		unset($_SESSION['alertInvestasi']);

		return <<<notifikasi
		<div class="alert alert-success alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Berhasil</strong> $notifikasi
		</div>
notifikasi;
	}
}

?>
<?php ob_end_flush(); ?>